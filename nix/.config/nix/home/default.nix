{
  lib,
  self,
  ...
}: {
  # disable manuals as nmd fails to build often
  manual = {
    html.enable = false;
    json.enable = false;
    manpages.enable = false;
  };
  
  systemd.user.startServices = "sd-switch";
  
  nixpkgs = {
    config.allowUnfree = true;
  };
}
