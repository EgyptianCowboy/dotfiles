{ stdenv, lib, fetchgit, pkg-config, wayland, guile, wayland-scanner }:

stdenv.mkDerivation rec {
  pname = "riverguile";
  version = "0.1.0";

  src = fetchgit {
    url = "https://git.sr.ht/~leon_plickat/riverguile";
    rev = "v${version}";
    sha256 = "sha256-v8XJwS6P0pIBVsg2R4Jgf5nr96X4Ac1ulXH2Gqa7JNQ=";
  };

  nativeBuildInputs = [ pkg-config wayland-scanner ];
  buildInputs = [ wayland guile ];

  makeFlags = [
    "PREFIX=$(out)"
    "BINDIR=$(out)/bin"
    "DATADIR=$(out)/share"
    "MODULEDIR=$(out)/share/guile/3.0"
    "MANDIR=$(out)/share/man"
  ];

  meta = with lib; {
    description = "Guile modules for controlling the river Wayland compositor";
    homepage = "https://git.sr.ht/~leon_plickat/riverguile";
    license = licenses.gpl3Plus;
    platforms = platforms.linux;
  };
}
