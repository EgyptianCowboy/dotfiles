{
  pkgs,
  ...
}:
let
  riverguile = pkgs.callPackage ./riverguile {};
in
{
  wayland.windowManager.river = {
    enable = true;
    package = pkgs.river;
    systemd.enable = true;
    xwayland = {
      enable = true;
      # hidpi = true;
    };
  };

  home.packages = with pkgs; [
    riverguile
  ];

  # xdg.configFile = {

  # };

  # home.sessionVariables = {
  #   XDG_CURRENT_DESKTOP="Hyprland";
  #   XDG_SESSION_DESKTOP="Hyprland";
  # };

  # home.packages = with pkgs; [
  #   hyprpaper
  # ];
}
