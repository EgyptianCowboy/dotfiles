{...}:
{
  services.wlsunset = {
    enable = true;
    latitude = "50.8";
    longitude = "4.3";
    temperature = {
      day = 6000;
      night = 3000;
    };
  };
}
