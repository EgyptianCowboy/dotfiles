{
  pkgs,
  ...
}:
{
  imports = [
    ./waybar
    ./hyprland
    # ./river
    ./swaylock
    ./wlsunset.nix
    ./swayidle.nix
    ./fnott.nix
  ];

  home.packages = with pkgs; [
    # screenshot
    grim
    slurp
  ];

  # make stuff work on wayland
  home.sessionVariables = {
    QT_QPA_PLATFORM = "wayland";
    SDL_VIDEODRIVER = "wayland";
    XDG_SESSION_TYPE = "wayland";

    NIXOS_OZONE_WL=1;
    MOZ_ENABLE_WAYLAND=1;
    MOZ_USE_XINPUT2=1;
  };

  # fake a tray to let apps start
  # https://github.com/nix-community/home-manager/issues/2064
  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = ["graphical-session-pre.target"];
    };
  };
}
