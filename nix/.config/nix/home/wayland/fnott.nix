{...}: {
  services.fnott = {
    enable = true;
    settings = {
      main = {
        title-font = "Berkeley Mono";
        title-color = "8f0075ff";
        body-font = "Berkeley Mono";
        body-color = "000000ff";
        summary-font = "Berkeley Mono";
        summary-color = "000000ff";

        background = "ffffffff";
        border-color = "9f9f9fff";
        border-size = "3";
        padding-vertical = "20";
        padding-horizontal = "20";

        title-format = "<b>%a</b> %A\\n";
        summary-format = "%s\\n";
        body-format = "%b\\n";
      };
      low = {
        title-color = "8f0075ff";
        body-color = "000000ff";
        summary-color = "000000ff";
        background = "ffffffff";
      };
      normal = {
        title-color = "8f0075ff";
        body-color = "000000ff";
        summary-color = "000000ff";
        background = "ffffffff";
      };
      critical = {
        background = "ff8f88ff";
      };
    };
  };
}
