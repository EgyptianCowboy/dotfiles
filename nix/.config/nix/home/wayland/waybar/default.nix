{
  pkgs,
  ...
}: {
  programs.waybar = {
    enable = true;
    package = pkgs.waybar;
    settings = [{
      layer = "top";
      position = "top";
      height = 34;
      margin = "4,4,4,0";
      modules-left = ["custom/launcher"];
      modules-center = ["hyprland/workspaces"];
      modules-right = ["pulseaudio" "network" "battery" "cpu" "memory" "temperature" "clock"];
      "hyprland/workspaces" = {
        on-scroll-up = "hyprctl dispatch workspace e+1";
        on-scroll-down = "hyprctl dispatch workspace e-1";
        on-click = "activate";
        all-outputs = true;
        sort-by-number = true;
        active-only = false;
        format = "{icon}";
        format-icons = {
          "urgent" = "";
          "active" = "";
          "focused" = "";
          "default" = "";
        };   
      };
      "clock" = {
        timezone = "Europe/Brussels";
        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
        format-alt = "{:%Y-%m-%d}";
      };
      "battery" = {
        bat = "BAT0";
        states = {
          "warning" = 30;
          "critical" = 15;
        };
        format = "{icon}  {capacity}%";
        format-icons = ["" "" "" "" ""];
        format-charging = "{capacity}% ";
        format-plugged = "{capacity}% ";
        format-alt = "{time} {icon}";
      };
      "network" = {
        format-wifi = " {essid} ({signalStrength}%)";
        format-ethernet = " {ifname}: {ipaddr}/{cidr}";
        format-disconnected = "Disconnected ⚠";
        tooltip-format = "{ifname} via {gwaddr} ";
        format-alt = "{ifname}: {ipaddr}/{cidr}";
      };
      "pulseaudio" = {
        scroll-step = 1;
        format = "{volume}% {icon}";
        format-bluetooth = "{volume}% {icon}";
        format-bluetooth-muted = "{icon} {format_source}";
        format-muted = "{format_source}";
        format-source = "";
        format-source-muted = "";
        format-icons = {
          headphone = "";
          hands-free = "";
          headset = "";
          phone = "";
          portable = "";
          car = "";
          default = ["" "" ""];
        };
        on-click = "pavucontrol";
      };
      "custom/launcher" = {
        format = "";
        on-click = "fuzzel";
        on-click-right = "fuzzel";
      };
      "backlight" = {
        format = "{percent}% {icon}";
        format-icons = ["" "" "" "" "" "" "" "" "" "" "" "" "" ""];
      };
      "cpu" = {
        format = "{usage}% ";
        tooltip = false;
      };
      "memory" = {
        format = "{}% ";
      };
      "temperature" = {
        critical-threshold = 80;
        format = "{temperatureC}°C {icon}";
        format-icons = [""];
      };
    }];
    style = (builtins.readFile ./style.css);
  };
}
