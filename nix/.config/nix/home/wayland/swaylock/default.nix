 {
   pkgs,
   self,
  ...
 }: {
   programs.swaylock = {
      enable = true;
      package = pkgs.swaylock-effects;
      settings = {
        clock = true;
        indicator = true;
	      indicator-radius = 100;
	      indicator-thickness =  7;
	      effect-blur = "7x5";
        # fade-in = 0.2;
        image = "${self}/home/images/stallman.jpg";
      };
    };
 }
