{
  pkgs,
  ...
}: {
  wayland.windowManager.hyprland = {
    enable = true;
    package = pkgs.hyprland;
    extraConfig = (builtins.readFile ./hypr/hyprland.conf);
    systemd.enable = true;
    xwayland = {
      enable = true;
      # hidpi = true;
    };
  };

  xdg.configFile = {
    "hypr/hyprpaper.conf".source = ./hypr/hyprpaper.conf;
    "hypr/colors" = { source = ./hypr/colors; recursive = true;};
    "hypr/themes" = { source = ./hypr/themes; recursive = true;};
    "wallpapers" = { source = ../../images; recursive = true; };
  };

  home.sessionVariables = {
    XDG_CURRENT_DESKTOP="Hyprland";
    XDG_SESSION_DESKTOP="Hyprland";
  };

  home.packages = with pkgs; [
    hyprpaper
  ];
}
