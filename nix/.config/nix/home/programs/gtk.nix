{
  pkgs,
  config,
  ...
}: let
  inherit (config.programs.matugen) variant;
in {
  home.pointerCursor = {
    package = pkgs.bibata-cursors;
    name = "Bibata-Modern-Classic";
    size = 24;
    gtk.enable = true;
    x11.enable = true;
  };

    # GTK
  gtk = {
    enable = true;
    font.name = "Roboto";

    cursorTheme = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
    };

    # theme = {
    #   name = "WhiteSur";
    #   package = pkgs.whitesur-gtk-theme.override {
    #     altVariants = ["normal"];
    #     opacityVariants = ["normal"];
    #     themeVariants = ["default" "purple"];
    #     nautilusSize = "default";
    #     panelOpacity = "default";
    #     panelSize = "default";
    #   };
    # };
    #
    # iconTheme = {
    #   name = "WhiteSur";
    #   package = pkgs.whitesur-icon-theme.override {
    #     themeVariants = ["default" "purple"];
    #   };
    # };

    # theme = {
    #   name = "Everforest-Light-B";
    #   package = (pkgs.callPackage ./modules/packages/everforest { });
    # };

    # iconTheme = {
    #   name = "everforest_light";
    #   package = (pkgs.callPackage ./modules/packages/everforest-icons { });
    # };

    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = false;
      gtk-key-theme-name    = "Emacs";
      # gtk-theme-name        = "WhiteSur";
      # gtk-icon-theme-name   = "WhiteSur";
      gtk-cursor-theme-name = "Bibata-Modern-Ice";
    };
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      gtk-key-theme = "Emacs";
      cursor-theme = "Bibata-Modern-Ice";
    };
  };

  xdg.systemDirs.data = [
    "${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}"
    "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}"
  ];
}
