{pkgs, lib, ...}:
let
  emacsLspBooster = import ./emacs-lsp-booster.nix { inherit pkgs lib; };
in {
  home.packages = with pkgs; [
    # misc
    libnotify
    xdg-utils

    # Keyboard
    qmk
    qmk-udev-rules
    via

    # nixd
    nil

    # emacs packages
    emacsLspBooster

    # reading
    foliate
    mupdf
    llpp
    pdf2svg

    # Dictionaries
    gcc
    enchant
    hunspell
    hunspellDicts.en_US
    hunspellDicts.nl_NL

    locale

    typst
  ];
}
