{
  pkgs, ...
}:
{
  imports = [
    ./xdg.nix
    ../terminals/foot.nix
    ./media.nix
    ./git.nix
    ./gtk.nix
    ./gpg.nix
    ./fuzzel.nix
    ./packages.nix
    ./sioyek.nix
    ./browser.nix
    ./pass.nix
  ];
}
