{pkgs, ...}: {
  services.gpg-agent = {
    enable = true;
    extraConfig = ''
      allow-emacs-pinentry
      allow-loopback-pinentry
      '';
    pinentryPackage = pkgs.pinentry-gtk2;
  };
}
