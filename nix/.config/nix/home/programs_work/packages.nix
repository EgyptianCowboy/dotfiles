{pkgs, ...}:
{
  home.packages = with pkgs; [
    nil
    foliate
    locale
    emacs-lsp-booster
    departure-mono
  ];
}
