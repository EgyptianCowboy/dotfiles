{
  pkgs, ...
}:
{
  imports = [
    # ../terminals/foot.nix
    # ./media.nix
    ./git.nix
    # ./xdg.nix
    # ./gtk.nix
    ./gpg.nix
    ./fuzzel.nix
    ./packages.nix
    #  ./sioyek.nix
    ./browser.nix
    # ./pass.nix
  ];
}
