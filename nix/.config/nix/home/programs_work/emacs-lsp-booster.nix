{ pkgs, lib, }:

let
  version = "v0.1.1";
in
pkgs.stdenv.mkDerivation {
  pname = "emacs-lsp-booster";
  inherit version;

  src = pkgs.fetchurl {
    url = "https://github.com/blahgeek/emacs-lsp-booster/releases/download/${version}/emacs-lsp-booster_${version}_x86_64-unknown-linux-musl.zip";
    sha256 = "sha256-89qw75HHNtK9+fvonoHZ0gCjeGoyKm69/yZIAD15RPI=";
  };
  
  sourceRoot = ".";

  buildInputs = [ pkgs.makeWrapper pkgs.unzip ];

  unpackPhase = ''
    unzip $src
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp emacs-lsp-booster $out/bin/
  '';

  meta = with lib; {
    description = "Emacs LSP performance booster";
    homepage = "https://github.com/blahgeek/emacs-lsp-booster";
    license = licenses.mit;
    # maintainers = with maintainers; [ yourname ];
    platforms = platforms.linux;
  };
}
