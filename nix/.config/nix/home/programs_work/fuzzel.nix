{
  pkgs,
  ...
}: {
  programs.fuzzel = {
    enable = true;
    settings = {
      main = {
        font = "Berkeley Mono:size=16";
        prompt = ">> ";
        lines = 15;
        width = 40;
        tabs = 4;
        inner-pad = 5;
        terminal = "''${pkgs.foot}/bin/foot";
        layer = "overlay";
      };
      colors = {
        background="ffffffff";
        text="000000ff";
        match="3548cfff";
        selection="e0e0e0ff";
        selection-text="8f0075ff";
        border="9f9f9fff";
      };
      border = {
        width = 2;
        radius = 5;
      };
    };
  };
}
