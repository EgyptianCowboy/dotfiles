{
  pkgs,
  ...
}: {
  home.packages = [pkgs.gh];
  
  programs.git = {
      enable = true;

      userName = "svaes";
      userEmail = "sil.vaes@rideontrack.com";

      delta = {
        enable = true;
      };

      extraConfig = {
        diff.colorMoved = "default";
        merge.conflictstyle = "diff3";
        credential.helper = "store";
        
      };

      aliases = {
        a = "add";
        b = "branch";
        c = "commit";
        ca = "commit --amend";
        cm = "commit -m";
        co = "checkout";
        d = "diff";
        ds = "diff --staged";
        p = "push";
        pf = "push --force-with-lease";
        pl = "pull";
        l = "log";
        r = "rebase";
        s = "status --short";
        ss = "status";
        forgor = "commit --amend --no-edit";
        graph = "log --all --decorate --graph --oneline";
        oops = "checkout --";
      };

      ignores = ["*~" "*.swp" "*result*" ".direnv" "node_modules"];

      extraConfig.gpg.format = "ssh";
  };
}
