{
  home = {
    username = "sil";
    homeDirectory = "/home/sil";
    stateVersion = "23.11";
    sessionPath = [ "~/.local/bin" ];
    language = {
      base = "en_US.utf8";
      ctype = "en_US.utf8";
      numeric = "en_US.utf8";
      time = "en_US.utf8";
      collate = "en_US.utf8";
      monetary = "en_US.utf8";
      messages = "en_US.utf8";
      paper = "en_US.utf8";
      name = "en_US.utf8";
      address = "en_US.utf8";
      telephone = "en_US.utf8";
      measurement = "en_US.utf8";
    };
  };
  programs.home-manager.enable = true;

  nixpkgs = {
    config.allowUnfree = true;
  };

  imports = [
    ../../editors/emacs
    # ../../programs
    ../../programs_work
    ../../terminals/foot.nix
    # ../../terminals/ghostty.nix
    ../../shell
    # ../../wayland/waybar
  ];
}
