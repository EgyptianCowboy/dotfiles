{
  # self,
  inputs,
  withSystem,
  module_args,
  ...
}: let
  sharedModules = [
    ../.
    ../shell
    module_args
    inputs.nix-index-db.hmModules.nix-index
    inputs.hyprland.homeManagerModules.default
  ];

  homeImports = {
    "sil@t480s" = [./sil] ++ sharedModules;
    "work@zbook" = [./work] ++ [
      module_args
    ];
  };

  inherit (inputs.home-manager.lib) homeManagerConfiguration;
in {
  imports = [
    # we need to pass this to NixOS' HM module
    {_module.args = {inherit homeImports;};}
  ];

  flake = {
    homeConfigurations = withSystem "x86_64-linux" ({pkgs, ...}: {
      "sil@t480s" = homeManagerConfiguration {
        modules = homeImports."sil@t480s";
        inherit pkgs;
      };
      "work@zbook" = homeManagerConfiguration {
	      modules = homeImports."work@zbook";
        inherit pkgs;
      };
    });
    i18n.defaultLocale = "en_US.utf8";
    i18n.supportedLocales = ["all"];
  };
}
