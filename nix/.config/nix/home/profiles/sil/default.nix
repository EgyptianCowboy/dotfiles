{
  home = {
    username = "sil";
    homeDirectory = "/home/sil";
    stateVersion = "23.11";
  };
  
  programs.home-manager.enable = true;
  
  imports = [
    ../../editors/emacs
    ../../programs
    ../../wayland
    ../../terminals/foot.nix
  ];
}
