{ pkgs, inputs, ... }:

{
  home.packages = [
    inputs.ghostty.packages.${pkgs.system}.default
  ];

  # You can add Ghostty configuration here if needed
  # programs.ghostty = {
  #   enable = true;
  #   settings = {
  #     # Your Ghostty settings
  #   };
  # };
}
