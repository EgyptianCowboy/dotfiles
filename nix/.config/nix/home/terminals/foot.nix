{pkgs, ...}: {
  programs.foot = {
    # enable = true;
    settings = {
      main = {
        shell="fish";
        term = "xterm";
        font = "Berkeley Mono:size=12";
        dpi-aware = "yes";

        pad = "20x20";

        selection-target = "clipboard";
        box-drawings-uses-font-glyphs = "yes";
      };

      desktop-notifications = {
        command = "notify-send -a \${app-id} -i \${app-id} \${title} \${body}";
      };

      mouse = {
        hide-when-typing = "yes";
      };

      bell = {
        urgent = "no";
        notify = "yes";
      };

      cursor = {
        style = "beam";
        beam-thickness = 1;
      };

      colors = {
        alpha = "1.0";
        background = "ffffff";  # White background
        foreground = "000000";  # Black foreground

        ## Normal/regular colors (color palette 0-7)
        regular0 = "282828";  # black
        regular1 = "cc241d";  # red
        regular2 = "98971a";  # green
        regular3 = "d79921";  # yellow
        regular4 = "458588";  # blue
        regular5 = "b16286";  # magenta
        regular6 = "689d6a";  # cyan
        regular7 = "a89984";  # white

        ## Bright colors (color palette 8-15)
        bright0 = "928374";   # bright black
        bright1 = "fb4934";   # bright red
        bright2 = "b8bb26";   # bright green
        bright3 = "fabd2f";   # bright yellow
        bright4 = "83a598";   # bright blue
        bright5 = "d3869b";   # bright magenta
        bright6 = "8ec07c";   # bright cyan
        bright7 = "ebdbb2";   # bright white

        ## Dim colors (color palette 16-23)
        dim0 = "282828";   # dim black
        dim1 = "9d0006";   # dim red
        dim2 = "79740e";   # dim green
        dim3 = "b57614";   # dim yellow
        dim4 = "076678";   # dim blue
        dim5 = "8f3f71";   # dim magenta
        dim6 = "427b58";   # dim cyan
        dim7 = "7c6f64";   # dim white
      };
    };
  };
}
