{
  pkgs,
  inputs,
  ...
}: {
  programs.emacs = {
      enable = true;
      package = inputs.emacs.packages.${pkgs.system}.emacs-pgtk;
      extraPackages =  epkgs: [ epkgs.jinx epkgs.pdf-tools epkgs.auctex ];
    };
}
