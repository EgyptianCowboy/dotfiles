{
  ...
}: {
  programs.fish = {
      enable = true;

      shellAliases = {
        ip = "ip --color=auto";
        find = "fd";
        grep = "rg";
        diff = "colordiff";
        shutdown = "doas shutdown now";
        reboot = "doas reboot";
        timeline = "git log --decorate --oneline --graph";
      };
  };
}
