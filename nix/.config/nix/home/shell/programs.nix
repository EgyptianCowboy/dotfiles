{
  pkgs,
  config,
  ...
}:
{
  home.packages = with pkgs; [
    # archives
    zip
    unzip
    unrar

    # utils
    file
    du-dust
    duf
    fd
    ripgrep

  ];

  programs = {
    broot = {
      enable = true;
      enableFishIntegration = true;
      settings = {
        "default_flags"="hd";
        "cols_order"="scdgbn";
        verbs = [
          {
            invocation = "edit";
            key = "ctrl-e";
            shorcut = "e";
            execution = "$EDITOR {file}";
            leave_broot = false;
          }
          {
            invocation = "create {subpath}";
            execution = "$EDITOR {directory}/{subpath}";
            leave_broot = false;
          }
          {
            invocation = "git_diff";
            key = "enter";
            shorcut = "gd";
            execution = "git difftool -y {file}";
            leave_broot = false;
            apply_to = "file";
          }
          {
            invocation = "git_add";
            key = "ctrl-a";
            shorcut = "ga";
            execution = "git add {file}";
            leave_broot = false;
            apply_to = "file";
          }
          {
            invocation = "git_checkout";
            shorcut = "gco";
            execution = "git checkout {file}";
            leave_broot = false;
            apply_to = "file";
          }
        ];
      };
    };

    bat = {
      enable = true;
      config = {
        pager = "less -FR";
      };
    };

    btop.enable = true;
    eza.enable = true;
    ssh = {
      enable = true;
      matchBlocks = {
        "gitlab.com" = {
          hostname = "gitlab.com";
          user = "git";
          port = 22;
          identityFile = "~/.ssh/gitlab_personal";
          identitiesOnly = true;
        };
        "bitbucket.org" = {
          hostname = "bitbucket.org";
          user = "git";
          port = 22;
          identityFile = "~/.ssh/bitbucket_rot";
          identitiesOnly = true;
        };
        "github.com" = {
          hostname = "github.com";
          user = "git";
          port = 22;
          identityFile = "~/.ssh/github";
          identitiesOnly = true;
        };
        "rot_server" = {
          hostname = "172.16.3.2";
          user = "svaes";
          port = 22;
          identityFile = "~/.ssh/rot_server";
          identitiesOnly = true;
        };
        "sophia" = {
          hostname = "172.16.3.44";
          user = "sil";
          port = 22;
          identityFile = "~/.ssh/sophia";
          identitiesOnly = true;
        };
        "macca" = {
          hostname = "172.16.2.101";
          user = "rot";
          port = 22;
          identityFile = "~/.ssh/macca";
          identitiesOnly = true;
        };
      };
    };

    zoxide = {
      enable = true;
      enableFishIntegration = true;
    };

    skim = {
      enable = true;
      enableFishIntegration = true;
      defaultCommand = "rg --files --hidden";
      defaultOptions = [
        "--height 40%" "--prompt ⟫"
        "--color=light,fg:#000000,bg:#ffffff,bg+:#f0f0f0,info:#005f87,matched:#000000,matched_bg:#ffff00,current:#1a1a1a,current_bg:#d7d7d7,current_match:#1a1a1a,current_match_bg:#ffd700,query:#000000,query_bg:#f0f0f0,border:#888888,prompt:#000000,pointer:#000000,marker:#005f87,spinner:#005f87,header:#005f87"
      ];
      changeDirWidgetOptions = [
        "--preview 'eza --icons --git --color always -T -L 3 {} | head -200'"
        "--exact"
      ];
    };
  };
}
