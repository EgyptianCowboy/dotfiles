{config, ...}: {
  home.sessionVariables.STARSHIP_CACHE = "${config.xdg.cacheHome}/starship";

  programs.starship = {
    enable = true;
    # enableFishIntegration = true;
    enableNushellIntegration = true;
    settings = {
      character = {
        success_symbol = "[›](bold green)";
        error_symbol = "[›](bold red)";
      };

      git_status = {
        deleted = "✗";
        modified = "✶";
        staged = "✓";
        stashed = "≡";
      };

      nix_shell = {
        symbol = " ";
        heuristic = true;
      };
    };
  };
  
    # starship = { enable = true;
    #              settings = {
    #                add_newline = true;
    #                # character = { 
    #                  # success_symbol = "[➜](bold green)";
    #                  #error_symbol = "[➜](bold red)";
    #                 # };
    #              };
    #            };
}
