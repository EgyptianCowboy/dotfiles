{...}: {
  programs.zellij = {
    enable = true;
    enableFishIntegration = true;
    settings = {
      term = "xterm-256color";
    };
  };
}
