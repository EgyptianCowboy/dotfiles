{ ... }:
{
  imports = [
    ./programs.nix
    ./starship.nix
    ./fish.nix
    ./nushell
    ./nix.nix
    # ./zellij.nix
  ];

  # add environment variables
  home.sessionVariables = {
    EDITOR = "emacs";
  };
}
