{
  config,
  lib,
  ...
}:
# networking configuration
{
  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = lib.mkForce [ 5000 ];
      allowedUDPPorts = lib.mkForce [ 5000 ];
    };

    wireless.iwd = {
      enable = true;
      settings = {
        General = {
          UseDefaultInterface = false;
          NameResolvingService = "systemd";
        };
      };
    };
    useDHCP = true;
    nat.enable = true;
    nat.externalInterface = "wlan0";
  };

  services = {
    # network discovery, mDNS
    # avahi = {
    #   enable = true;
    #   nssmdns = true;
    #   publish = {
    #     enable = true;
    #     domain = true;
    #     userServices = true;
    #   };
    # };

    resolved = {
      enable = true;
      dnssec = "allow-downgrade";
      llmnr = "resolve";
      extraConfig = ''
      DNSOverTLS=opportunistic
      MulticastDNS=yes
    '';

      fallbackDns = [
        "8.8.8.8#dns.google"
        "1.1.1.1#cloudflare-dns.com"
        "9.9.9.9#dns.quad9.net"
        "2001:4860:4860::8888#dns.google"
        "2606:4700:4700::1111#cloudflare-dns.com"
        "2620:fe::9#dns.quad9.net"
      ];
    };
    openssh = {
      enable = true;
      settings.UseDns = true;
    };
  };

  systemd.network.networks = {
    "20-wired" = {
      matchConfig.Name = "enp*";
      networkConfig.DHCP = "yes";
    };
    "25-wireless" = {
      matchConfig.Name = "wl*";
      networkConfig = {
        DHCP = "yes";
        IgnoreCarriesLoss = "3s";
      };
    };
  };

  # Don't wait for network startup
  systemd.services.NetworkManager-wait-online.enable = lib.mkForce false;
}
