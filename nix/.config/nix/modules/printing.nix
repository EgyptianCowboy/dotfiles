{
  lib,
  pkgs,
  inputs,
  hardware,
  ...
}:
{
  # hardware.printers = {
  #   ensurePrinters = [
  #     {
  #       name = "HP_P2055dn";
  #       location = "Home";
  #       deviceUri = "http://192.168.178.2:631/printers/HP_P2055dn";
  #       model = "drv:///sample.drv/generic.ppd";
  #       ppdOptions = {
  #         PageSize = "A4";
  #       };
  #     }
  #   ];
  #   ensureDefaultPrinter = "HP_P2055dn";
  # };
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];
}
