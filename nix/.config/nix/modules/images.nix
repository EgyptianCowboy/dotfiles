{
  config,
  pkgs,
  self,
  inputs,
  lib,
  ...
}: {
  virtualisation.docker = {
    enable = true;
    rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };
}
