{
  pkgs,
  lib,
  self,
  inputs,
  ...
}: {
  # boot.plymouth = {
  #   enable = true;
  # };

      # Fonts
  fonts = {
    packages = with pkgs; [
      recursive
      go-font
      julia-mono
      
      iosevka-comfy.comfy
      iosevka-comfy.comfy-duo
      iosevka-comfy.comfy-fixed
      iosevka-comfy.comfy-motion
      iosevka-comfy.comfy-motion-fixed
      iosevka-comfy.comfy-motion-duo
      iosevka-comfy.comfy-wide
      iosevka-comfy.comfy-wide-motion
      iosevka-comfy.comfy-wide-motion-fixed
      roboto-mono

      roboto
      noto-fonts
      noto-fonts-emoji
      libertine
    ];

    fontconfig = {
      enable = true;
      # Fixes pixelation
      antialias = true;

      # Fixes antialiasing blur
      hinting = {
        enable = true;
        style = "slight"; # no difference
        autohint = false; # no difference
      };

      subpixel = {
        # Makes it bolder
        rgba = "rgb";
        lcdfilter = "default"; # no difference
      };

      defaultFonts = {
        serif = ["Linux Libertine Display O"];
        sansSerif = ["Roboto"];
        monospace = ["Berkeley Mono"];
        emoji = ["Noto Color Emoji"];
      };
      allowBitmaps = true;
    };
  };

  # use Wayland where possible (electron)
  environment.variables.NIXOS_OZONE_WL = "1";

  hardware = {
    pulseaudio.enable = lib.mkForce false;
  };

  # enable location service
  location.provider = "geoclue2";

  nix = {
    settings = {
      substituters = [
        "https://anyrun.cachix.org"
        "https://nix-gaming.cachix.org"
        "https://hyprland.cachix.org"
      ];
      trusted-public-keys = [
        "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
        "anyrun.cachix.org-1:pqBobmOjI7nKlsUMV25u9QHa9btJK65/C8vnO3p346s="
      ];
    };
  };

  programs = {
    # make HM-managed GTK stuff work
    dconf.enable = true;
  };

  qt = {
    enable = true;
    platformTheme = "gtk2";
    style = "gtk2";
  };

  services = {
    logind.extraConfig = ''
      RuntimeDirectorySize=2G  
      HandleLidSwitch=suspend
      HandlePowerKey=suspend
    '';

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      jack.enable = true;
      pulse.enable = true;
    };

    tlp = {
      enable = true;
      settings = {
        START_CHARGE_THRESH_BAT0=75;
        STOP_CHARGE_THRESH_BAT0=80;

        CPU_SCALING_GOVERNOR_ON_AC="schedutil";
        CPU_SCALING_GOVERNOR_ON_BAT="powersave";

        CPU_SCALING_MIN_FREQ_ON_AC=800000;
        CPU_SCALING_MAX_FREQ_ON_AC=3500000;
        CPU_SCALING_MIN_FREQ_ON_BAT=800000;
        CPU_SCALING_MAX_FREQ_ON_BAT=2300000;

        CPU_ENERGY_PERF_POLICY_ON_AC="balance_performance";
        CPU_ENERGY_PERF_POLICY_ON_BAT="balance_power";

        CPU_BOOST_ON_AC=1;
        CPU_BOOST_ON_BAT=0;

        # Enable audio power saving for Intel HDA, AC97 devices (timeout in secs).
        # A value of 0 disables, >=1 enables power saving (recommended: 1).
        # Default: 0 (AC), 1 (BAT);
        SOUND_POWER_SAVE_ON_AC=0;
        SOUND_POWER_SAVE_ON_BAT=1;

        # Runtime Power Management for PCI(e) bus devices: on=disable, auto=enable.
        # Default: on (AC), auto (BAT)
        RUNTIME_PM_ON_AC="on";
        RUNTIME_PM_ON_BAT="auto";

        # Battery feature drivers: 0=disable, 1=enable
        # Default: 1 (all)
        NATACPI_ENABLE=1;
        TPACPI_ENABLE=1;
        TPSMAPI_ENABLE=1;
      };
    };

    # battery info & stuff
    upower.enable = true;
    
    dbus.enable = true;
    # needed for GNOME services outside of GNOME Desktop
    dbus.packages = [pkgs.gcr];
  };

  security = {
    polkit.enable = true;
    sudo.enable=true;
    rtkit.enable = true;

    pam.services.swaylock = {
      text = "auth include login";
    };
    doas = {
      enable=true;
      extraRules = [{
        users = [ "sil" ];
        keepEnv = true;
      }];
    };
  };

  xdg.portal = {
    enable = true;
    extraPortals = [pkgs.xdg-desktop-portal-gtk];
  };
}
