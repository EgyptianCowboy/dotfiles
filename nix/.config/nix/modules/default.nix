{
  self,
  inputs,
  default,
  ...
}: let
  # system-agnostic args
  module_args._module.args = {
    inherit default inputs self;
  };
in {
  imports = [
    {
      _module.args = {
        # we need to pass this to HM
        inherit module_args;

        # NixOS modules
        sharedModules = [
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
            };
          }

          inputs.home-manager.nixosModule
          inputs.hyprland.nixosModules.default
          module_args

          self.nixosModules.core
          self.nixosModules.network
          self.nixosModules.nix
          self.nixosModules.printing
          ./security.nix
        ];
      };
    }
  ];

  flake.nixosModules = {
    core = import ./core.nix;
    bluetooth = import ./bluetooth.nix;
    desktop = import ./desktop.nix;
    greetd = import ./greetd.nix;
    network = import ./network.nix;
    nix = import ./nix.nix;
    images = import ./images.nix;
    printing = import ./printing.nix;
  };
}
