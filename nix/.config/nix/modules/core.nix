{
  pkgs,
  lib,
  ...
}:
# configuration shared by all hosts
{
  documentation.dev.enable = true;

  # enable zsh autocompletion for system packages (systemd, etc)
  # environment.pathsToLink = ["/share/fish"];

  i18n = {
    defaultLocale = "en_US.UTF-8";
    # saves space
    supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "nl_BE.UTF-8/UTF-8"
    ];
  };

  # don't ask for password for wheel group
  security.sudo.wheelNeedsPassword = false;

  # don't touch this
  system.stateVersion = lib.mkDefault "23.05";

  time.timeZone = lib.mkDefault "Europe/Brussels";

  users.users.sil = {
    isNormalUser = true;
    shell = pkgs.fish;
    # shell = pkgs.nushell;
    extraGroups = ["adbusers" "input" "libvirtd" "networkmanager" "plugdev" "transmission" "video" "wheel" "docker"];
  };
  
  programs.fish.enable = true;
  # programs.nushell.enable = true;
  # compresses half the ram for use as swap
  # zramSwap.enable = true;
}
