{
  lib,
  pkgs,
  inputs,
  ...
}:
# greetd display manager
let
    hypr-run = pkgs.writeShellScriptBin "hypr-run" ''
    systemd-run --user --scope --collect --quiet --unit="hyprland" \
        systemd-cat --identifier="hyprland" ${pkgs.hyprland}/bin/Hyprland $@

    ${pkgs.hyprland}/bin/hyperctl dispatch exit
  '';
in {
  services.greetd = {
      enable = true;
      restart = false;
      settings = {
        default_session = {
          command = ''
            ${
              lib.makeBinPath [ pkgs.greetd.tuigreet ]
            }/tuigreet -r --asterisks --time \
              --cmd ${lib.getExe hypr-run}
                '';
        };
      };  
  };

  programs.hyprland.enable = true;
}
