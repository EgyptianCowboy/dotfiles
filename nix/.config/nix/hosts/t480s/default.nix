{
  config,
  inputs,
  pkgs,
  ...
}: {
  imports = [./hardware-configuration.nix];
  
  environment.systemPackages = [config.boot.kernelPackages.cpupower];
  
  networking = {
    hostName = "t480s"; # Define your hostname.
    wireless = {
      iwd = {        
        enable = true;
        settings = {
          General = {
            UseDefaultInterface = false;
            NameResolvingService = "systemd";
          };
        };
      };
    };
    
    useDHCP = true;
    # useNetworkd = true;
    firewall = {
      enable = true;
    };
    nat.enable = true;
    nat.externalInterface = "wlan0";
    # extraHosts =
    #   ''
    #   128.140.99.95 kube-master1
    #   128.140.99.95 kube-master1-root
    #   94.130.169.220 kube-worker1
    #   128.140.106.93 kube-worker2
    #   ''; 
  };

  services.resolved = {
    enable = true;
    dnssec = "allow-downgrade";
    llmnr = "resolve";
    extraConfig = ''
      DNSOverTLS=opportunistic
      MulticastDNS=yes
    '';

    fallbackDns = [
      "8.8.8.8#dns.google"
      "1.1.1.1#cloudflare-dns.com"
      "9.9.9.9#dns.quad9.net"
      "2001:4860:4860::8888#dns.google"
      "2606:4700:4700::1111#cloudflare-dns.com"
      "2620:fe::9#dns.quad9.net"
    ];
  };

  systemd.network.networks = {
  "20-wired" = {
    matchConfig.Name = "enp*";
    networkConfig.DHCP = "yes";
    };
  "25-wireless" = {
    matchConfig.Name = "wl*";
    networkConfig = {
      DHCP = "yes";
      IgnoreCarriesLoss = "3s";
      };
    };
  };

  programs = {
    # enable hyprland and required options
    hyprland.enable = true;
  };

  services = {
    # for SSD/NVME
    fstrim.enable = true;
  };
}
