{
  inputs,
  sharedModules,
  homeImports,
  ...
}: {
  flake.nixosConfigurations = let
    inherit (inputs.nixpkgs.lib) nixosSystem;
  in {
    t480s = nixosSystem {
      modules =
        [
          ./t480s
          ../modules/bluetooth.nix
          ../modules/greetd.nix
          ../modules/desktop.nix
          ../modules/images.nix
          {home-manager.users.sil.imports = homeImports."sil@t480s";}
        ]
        ++ sharedModules;
    };
    zbook = nixosSystem {
      modules = 
        [
          {home-manager.users.sil.imports = homeImports."sil@zbook";}
        ];
    };
  };
}
