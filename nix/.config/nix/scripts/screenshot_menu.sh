#!/bin/sh

screenshot_copy_all_displays="Screenshot all displays to clipboard"
screenshot_all_displays_to_file="Screenshot all displays to file"
screenshot_copy_area="Screenshot area to clipboard"
screenshot_copy_area_ocr="Screenshot area to copy text"
screenshot_area_to_file="Screenshot area to file"
screenshot_copy_window="Screenshot focused window to clipboard"
screenshot_window_to_file="Screenshot focused window to file"
screenshot_copy_monitor="Screenshot focused monitor to clipboard"
screenshot_monitor_to_file="Screenshot focused monitor to file"

# Store each option in a single string seperated by newlines.
options="$screenshot_copy_all_displays\n"
options+="$screenshot_all_displays_to_file\n"
options+="$screenshot_copy_area\n"
options+="$screenshot_copy_area_ocr\n"
options+="$screenshot_area_to_file\n"
options+="$screenshot_copy_window\n"
options+="$screenshot_window_to_file\n"
options+="$screenshot_copy_monitor\n"
options+="$screenshot_monitor_to_file"

# Prompt the user with wofi.
choice="$(echo -e "$options" | fuzzel -d)"

# Make sure that all pictures are saved in the screenshots folder.
cd ~/Pictures/Screenshots
file="$HOME/Pictures/Screenshots/$(date '+%y%m%d_%H-%M-%S').png"

case $choice in
    $screenshot_copy_all_displays)
        grim - | wl-copy
        notify-send 'Screenshot copied to clipboard'
        ;;
    $screenshot_all_displays_to_file)
        grim "$file"
        notify-send --icon "$file" 'Screenshot Saved to "$file"'
        ;;
    $screenshot_copy_area)
        grim -g "$(slurp)" - | wl-copy
        notify-send 'Screenshot copied to clipboard'
        ;;
    $screenshot_copy_area_ocr)
        grim -g "$(slurp)" - | tesseract - - | wl-copy
        notify-send 'Text copied to clipboard'
        ;;
    $screenshot_area_to_file)
        grim -g "$(slurp)" "$file"
        notify-send --icon "$file" 'Screenshot Saved'
        ;;
    $screenshot_copy_window)
        grim -g "$(printf "%d,%d %dx%d\n" $(hyprctl activewindow -j | jq '.at[0],.at[1],.size[0],.size[1]'))" - | wl-copy
        notify-send 'Screenshot copied to clipboard'
        ;;
    $screenshot_window_to_file)
        grim -g "$(printf "%d,%d %dx%d\n" $(hyprctl activewindow -j | jq '.at[0],.at[1],.size[0],.size[1]'))" "$file"
        notify-send --icon "$file" 'Screenshot Saved to "$file"'
        ;;
    $screenshot_copy_monitor)
        grim -o $(hyprctl monitors -j | jq -r '.[] | select(.focused) | .name') - | wl-copy
        notify-send 'Screenshot copied to clipboard'
        ;;
    $screenshot_monitor_to_file)
        grim -o $(hyprctl monitors -j | jq -r '.[] | select(.focused) | .name') "$HOME/Pictures/Screenshot/$(date '+%y%m%d_%H-%M-%S').png"
        notify-send --icon "$file" 'Screenshot Saved to "$file"'
        ;;
esac
