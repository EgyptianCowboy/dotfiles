;;; nix.el --- -*- lexical-binding: t; -*-

(use-package nix-mode)

(provide 'nix)
;;; nix.el ends here
