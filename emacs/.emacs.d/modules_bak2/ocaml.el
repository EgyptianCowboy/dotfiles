;;; ocaml.el --- -*- lexical-binding: t; -*-

;; (use-package ocaml-ts-mode
;;   :ensure (ocaml-ts-mode :type git
;; 			 :host github
;; 			 :repo "terrateamio/ocaml-ts-mode"
;; 			 :branch "main"))

(use-package tuareg
  ;; :custom
  ;; (major-mode-remap-alist
  ;;  '((tuareg-mode . ocaml-ts-mode)))
  )

(use-package utop
  :bind
  ;; (:map ocaml-ts-mode-map
  ;;       ("C-x C-e" . utop-eval-phrase)
  ;;       ("C-c C-b" . utop-eval-buffer)
  ;;       ("C-c C-r" . utop-eval-region))
   (:map tuareg-mode-map
        ("C-x C-e" . utop-eval-phrase)
        ("C-c C-b" . utop-eval-buffer)
        ("C-c C-r" . utop-eval-region))
  :hook
  ;; (ocaml-ts-mode . utop-minor-mode)
  (tuareg-mode . utop-minor-mode)
  :custom
  (utop-command "dune utop . -- -emacs"))

(provide 'ocaml)
;;; ocaml.el ends here
