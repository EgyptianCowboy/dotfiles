;;; rusty.el --- -*- lexical-binding: t; -*-

(use-package rust-mode
  :init
  (setq rust-mode-treesitter-derive t))

(provide 'rusty)
;;; rusty.el ends here
