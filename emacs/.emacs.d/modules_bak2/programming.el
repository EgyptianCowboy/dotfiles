;;; programming.el --- -*- lexical-binding: t; -*-

(use-package treesit
  :ensure nil
  :commands (treesit-install-language-grammar sil-treesit-install-all-languages)
  :custom
  (treesit-language-source-alist
   '((bash . ("https://github.com/tree-sitter/tree-sitter-bash"))
     (c . ("https://github.com/tree-sitter/tree-sitter-c"))
     (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"))
     (json . ("https://github.com/tree-sitter/tree-sitter-json"))
     (ocaml . ("https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src"))
     (ocaml-interface . ("https://github.com/tree-sitter/tree-sitter-ocaml" "master" "interface/src"))
     (python . ("https://github.com/tree-sitter/tree-sitter-python"))
     (toml . ("https://github.com/tree-sitter/tree-sitter-toml"))
     (go . ("https://github.com/tree-sitter/tree-sitter-go"))
     (gomod . ("https://github.com/camdencheek/tree-sitter-go-mod"))
     (rust . ("https://github.com/tree-sitter/tree-sitter-rust"))
     ((cmake . ("https://github.com/uyha/tree-sitter-cmake")))))
  :preface
  (defun sil-treesit-install-all-languages ()
    "Install all languages specified by `treesit-language-source-alist'."
    (interactive)
    (let ((languages (mapcar 'car treesit-language-source-alist)))
      (dolist (lang languages)
	    (treesit-install-language-grammar lang)
	    (message "`%s' parser was installed." lang)
	    (sit-for 0.75))))
  :init
  (dolist (mapping
           '((python-mode . python-ts-mode)
             (c++-mode . c++-ts-mode)
             (c-mode . c-ts-mode)
             (bash-mode . bash-ts-mode)
             (json-mode . json-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping)))

(use-package eglot
  :hook ((c-mode . eglot-ensure) ;; Autostart lsp servers for a given mode
         (c++-ts-mode . eglot-ensure)
         (cmake-ts-mode . eglot-ensure)
         (tuareg-mode . eglot-ensure)
         (go-mode . eglot-ensure)
         (rust-ts-mode . eglot-ensure)
         (eglot-managed-mode . eglot-inlay-hints-mode))
  :general (:keymaps 'eglot-mode-map
                     "C-c C-g" 'transient-eglot)
  :custom
  ;; Good default
  (eglot-events-buffer-size 0) ;; No event buffers (Lsp server logs)
  (eglot-autoshutdown t);; Shutdown unused servers.
  (fset #'jsonrpc--log-event #'ignore)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-sync-connect nil)
  (eglot-send-changes-idle-time 3)
  (flymake-no-changes-timeout 5)
  (eglot-connect-timeout nil)
  (eldoc-echo-area-use-multiline-p nil)
  :config
  (add-to-list 'eglot-server-programs '((gleam-ts-mode) . ("gleam" "lsp")))
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster))

(use-package eglot-booster
  :ensure (eglot-booster :type git :host github :repo "jdtsmith/eglot-booster")
  :after eglot
  :config
  (eglot-booster-mode))

(use-package markdown-mode)

(use-package flymake
  :ensure nil
  :general (:keymaps 'flymake-mode-map
                     "C-c f" 'transient-flymake)
  :hook
  ((prog-mode . flymake-mode)))

(use-package flymake-popon
  :ensure (flymake-popon
           :type git
           :repo "https://codeberg.org/akib/emacs-flymake-popon.git")
  :hook (flymake-mode . flymake-popon-mode))

(use-package eldoc-box
  :ensure (eldoc-box :type git
                     :host github
                     :repo "casouri/eldoc-box")
  :hook ((eglot-managed-mode . eldoc-box-hover-mode)
         (lspce-mode . eldoc-box-hover-mode))
  :custom-face
  (eldoc-box-body ((t (:inherit 'variable-pitch)))))

(use-package beardbolt
  :ensure (beardbolt :type git
                     :host github
                     :repo "joaotavora/beardbolt"))

(use-package display-line-numbers
  :ensure nil
  :hook (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-major-tick 0)
  (display-line-numbers-minor-tick 0)
  (display-line-numbers-grow-only t)
  (display-line-numbers-type t)
  (display-line-numbers-width 2)
  (display-line-numbers-widen t))

(use-package indent-bars
  :ensure (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :custom
  (indent-bars-treesit-support t)
  (indent-bars-no-descend-string t)
  (indent-bars-treesit-ignore-blank-lines-types '("module"))
  (indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
				      list list_comprehension
				      dictionary dictionary_comprehension
				      parenthesized_expression subscript)))
  (indent-bars-color '(highlight :face-bg t :blend 0.15))
  (indent-bars-pattern ".")
  (indent-bars-width-frac 0.1)
  (indent-bars-pad-frac 0.1)
  (indent-bars-zigzag nil)
  (indent-bars-color-by-depth '(:regexp "outline-\\([0-9]+\\)" :blend 1)) ; blend=1: blend with BG only
  (indent-bars-highlight-current-depth '(:blend 0.5)) ; pump up the BG blend on current
  (indent-bars-display-on-blank-lines t)
  :hook (prog-mode . indent-bars-mode)) ; or whichever modes you prefer


(use-package compile
  :ensure nil
  :custom
  (compilation-scroll-output 'first-error)
  (compilation-always-kill t)
  (compilation-max-output-line-length nil)
  :hook (compilation-mode . hl-line-mode)
  :init
  (add-hook 'compilation-buffer-function
            (lambda (buf str)
              (if (null (string-match ".*exited abnormally.*" str))
                  (progn
                    (run-at-time
                     "1 sec" nil 'delete-window-on
                     (get-buffer-create "*compilation*"))
                    (message "No Compilation Errors!"))))))

(use-package fancy-compilation
  :ensure t
  :init
  (fancy-compilation-mode)
  :custom
  (fancy-compilation-scroll-output 'first-error)
  (fancy-compilation-override-colors nil)
  (fancy-compilation-term "xterm-256color"))

(use-package yaml-mode)

(provide 'programming)
;;; programming.el ends here
