;;; dired-config.el --- -*- lexical-binding: t; -*-

(use-package dired
  :ensure nil
  :hook
  ((dired-mode . dired-hide-details-mode))
  :custom
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (delete-by-moving-to-trash t)
  (dired-listing-switches
   "-AGFhlv --group-directories-first --time-style=long-iso")
  (dired-dwim-target t)
  (dired-auto-revert-buffer #'dired-directory-changed-p) ; also see `dired-do-revert-buffer'
  (dired-make-directory-clickable t) ; Emacs 29.1
  (dired-free-space nil) ; Emacs 29.1
  (dired-mouse-drag-files t) ; Emacs 29.1
  (dired-guess-shell-alist-user ; those are the suggestions for ! and & in Dired
   '(("\\.\\(png\\|jpe?g\\|tiff\\)" "feh" "xdg-open")
     ("\\.\\(mp[34]\\|m4a\\|ogg\\|flac\\|webm\\|mkv\\)" "mpv" "xdg-open")
     (".*" "xdg-open"))))

(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package dired-filter)

(use-package dired-aux
  :ensure nil
  :bind (:map global-map
              ("C-+" . dired-create-empty-file)
              ("M-s f" . nil)
              ("C-x v v" . dired-vc-next-action))
  :custom
  (dired-isearch-filenames 'dwim)
  (dired-create-destination-dirs 'ask) ; Emacs 27
  (dired-vc-rename-file t)             ; Emacs 27
  (dired-do-revert-buffer (lambda (dir) (not (file-remote-p dir)))) ; Emacs 28
  (dired-create-destination-dirs-on-trailing-dirsep t) ; Emacs 29
  )

(use-package dired-x
  :ensure nil
  :bind (:map dired-mode-map
              ("I" . dired-info))
  :custom
  (dired-clean-up-buffers-too t)
  (dired-clean-confirm-killing-deleted-buffers t)
  (dired-x-hands-off-my-keys t)    ; easier to show the keys I use
  (dired-bind-man nil)
  (dired-bind-info nil))

(use-package dired-subtree
  :bind (:map dired-mode-map
              ("<tab>" . dired-subtree-toggle)
              ("<backtab>" . dired-subtree-remove))
  :custom
  (dired-subtree-use-backgrounds nil))

(use-package wdired
  :ensure nil
  :custom
  (wdired-allow-to-change-permissions t)
  (wdired-create-parent-directories t))

;; Addtional syntax highlighting for dired
(use-package diredfl
  :hook
  ((dired-mode . diredfl-mode))
  :config
  (set-face-attribute 'diredfl-dir-name nil :bold t))

(provide 'dired-config)
;;; dired-config.el ends here
