;;; defaults.el --- -*- lexical-binding: t; -*-

(use-package emacs
  :ensure nil  :preface
  (defun config-visit ()
    (interactive)
    (find-file (concat emacs-home "init.el")))

  (defun split-and-follow-h ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))

  (defun split-and-follow-v ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))

  (defun ol/split-window-right ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'right nil))

  (defun ol/split-window-below ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'below nil))
  :custom
  (delete-selection-mode t)   ;; Select text and delete it by typing.
  (electric-indent-mode t)  ;; Turn off the weird indenting that Emacs does by default.
  (electric-pair-mode t)      ;; Turns on automatic parens pairing

  (blink-cursor-mode nil)     ;; Don't blink cursor
  (global-auto-revert-mode t) ;; Automatically reload file and show changes if the file has changed

  (tab-width 4)
  (indent-tabs-mode nil)

  ;; Revert Dired and other buffers
  (global-auto-revert-non-file-buffers t)

  (yank-pop-change-selection t)
  (kill-whole-line t)
  (track-eol t) ; Keep cursor at end of lines.
  (line-move-visual nil) ; To be required by track-eol
  ;; Update UI less frequently
  (idle-update-delay 1.0)
  ;; Do not saves duplicates in kill-ring
  (kill-do-not-save-duplicates t)

  ;; Scrolling
  (auto-window-vscroll nil)
  (fast-but-imprecise-scrolling t)
  (scroll-conservatively 101)
  (scroll-margin 0)
  (hscroll-margin 0)
  (scroll-preserve-screen-position t)
  (pixel-scroll-precision-mode 1)

  (mouse-wheel-scroll-amount
   '(1
	 ((shift) . 5)
	 ((meta) . 0.5)
	 ((control) . text-scale)))
  (mouse-drag-copy-region nil)
  (make-pointer-invisible t)
  (mouse-wheel-progressive-speed t)
  (mouse-wheel-follow-mouse t)
  (mouse-wheel-mode 1)

  ;; Better support for files with long lines
  (bidi-paragraph-direction 'left-to-right)
  (bidi-inhibit-bpa t)
  (indicate-buffer-boundaries nil) ;  Don't show where buffer starts/ends
  (indicate-empty-lines nil)
  (apropos-do-all t)
  (global-so-long-mode 1)
  (global-subword-mode 1)

  ;; UI
  (frame-resize-pixelwise t)
  (visible-bell nil)
  (ring-bell-function 'ignore)
  ;; Disable system-wide dialogs
  (use-file-dialog nil)
  (use-dialog-box nil)                ; Avoid GUI dialogs
  (x-gtk-use-system-tooltips nil)     ; Do not use GTK tooltips
  (epa-pinentry-mode 'loopback)

  ;; Parens
  (show-paren-mode t)
  (show-paren-context-when-offscreen 'child-frame)
  (show-paren-when-point-in-periphery t)
  (show-paren-when-point-inside-paren t)

  ;; Backups and lock files
  (create-lockfiles nil)
  (make-backup-files nil)
  :config
  (defalias 'yes-or-no-p 'y-or-n-p)
  :general (
            "<insert>" nil
	        "M-SPC" 'cycle-spacing
            "C-x k" 'kill-this-buffer
	        "C-c o" 'occur
	        "C-z" 'nil
	        "C-c C-z" 'nil
	        "C-x C-z" 'nil
            "C-x C-c" 'nil
	        "C-h h" 'nil
	        "C-c e" 'config-visit
	        [remap split-window-below] 'split-and-follow-h
	        [remap split-window-right] 'split-and-follow-v))

(use-package display-line-numbers
  :ensure nil
  :init 
  (global-display-line-numbers-mode t)
  :custom
  (display-line-numbers-major-tick 0)
  (display-line-numbers-minor-tick 0)
  (display-line-numbers-grow-only t)
  (display-line-numbers-type t)
  (display-line-numbers-width 2)
  (display-line-numbers-widen t))

;;;; Direnv
(use-package envrc
  :init (envrc-global-mode 1))

;;;; Which key
(use-package which-key
  :init
  (which-key-mode 1)
  :custom
  (which-key-side-window-location 'bottom)
  (which-key-sort-order #'which-key-key-order-alpha) ;; Same as default, except single characters are sorted alphabetically
  (which-key-sort-uppercase-first nil)
  (which-key-add-column-padding 1) ;; Number of spaces to add to the left of each column
  (which-key-min-display-lines 6)  ;; Increase the minimum lines to display, because the default is only 1
  (which-key-idle-delay 0.8)       ;; Set the time delay (in seconds) for the which-key popup to appear
  (which-key-max-description-length 25))

;;;; No littering
(use-package no-littering
  :custom
  (no-littering-etc-directory
   (expand-file-name "config/" user-emacs-directory))
  (no-littering-var-directory
   (expand-file-name "data/" user-emacs-directory))
  :init
  (require 'no-littering))

;;;; Recentf
(use-package recentf
  :ensure nil
  :after no-littering
  :custom
  (recentf-save-file (locate-user-emacs-file "var/recentf-save.el"))
  (recentf-max-saved-items 1000)
  (recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  (recentf-auto-cleanup 30)
  (recentf-max-menu-items 50)
  (recentf-additional-variables '(kill-ring search-ring regexp-search-ring))
  :init
  (recentf-mode 1)
  :config
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  (run-with-idle-timer 30 t 'recentf-save-list))

;;;; Better comments
(use-package comment-dwim-2
  :bind (:map global-map
              ([remap comment-dwim] . comment-dwim-2)))

;;;; Unique buffer names
(use-package uniquify
  :ensure nil
  :custom
  (uniquify-buffer-name-style 'reverse)
  (uniquify-separator " • ")
  (uniquify-after-kill-buffer-p t)
  (uniquify-ignore-buffers-re "^\\*")
  (uniquify-strip-common-suffix t)
  (uniquify-after-kill-buffer-p t))

;;;; GC
;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
;; Increase the amount of data which Emacs reads from the process
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; Popup windows
(use-package popper
  :ensure t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :custom
  (popper-reference-buffers
   '("\\*compilation\\*" compilation-mode
     "\\*Cargo Run\\*" cargo-process-mode
     "^\\*vterm\\*" vterm-mode
     "^\\*Flymake.*" flymake-mode
     "^\\*Flycheck.*" flycheck-error-list-mode
     "^\\*lsp-help\\*" lsp-help-mode
     "^\\*eldoc\\*" special-mode
     "^\\*ert\\*" ert-results-mode
     "^\\*HTTP Response\\*" javascript-mode
     "^\\*SQL.*" sql-interactive-mode
     "^\\*cargo-test\\*" cargo-test-mode
     "^\\*cargo-run\\*" cargo-run-mode
     "^\\*rustic-compilation\\*" rustic-compilation-mode
       "\\*Messages\\*"
     "Output\\*$"
     "\\*Async Shell Command\\*"
     "\\*Go-Translate\\*"
     ("^\\*Warnings\\*$" . hide)
     help-mode
     helpful-mode))
  :init
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

;;; Whitespace

;; (use-package whitespace
;;   :ensure nil
;;   :custom
;;   (whitespace-style (quote
;;                      (spaces tabs newline space-mark tab-mark newline-mark)))

;;   (whitespace-display-mappings
;;    '(
;;      (space-mark   ?\     [? ]) ;;;use space not dotimes
;;      (space-mark   ?\xA0  [?\u00A4]     [?_])
;;      (space-mark   ?\x8A0 [?\x8A4]      [?_])
;;      (space-mark   ?\x920 [?\x924]      [?_])
;;      (space-mark   ?\xE20 [?\xE24]      [?_])
;;      (space-mark   ?\xF20 [?\xF24]      [?_])
;;      ;;(newline-mark ?\n    [? ?\n])
;;      (tab-mark     ?\t    [?\u00BB ?\t] [?\\ ?\t])
;;                                         ;(tab-mark 9 [9655 9] [92 9]) ; tab
;;      ))
;;   :config
;;   (global-whitespace-mode 1))

(provide 'defaults)
;;; defaults.el ends here
