;;; scheme-config.el --- -*- lexical-binding: t; -*-

(use-package geiser)
(use-package geiser-guile)

(provide 'scheme-config)
;;; scheme-config.el ends here
