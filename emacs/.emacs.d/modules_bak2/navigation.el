;;; navigation.el --- -*- lexical-binding: t; -*-

(use-package avy
  :after transient
  :general ("M-g" 'transient-avy))

(use-package windmove
  :ensure nil
  :general ("C-c C-w" 'transient-window))

(provide 'navigation)
;;; navigation.el ends here
