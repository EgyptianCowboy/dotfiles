;;; projects.el --- -*- lexical-binding: t; -*-

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package treemacs
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode 'always)
  (when treemacs-python-executable
    (treemacs-git-commit-diff-mode t))

  (pcase (cons (not (null (executable-find "git")))
               (not (null treemacs-python-executable)))
    (`(t . t)
     (treemacs-git-mode 'deferred))
    (`(t . _)
     (treemacs-git-mode 'simple)))

  (treemacs-hide-gitignored-files-mode t)
  :general ("M-0"       'treemacs-select-window
            "C-x t 1"   'treemacs-delete-other-windows
            "C-x t t"   'treemacs
            "C-x t d"   'treemacs-select-directory
            "C-x t B"   'treemacs-bookmark
            "C-x t C-t" 'treemacs-find-file
            "C-x t M-t" 'treemacs-find-tag))

(use-package treemacs-magit
  :after (treemacs magit))

(provide 'projects)
;;; projects.el ends here
