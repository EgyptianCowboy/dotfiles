;;; visual.el --- -*- lexical-binding: t; -*-

;;; Themes
;; (use-package modus-themes
;;   :init
;;   (load-theme 'modus-operandi-tinted t))

(use-package ef-themes
  :init (load-theme 'ef-reverie t))

;;; Padding
(use-package spacious-padding
  :custom
  ;; (spacious-padding-subtle-mode-line t)
  (spacious-padding-subtle-mode-line
   `( :mode-line-active 'default
      :mode-line-inactive vertical-border))
  (spacious-padding-widths
   '( :internal-border-width 15
      :header-line-width 4
      :mode-line-width 6
      :tab-width 4
      :right-divider-width 15
      :scroll-bar-width 0
      :fringe-width 8))
  :init
  (spacious-padding-mode t))

;;; Font
(set-face-attribute 'default nil
                    :family "PragmataPro Liga" :height 110)
(set-face-attribute 'fixed-pitch nil
                    :family "PragmataPro Liga")
(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Etoile"
                    :height 110)

(customize-set-variable 'inhibit-compacting-font-caches t)
(customize-set-variable 'x-underline-at-descent-line t)
(customize-set-variable 'underline-minimum-offset 0)
(customize-set-variable 'line-spacing 0)
(customize-set-variable 'text-scale-remap-header-line t)

;;; Modeline
(use-package sil-modeline
  :ensure nil
  :custom
  (mode-line-compact nil) ; Emacs 28
  (mode-line-right-align-edge 'right-margin) ; Emacs 30
  :config
  (setq-default mode-line-format
                '("%e"
                  sil-buffer-status
                  sil-buffer-identification
                  "  "
                  sil-major-mode
		  mode-line-format-right-align ; Emacs 30
                  sil-eglot
                  "  "
                  sil-flymake)))

(provide 'visual)
;;; visual.el ends here
