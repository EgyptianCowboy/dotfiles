;;; sepples.el --- C++ settings                      -*- lexical-binding: t; -*-

(use-package cmake-mode
  :hook (cmake-mode . cmake-ts-mode))

(use-package cmake-integration
  :ensure (cmake-integration :host github
                             :type git
                             :repo "darcamo/cmake-integration"
                             :branch "main")
  :bind (:map c++-ts-mode-map
              ([M-f9] . cmake-integration-save-and-compile) ;; Ask for the target name and compile it
              ([f9] . cmake-integration-save-and-compile-last-target) ;; Recompile the last target
              ([M-f10] . cmake-integration-run-last-target-with-arguments) ;; Ask for command line parameters to run the program
              ([f10] . cmake-integration-run-last-target) ;; Run the program (possible using the last command line parameters)
              ([M-f8] . cmake-integration-cmake-configure-with-preset) ;; Ask for a preset name and call CMake
              ([f8] . cmake-integration-cmake-reconfigure) ;; Call CMake with the last chosen preset
              )
  )

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cppm\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cxx\\'" . c++-mode))

(provide 'sepples)
;;; sepples.el ends here
