;;; completion.el --- -*- lexical-binding: t; -*-

(use-package emacs
  :ensure nil
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  :custom
  ;; Do not allow the cursor in the minibuffer prompt
  (minibuffer-prompt-properties
   '(read-only t cursor-intangible t face minibuffer-prompt))
  ;; Enable recursive minibuffers
  (enable-recursive-minibuffers t)
  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (read-extended-command-predicate
   #'command-completion-default-include-p))

(use-package orderless
  :custom
  ;; Configure a custom style dispatcher (see the Consult wiki)
  (orderless-style-dispatchers '(orderless-affix-dispatch))
  ;; (orderless-style-dispatchers '(+orderless-consult-dispatch orderless-affix-dispatch))
  (orderless-component-separator #'orderless-escapable-split-on-space)
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion))))
  (read-file-name-completion-ignore-case t)
  (read-buffer-completion-ignore-case t)
  (completion-ignore-case t))

(use-package vertico
  :ensure (vertico :host github
                   :repo "minad/vertico"
                   :files (:defaults "extensions/*")
                   :main "vertico.el"
		   :includes (vertico-directory))
  :general
  (:keymaps 'vertico-map
            "<tab>" 'vertico-insert
	    "<escape>" 'minibuffer-keyboard-quit
	    "C-k" 'vertico-exit
	    "C-n" 'vertico-next
	    "C-p" 'vertico-previous
	    "M-RET" 'vertico-exit-input

	    "RET" 'vertico-directory-enter
	    "<backspace>" 'vertico-directory-delete-char
	    "C-w" 'vertico-directory-delete-word
	    "C-<backspace>" 'vertico-directory-delete-word
	    "DEL" 'vertico-directory-delete-word
	    "M-d" 'vertico-directory-delete-char)
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :custom
  (vertico-scroll-margin 0)
  (vertico-count 10)
  :init
  (vertico-mode t))

(use-package marginalia
  :init
  (marginalia-mode t))

(use-package consult
  :general
  ([remap isearch-forward] 'consult-line
   [remap switch-to-buffer] 'consult-buffer
   [remap Info-search] 'consult-info
   [remap isearch-backward] 'consult-recent-file
   [remap yank-pop] 'consult-yank-pop)
  :custom
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref))

(use-package corfu
  :general
  ("M-i" 'completion-at-point)
  (:keymaps 'corfu-map
            [tab] 'corfu-next
            [backtab] 'corfu-previous)
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto-prefix 2)          ;; Minimum length of prefix for auto completion.
  (corfu-popupinfo-mode t)       ;; Enable popup information
  (corfu-popupinfo-delay 0.5)    ;; Lower popupinfo delay to 0.5 seconds from 2 seconds
  (corfu-separator ?\s)          ;; Orderless field separator, Use M-SPC to enter separator
  (corfu-preview-current 'insert) ;; Don't insert completion without confirmation
  (corfu-preselect 'prompt)
  (corfu-auto t)
  :init
  (global-corfu-mode t))

;; (use-package nerd-icons-corfu
;;   :after corfu
;;   :init (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package cape
  :after corfu
  ;; :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  ;; The functions that are added later will be the first in the list

  ;; (add-to-list 'completion-at-point-functions #'cape-dabbrev) ;; Complete word from current buffers
  ;; (add-to-list 'completion-at-point-functions #'cape-dict) ;; Dictionary completion
  ;; (add-to-list 'completion-at-point-functions #'cape-file) ;; Path completion
  ;; (add-to-list 'completion-at-point-functions #'cape-elisp-block) ;; Complete elisp in Org or Markdown mode
  ;; (add-to-list 'completion-at-point-functions #'cape-keyword) ;; Keyword/Snipet completion

  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev) ;; Complete abbreviation
  ;;(add-to-list 'completion-at-point-functions #'cape-history) ;; Complete from Eshell, Comint or minibuffer history
  ;;(add-to-list 'completion-at-point-functions #'cape-line) ;; Complete entire line from current buffer
  ;;(add-to-list 'completion-at-point-functions #'cape-elisp-symbol) ;; Complete Elisp symbol
  ;;(add-to-list 'completion-at-point-functions #'cape-tex) ;; Complete Unicode char from TeX command, e.g. \hbar
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml) ;; Complete Unicode char from SGML entity, e.g., &alpha
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345) ;; Complete Unicode char using RFC 1345 mnemonics
  )

;; (use-package completion-preview
;;   :ensure nil
;;   :hook (((prog-mode text-mode) . completion-preview-mode))
;;   :bind (:map completion-preview-active-mode-map
;;               ("M-n" . completion-preview-next-candidate)
;;               ("M-p" . completion-preview-prev-candidate)
;;               )
;;   :config
;;   (completion-preview-mode t)
;;   ;; Org mode has a custom `self-insert-command'
;;   (push 'org-self-insert-command completion-preview-commands)
;;   ;; Paredit has a custom `delete-backward-char' command
;;   (push 'paredit-backward-delete completion-preview-commands)
;;   :custom
;;   (completion-preview-minimum-symbol-length 1))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package nerd-icons-corfu
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(provide 'completion)
;;; completion.el ends here
