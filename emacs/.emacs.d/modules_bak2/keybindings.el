;;; keybindings.el --- -*- lexical-binding: t; -*-

(use-package general
  :demand t)

(elpaca-wait)

(provide 'keybindings)
;;; keybindings.el ends here
