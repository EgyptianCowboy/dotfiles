;;; gleam.el --- -*- lexical-binding: t; -*-

(use-package gleam-ts-mode
  :ensure (gleam-ts-mode :host github
                         :type git
                         :repo "gleam-lang/gleam-mode"
                         :branch "gleam-ts-mode")
  :mode "\\.gleam?\\'")

(provide 'gleam)
;;; gleam.el ends here
