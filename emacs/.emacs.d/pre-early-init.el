;;; pre-early-init.el --- Pre-early init -*- no-byte-compile: t; lexical-binding: t; -*-

(setq sil-emacs-user-directory
  (file-name-directory (or load-file-name (buffer-file-name))))

(setq custom-file null-device)

(provide 'pre-early-init)

;;; pre-early-init.el ends here
