;;; sil-modeline.el --- Code for my custom mode line -*- lexical-binding: t -*-

(defgroup sil-modeline nil
  "Custom modeline."
  :group 'mode-line)

(defgroup sil-modeline-faces nil
  "Faces for my custom modeline."
  :group 'sil-modeline)

(defcustom sil-modeline-string-truncate-length 9
  "String length after which truncation should be done in small windows."
  :type 'natnum)

;;;; Buffer status
(defvar-local sil-buffer-status
    '(:eval (cond (buffer-read-only "⨂")
                  ((buffer-modified-p) "⬤")
                  (t "◯")))
  "Buffer status.")

;;;; Buffer identification
(defvar-local sil-buffer-identification
    '(:eval
      (list
       " "
       (if (mode-line-window-selected-p)
	   (propertize (format "%s" (buffer-name)) 'face 'bold)
	 (propertize (format "%s" (buffer-name)) 'face 'shadow)))
      )
  "Buffer identification.")

;;;; Major mode
(defun sil-modeline--major-mode-name ()
  "Return capitalized `major-mode' as a string."
  (capitalize (string-replace "-mode" "" (symbol-name major-mode))))

(defun sil-modeline-major-mode-indicator ()
  "Return appropriate propertized mode line indicator for the major mode."
  (let ((indicator (cond
                    ((derived-mode-p 'text-mode) "§")
                    ((derived-mode-p 'prog-mode) "λ")
                    ((derived-mode-p 'comint-mode) ">_")
                    (t "◦"))))
    (propertize indicator 'face 'shadow)))

;; (;; defvar-local sil/line-major-mode
;;  ;;    '(:eval
;;  ;;      (list
;;  ;;       (propertize (sil/modeline--major-mode-name) 'face 'bold)
;;  ;;       ))
;;  ;;  "Mode line construct to display the major mode.")

(defvar-local sil-major-mode
    '(:eval
      (concat
       (sil-modeline-major-mode-indicator)
       " "
       (propertize (sil-modeline--major-mode-name) 'face 'bold)
       ))
  "Mode line construct for displaying major modes.")

;;;; Flymake
(declare-function flymake--severity "flymake" (type))
(declare-function flymake-diagnostic-type "flymake" (diag))

;; Based on `flymake--mode-line-counter'.
(defun sil-modeline-flymake-counter (type)
  "Compute number of diagnostics in buffer with TYPE's severity.
TYPE is usually keyword `:error', `:warning' or `:note'."
  (let ((count 0))
    (dolist (d (flymake-diagnostics))
      (when (= (flymake--severity type)
               (flymake--severity (flymake-diagnostic-type d)))
        (cl-incf count)))
    (when (cl-plusp count)
      (number-to-string count))))

(defvar sil-modeline-flymake-map
  (let ((map (make-sparse-keymap)))
    (define-key map [mode-line down-mouse-1] 'flymake-show-buffer-diagnostics)
    (define-key map [mode-line down-mouse-3] 'flymake-show-project-diagnostics)
    map)
  "Keymap to display on Flymake indicator.")

(defmacro sil-modeline-flymake-type (type indicator &optional face)
  "Return function that handles Flymake TYPE with stylistic INDICATOR and FACE."
  `(defun ,(intern (format "sil-modeline-flymake-%s" type)) ()
     (when-let ((count (sil-modeline-flymake-counter
                        ,(intern (format ":%s" type)))))
       (concat
        (propertize ,indicator 'face 'shadow)
        (propertize count
                    'face ',(or face type)
                     'mouse-face 'mode-line-highlight
                     ;; FIXME 2023-07-03: Clicking on the text with
                     ;; this buffer and a single warning present, the
                     ;; diagnostics take up the entire frame.  Why?
                     'local-map sil-modeline-flymake-map
                     'help-echo "mouse-1: buffer diagnostics\nmouse-3: project diagnostics")))))

(sil-modeline-flymake-type error "☣")
(sil-modeline-flymake-type warning "!")
(sil-modeline-flymake-type note "·" success)

(defvar-local sil-flymake
    `(:eval
      (when (and (bound-and-true-p flymake-mode)
                 (mode-line-window-selected-p))
        (list
         ;; See the calls to the macro `prot-modeline-flymake-type'
         '(:eval (sil-modeline-flymake-error))
         '(:eval (sil-modeline-flymake-warning))
         '(:eval (sil-modeline-flymake-note)))))
  "Mode line construct displaying `flymake-mode-line-format'.
Specific to the current window's mode line.")

;;;; Eglot

(with-eval-after-load 'eglot
  (setq mode-line-misc-info
        (delete '(eglot--managed-mode (" [" eglot--mode-line-format "] ")) mode-line-misc-info)))

(defvar-local sil-eglot
    `(:eval
      (when (and (featurep 'eglot) (mode-line-window-selected-p))
        '(eglot--managed-mode eglot--mode-line-format)))
  "Mode line construct displaying Eglot information.
Specific to the current window's mode line.")

(dolist (construct '(sil-buffer-status
                     sil-buffer-identification
                     sil-major-mode
                     ;; sil-vc-branch
                     sil-flymake
                     sil-eglot))
  (put construct 'risky-local-variable t))

		   

(provide 'sil-modeline)
;;; sil-modeline.el ends here
