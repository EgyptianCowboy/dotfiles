;;; transients-menus.el --- Central file for custom transient menus -*- lexical-binding: t -*-

;;; Commentary:
;; This file loads all custom transient menu definitions.

;;; Code:

(require 'transient)

;; Load individual transient files
(require 'file-transient)
(require 'buffer-transient)
(require 'window-transient)
(require 'text-transient)
;; (require 'avy-transient)
(require 'prog-transient)

;; Define a main transient menu that can access all other menus
(transient-define-prefix my-main-transient-menu ()
  "Main transient menu to access all custom menus."
  [["Transient Menus"
    ("f" "File operations" my-file-menu)
    ("b" "Buffer operations" my-buffer-menu)
    ("w" "Window operations" my-window-menu)
    ("t" "Text editing" my-text-editing-menu)
    ;; ("a" "Avy operations" my-avy-menu)
    ("c" "Code editing" my-coding-menu)
    ]])

(provide 'transient-menus)
;;; transient-menus.el ends here
