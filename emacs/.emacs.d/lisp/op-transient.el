;; op-transient.el --- 1Password CLI interface using transient -*- lexical-binding: t -*-

;; Copyright (C) 2024 Your Name
;; Author: Your Name <your.email@example.com>
;; Version: 0.1.0
;; Package-Requires: ((emacs "27.1") (transient "0.3.7"))
;; Keywords: tools, convenience
;; URL: https://github.com/yourusername/op-transient

;;; Commentary:

;; This package provides a transient interface for the 1Password CLI tool (op).
;; It allows you to easily manage your 1Password items from within Emacs.

;;; Code:

(require 'transient)
(require 'json)
(require 'auth-source)

(defgroup op-transient nil
  "Interface for 1Password CLI using transient."
  :group 'tools)

(defcustom op-transient-executable "op"
  "Path to the 1Password CLI executable."
  :type 'string
  :group 'op-transient)

(defcustom op-transient-account nil
  "1Password account shorthand (e.g., 'rideontrack' for rideontrack.1password.com)."
  :type 'string
  :group 'op-transient)

(defcustom op-transient-debug nil
  "When non-nil, enable debug logging for op-transient operations."
  :type 'boolean
  :group 'op-transient)

(defvar op-transient--session-token nil
  "Current 1Password session token.")

(defvar op-transient--session-env nil
  "Current 1Password session environment variable.")

(defun op-transient--debug (format-string &rest args)
  "Log debug message FORMAT-STRING with ARGS when debug is enabled."
  (when op-transient-debug
    (let ((msg (apply #'format (concat "[op-transient] " format-string) args)))
      (message "%s" msg))))

(defun op-transient--get-master-password ()
  "Get 1Password master password from auth-source."
  (when-let* ((auth-info (car (auth-source-search
                              :host "1password"
                              :require '(:secret)
                              :max 1)))
              (password (funcall (plist-get auth-info :secret))))
    password))

(defun op-transient--extract-token (output)
  "Extract session token from signin OUTPUT."
  (when output
    (let ((lines (split-string output "\n" t "[ \t\n\r]+")))
      ;; Get the line containing just the token (alphanumeric, dash, underscore)
      (car (seq-filter (lambda (line)
                        (string-match-p "^[A-Za-z0-9_-]+$" line))
                      lines)))))

(defun op-transient--run-command (&rest args)
  "Run op command with ARGS and return the output."
  (with-temp-buffer
    (let* ((process-environment
            (if op-transient--session-env
                (cons op-transient--session-env process-environment)
              process-environment))
           (exit-code (apply #'call-process op-transient-executable nil (list t t) nil args)))
      (op-transient--debug "Running command: op %s" (string-join args " "))
      (op-transient--debug "Using session env: %s" op-transient--session-env)
      (op-transient--debug "Exit code: %s" exit-code)
      (op-transient--debug "Output: %s" (buffer-string))
      (when (zerop exit-code)
        (buffer-string)))))

(defun op-transient--run-command-with-input (input &rest args)
  "Run op command with ARGS and INPUT as stdin. Return the output."
  (with-temp-buffer
    (op-transient--debug "Running command: op %s" (string-join args " "))
    (let ((process (make-process
                   :name "op-process"
                   :buffer (current-buffer)
                   :command (cons op-transient-executable args))))
      (process-send-string process (concat input "\n"))
      (process-send-eof process)
      (while (process-live-p process)
        (sleep-for 0.1))
      (let ((exit-code (process-exit-status process))
            (output (buffer-string)))
        (op-transient--debug "Exit code: %s" exit-code)
        (op-transient--debug "Raw output: %s" output)
        (when (zerop exit-code)
          output)))))

(defun op-transient--get-items ()
  "Get list of items from 1Password vault."
  (when-let* ((output (op-transient--run-command "item" "list" "--format=json"))
              (items (json-parse-string output :object-type 'plist)))
    (seq-map (lambda (item)
               (cons (plist-get item :title)
                     (plist-get item :id)))
             items)))

(defun op-transient--get-vaults ()
  "Get list of available vaults."
  (when-let* ((output (op-transient--run-command "vault" "list" "--format=json"))
              (vaults (json-parse-string output :object-type 'plist)))
    (seq-map (lambda (vault)
               (cons (plist-get vault :name)
                     (plist-get vault :id)))
             vaults)))

(defun op-transient-signin ()
  "Sign in to 1Password using master password from auth-source."
  (interactive)
  (op-transient--debug "Attempting to sign in with account: %s" op-transient-account)
  (if-let ((master-password (op-transient--get-master-password)))
      (progn
        (op-transient--debug "Got master password of length: %d" (length master-password))
        (if-let* ((output (op-transient--run-command-with-input
                          master-password
                          "signin"
                          "--raw"
                          "--account"
                          op-transient-account))
                  (token (op-transient--extract-token output)))
            (progn
              (setq op-transient--session-token token)
              (setq op-transient--session-env
                    (format "OP_SESSION_%s=%s" op-transient-account token))
              (op-transient--debug "Successfully obtained session token")
              (op-transient--debug "Set session env: %s" op-transient--session-env)
              (message "Signed in to 1Password"))
          (user-error "Failed to sign in. Check the debug output for details")))
    (user-error "Could not retrieve master password from auth-source")))

(defun op-transient-get-password (item)
  "Get password for ITEM and copy it to kill ring."
  (interactive (list (completing-read "Item: " (op-transient--get-items))))
  (when-let ((password (op-transient--run-command "item" "get" item "--field=password" "--reveal")))
    (kill-new (string-trim password))
    (message "Password copied to kill ring")))

(defun op-transient-view-item (item)
  "View details of ITEM."
  (interactive (list (completing-read "Item: " (op-transient--get-items))))
  (when-let ((details (op-transient--run-command "item" "get" item "--format=json")))
    (with-current-buffer (get-buffer-create "*1Password Item*")
      (erase-buffer)
      (insert details)
      (view-mode)
      (pop-to-buffer (current-buffer)))))

(transient-define-prefix op-transient-dispatch ()
  "Dispatch 1Password CLI commands."
  ["1Password Commands"
   ["Items"
    ("i" "List items" op-transient-list-items)
    ("g" "Get password" op-transient-get-password)
    ("v" "View item details" op-transient-view-item)]
   ["Vaults"
    ("V" "List vaults" op-transient-list-vaults)]
   ["Account"
    ("s" "Sign in" op-transient-signin)
    ("S" "Sign out" op-transient-signout)
    ("w" "Who am I" op-transient-whoami)]])

(defun op-transient-list-items ()
  "List items in the current vault."
  (interactive)
  (when-let ((items (op-transient--get-items)))
    (with-current-buffer (get-buffer-create "*1Password Items*")
      (erase-buffer)
      (dolist (item items)
        (insert (format "%s\n" (car item))))
      (view-mode)
      (pop-to-buffer (current-buffer)))))

(defun op-transient-list-vaults ()
  "List available vaults."
  (interactive)
  (when-let ((vaults (op-transient--get-vaults)))
    (with-current-buffer (get-buffer-create "*1Password Vaults*")
      (erase-buffer)
      (dolist (vault vaults)
        (insert (format "%s\n" (car vault))))
      (view-mode)
      (pop-to-buffer (current-buffer)))))

(defun op-transient-signout ()
  "Sign out from 1Password account."
  (interactive)
  (when-let ((output (op-transient--run-command "signout")))
    (setq op-transient--session-token nil)
    (setq op-transient--session-env nil)
    (message "%s" (string-trim output))))

(defun op-transient-whoami ()
  "Show current 1Password session status."
  (interactive)
  (if-let ((output (op-transient--run-command "whoami")))
      (message "%s" (string-trim output))
    (message "Not signed in")))

(provide 'op-transient)
;;; op-transient.el ends here
