;; sip-monitor.el --- Monitor SIP traffic in Emacs -*- lexical-binding: t -*-

(require 'json)
(require 'svg-lib)
(require 'dash)

(defgroup sip-monitor nil
  "Monitor SIP traffic in real-time."
  :group 'tools)

(defcustom sip-monitor-default-port 5060
  "Default port for SIP monitoring."
  :type 'integer
  :group 'sip-monitor)

(defvar sip-monitor-process nil
  "Process handle for tshark.")

(defvar sip-monitor-buffer "*SIP Monitor*"
  "Buffer name for SIP traffic display.")

(defvar-local sip-monitor-partial-data ""
  "Buffer for incomplete JSON data.")

(defface sip-monitor-request-face
  '((t :inherit font-lock-keyword-face :weight bold))
  "Face for SIP request methods.")

(defface sip-monitor-response-face
  '((t :inherit success :weight bold))
  "Face for SIP response codes.")

(defface sip-monitor-timestamp-face
  '((t :inherit font-lock-comment-face))
  "Face for timestamp display.")

(defface sip-monitor-user-face
  '((t :inherit font-lock-function-name-face))
  "Face for SIP user names.")

(defun sip-monitor-get-interfaces ()
  "Get list of network interfaces using tshark."
  (with-temp-buffer
    (call-process "tshark" nil t nil "-D")
    (goto-char (point-min))
    (let (interfaces)
      (while (not (eobp))
        (when (looking-at "^\\([0-9]+\\)\\.\\s-*\\(.+?\\)\\s-")
          (push (cons (match-string 2) (match-string 1)) interfaces))
        (forward-line 1))
      (nreverse interfaces))))

(defun sip-monitor-parse-header (header)
  "Parse SIP header string into alist."
  (let ((result '()))
    (with-temp-buffer
      (insert header)
      (goto-char (point-min))
      (while (not (eobp))
        (when (looking-at "^\\([^:\n]+\\):\\s-*\\(.+\\)\\(\r\n\\|\n\\)")
          (let ((key (match-string 1))
                (value (match-string 2)))
            (push (cons key value) result)))
        (forward-line 1)))
    result))

(defun sip-monitor-parse-uri (uri)
  "Parse SIP URI string to extract user part."
  (when (and uri (string-match "<sip:\\([^@>]+\\)@" uri))
    (match-string 1 uri)))

(defun sip-monitor-format-brief (msg)
  "Format SIP message for brief display."
  (let* ((source (alist-get '_source msg))
         (layers (alist-get 'layers source))
         (sip (alist-get 'sip layers))
         (headers (sip-monitor-parse-header (alist-get 'sip.msg_hdr sip)))
         (method (if (alist-get 'sip.Request-Line sip)
                    (car (split-string (alist-get 'sip.Request-Line sip)))
                  (car (last (split-string (alist-get 'sip.Status-Line sip))))))
         (from (alist-get "From" headers))
         (to (alist-get "To" headers))
         (call-id (alist-get "Call-ID" headers))
         (cseq (alist-get "CSeq" headers))
         (from-user (sip-monitor-parse-uri from))
         (to-user (sip-monitor-parse-uri to))
         (timestamp (propertize (format-time-string "%H:%M:%S")
                              'face 'sip-monitor-timestamp-face)))
    (propertize
     (format "%s %-8s %-15s → %-15s [%s] %s"
             timestamp
             (if (alist-get 'sip.Request-Line sip)
                 (propertize method 'face 'sip-monitor-request-face)
               (propertize method 'face 'sip-monitor-response-face))
             (propertize (or from-user "-") 'face 'sip-monitor-user-face)
             (propertize (or to-user "-") 'face 'sip-monitor-user-face)
             (if call-id (substring call-id 0 8) "-")
             (or cseq "-"))
     'sip-message msg)))

(defun sip-monitor-show-details ()
  "Show detailed view of SIP message at point."
  (interactive)
  (when-let ((msg (get-text-property (point) 'sip-message)))
    (let ((buf (get-buffer-create "*SIP Details*")))
      (with-current-buffer buf
        (let ((inhibit-read-only t))
          (erase-buffer)
          ;; Insert formatted SIP message
          (let* ((source (alist-get '_source msg))
                 (layers (alist-get 'layers source))
                 (sip (alist-get 'sip layers))
                 (start-line (or (alist-get 'sip.Request-Line sip)
                               (alist-get 'sip.Status-Line sip)))
                 (headers (alist-get 'sip.msg_hdr sip)))
            (insert start-line "\n" headers))
          (goto-char (point-min))
          (special-mode)))
      (display-buffer buf))))

(defun sip-monitor-clear ()
  "Clear the monitor buffer."
  (interactive)
  (with-current-buffer (get-buffer-create sip-monitor-buffer)
    (let ((inhibit-read-only t))
      (erase-buffer))))

(defun sip-monitor-start (interface port)
  "Start monitoring SIP traffic on INTERFACE and PORT."
  (interactive
   (list (completing-read "Interface: " (sip-monitor-get-interfaces))
         (read-number "Port: " sip-monitor-default-port)))

  (let ((buf (get-buffer-create sip-monitor-buffer)))
    (pop-to-buffer buf)
    (with-current-buffer buf
      (sip-monitor-mode)
      (sip-monitor-clear)
      (setq-local sip-monitor-partial-data ""))

    (let ((cmd (list "tshark" "-l" "-i" interface "-Y" "sip" "-T" "json" "-j" "\"sip\""
                    (format "port %d" port))))
      (setq sip-monitor-process
            (make-process
             :name "sip-monitor"
             :buffer buf
             :command cmd
             :sentinel #'sip-monitor-process-sentinel
             :filter #'sip-monitor-process-filter)))))

(defun sip-monitor-process-sentinel (proc event)
  "Handle process state changes for PROC with EVENT."
  (when (string-match-p "\\(finished\\|exited\\|terminated\\)" event)
    (with-current-buffer (process-buffer proc)
      (let ((inhibit-read-only t))
        (goto-char (point-max))
        (insert "\n[SIP Monitor stopped]\n")))))

(defun sip-monitor-process-filter (proc string)
  "Process tshark output PROC and STRING, updating the monitor buffer."
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (let ((inhibit-read-only t))
        ;; Handle startup message
        (if (string-prefix-p "Capturing on" string)
            (insert "\n[Monitor started...]\n\n")
          ;; Accumulate data
          (setq sip-monitor-partial-data (concat sip-monitor-partial-data string))

          ;; Try to find complete JSON array
          (when (and (string-prefix-p "[" sip-monitor-partial-data)
                    (string-match "\\][\n ]*\\'" sip-monitor-partial-data))
            (condition-case nil
                (let* ((json-array-type 'list)
                       (json-object-type 'alist)
                       (json-key-type 'symbol)
                       (msgs (json-read-from-string sip-monitor-partial-data)))
                  (save-excursion
                    (goto-char (point-max))
                    (dolist (msg msgs)
                      (insert (sip-monitor-format-brief msg) "\n")))
                  (setq sip-monitor-partial-data ""))
              (error nil))))))))

(defun sip-monitor-stop ()
  "Stop SIP monitoring."
  (interactive)
  (when (process-live-p sip-monitor-process)
    (delete-process sip-monitor-process)
    (setq sip-monitor-process nil)))

(define-derived-mode sip-monitor-mode special-mode "SIP Monitor"
  "Major mode for monitoring SIP traffic."
  (buffer-disable-undo)
  (setq truncate-lines t
        buffer-read-only t)

  ;; Add buttons for common actions
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "RET") #'sip-monitor-show-details)
    (define-key map "q" #'sip-monitor-stop)
    (define-key map "g" #'sip-monitor-start)
    (define-key map "c" #'sip-monitor-clear)
    (use-local-map map)))

(provide 'sip-monitor)

;; "-e" "sip.Method" "-e" "sip.Request-Line" "-e" "sip.r-uri" "-e" "sip.r-uri.user" "-e" "sip.r-uri.host" "-e" "sip.r-uri.port" "-e" "sip.Status-Code" "-e" "sip.Status-Line" "-e" "sip.Call-ID" "-e" "sip.From"
;; "-e" "sip.from.user" "-e" "sip.from.host" "-e" "sip.from.port" "-e" "sip.from.tag" "-e" "sip.To" "-e" "sip.to.user" "-e" "sip.to.host" "-e" "sip.to.port" "-e" "sip.to.tag" "-e" "sip.Via" "-e" "sip.Via.transport"
;; "-e" "sip.Via.branch" "-e" "sip.Via.sent-by.address" "-e" "sip.Via.sent-by.port" "-e" "sip.CSeq" "-e" "sip.CSeq.method" "-e" "sip.CSeq.seq" "-e" "sip.Contact" "-e" "sip.contact.uri" "-e" "sip.contact.user"
;; "-e" "sip.contact.host" "-e" "sip.contact.port" "-e" "sip.Route" "-e" "sip.Record-Route" "-e" "sip.Max-Forwards" "-e" "sip.Expires" "-e" "sip.Content-Type" "-e" "sip.Content-Length" "-e" "sip.Content-Encoding"
;; "-e" "sip.User-Agent" "-e" "sip.Server" "-e" "sip.Authorization" "-e" "sip.auth.scheme" "-e" "sip.auth.realm" "-e" "sip.auth.nonce" "-e" "sip.auth.username" "-e" "sip.auth.uri" "-e" "sip.WWW-Authenticate"
;; "-e" "sip.Proxy-Authenticate" "-e" "sip.Proxy-Authorization" "-e" "sip.Allow" "-e" "sip.Supported" "-e" "sip.Require" "-e" "sip.RSeq" "-e" "sip.RAck" "-e" "sip.Timestamp" "-e" "sip.msg_body" "-e" "sip.Warning"
;; "-e" "sip.P-Asserted-Identity" "-e" "sip.P-Preferred-Identity" "-e" "sip.Privacy" "-e" "sip.P-Preferred-Service" "-e" "sip.Retry-After" "-e" "sip.Reason" "-e" "sip.response-time" "-e" "sip.release-time" "-e" "sip.Event"
