(require 'transient)

(transient-define-prefix my-window-menu ()
  "Transient menu for window operations."
  :transient-suffix 'transient--do-stay
  ["Window Operations"
   ["Split"
    ("h" "Split horizontally" split-window-below)
    ("v" "Split vertically" split-window-right)]
   ["Navigate"
    ("o" "Other window" other-window)
    ("n" "Next window" next-window)
    ("p" "Previous window" previous-window)]
   ["Resize"
    ("+" "Increase height" enlarge-window)
    ("-" "Decrease height" shrink-window)
    (">" "Increase width" enlarge-window-horizontally)
    ("<" "Decrease width" shrink-window-horizontally)]
   ["Manage"
    ("d" "Delete window" delete-window)
    ("D" "Delete other windows" delete-other-windows)
    ("s" "Swap windows" window-swap-states)
    ("b" "Balance windows" balance-windows)]])

(provide 'window-transient)
