(require 'transient)

(transient-define-prefix my-buffer-menu ()
  "Transient menu for buffer operations."
  :transient-suffix 'transient--do-stay
  ["Buffer Operations"
   ("b" "Switch buffer" switch-to-buffer)
   ("k" "Kill buffer" kill-buffer)
   ("r" "Rename buffer" rename-buffer)
   ("l" "List buffers" list-buffers)])

(provide 'buffer-transient)
