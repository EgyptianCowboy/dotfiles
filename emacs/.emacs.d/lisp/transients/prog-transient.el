(require 'transient)
(require 'eglot)
(require 'treesit)

(transient-define-prefix my-coding-menu ()
  "Transient menu for coding operations."
  :transient-suffix 'transient--do-stay
  ["Coding Operations"
   ["General"
    ("i" "Indent region" indent-region)
    ("c" "Comment region" comment-region)
    ("u" "Uncomment region" uncomment-region)
    ("f" "Format buffer" eglot-format)
    ("r" "Rename symbol" eglot-rename)]
   ["Navigation"
    ("D" "Go to declaration" eglot-find-declaration)
    ;; ("R" "Find references" eglot-find-references)
    ("I" "Find implementations" eglot-find-implementation)]
   ["LSP"
    ("x" "Execute code action" eglot-code-actions)]
   ["Tree-sitter"
    ("p" "Previous function" treesit-beginning-of-defun)
    ("n" "Next function" treesit-end-of-defun)]
   ;; ["Mode Specific"
   ;;  ("m" "Mode-specific commands" my-mode-specific-menu)]
   ])


(provide 'prog-transient)
