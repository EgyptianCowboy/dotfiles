(require 'transient)
(require 'avy)

(transient-define-prefix my-avy-menu ()
  "Transient menu for Avy commands."
  :transient-suffix 'transient--do-stay
  ["Avy Navigation"
   ["Jump to"
    ("c" "Char (2 chars)" avy-goto-char-2)
    ("w" "Word" avy-goto-word-1)
    ("l" "Line" avy-goto-line)
    ("s" "Subword" avy-goto-subword-1)]
   ["Select"
    ("r" "Region" avy-mark-region)
    ("o" "Symbol" avy-mark-symbol)]
   ["Advanced"
    ("m" "Move line" avy-move-line)
    ("y" "Copy line" avy-copy-line)
    ("k" "Kill line" avy-kill-whole-line)
    ("=" "Transpose lines" avy-transpose-lines-in-region)]
   ["Multi-line"
    ("M-w" "Multi-line word" avy-goto-word-0)
    ("M-l" "Multi-line line" avy-goto-line-above)]])

(provide 'avy-transient)
