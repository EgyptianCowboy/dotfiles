(require 'transient)

(transient-define-prefix my-file-menu ()
  "Transient menu for file operations."
  :transient-suffix 'transient--do-stay
  ["File Operations"
   ("o" "Open file" find-file)
   ("s" "Save file" save-buffer)
   ("r" "Rename file" rename-file)
   ("d" "Delete file" delete-file)])

(provide 'file-transient)
