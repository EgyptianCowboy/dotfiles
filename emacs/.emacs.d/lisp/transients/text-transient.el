(require 'transient)

(transient-define-prefix my-text-editing-menu ()
  "Transient menu for common text editing operations."
  :transient-suffix 'transient--do-stay
  ["Text Editing Operations"
   ["Move"
    ("f" "Forward word" forward-word)
    ("b" "Backward word" backward-word)
    ("a" "Beginning of line" move-beginning-of-line)
    ("e" "End of line" move-end-of-line)]
   ["Edit"
    ("u" "Uppercase word" upcase-word)
    ("l" "Lowercase word" downcase-word)
    ("c" "Capitalize word" capitalize-word)
    ("t" "Transpose words" transpose-words)]
   ["Delete"
    ("d" "Delete word forward" kill-word)
    ("D" "Delete word backward" backward-kill-word)
    ("k" "Kill line" kill-line)]
   ["Advanced"
    ("s" "Isearch forward" isearch-forward)
    ("r" "Replace string" replace-string)
    ("q" "Query replace" query-replace)
    ("w" "Mark word" mark-word)
    ("p" "Fill paragraph" fill-paragraph)]])

(provide 'text-transient)
