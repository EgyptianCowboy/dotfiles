;; infixir.el --- Interactive Elixir Development Environment -*- lexical-binding: t -*-

;; Copyright (C) 2024 Your Name

;; Author: Your Name <your.email@example.com>
;; Version: 0.1.0
;; Package-Requires: ((emacs "30.1") (earl "0.1") (elixir-ts-mode "0.1"))
;; Keywords: languages, processes, tools
;; URL: https://github.com/yourusername/infixir

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Infixir provides an interactive development environment for Elixir,
;; focusing on seamless integration with the BEAM VM and providing rich
;; visualization and inspection tools.  It leverages the earl.el library
;; for BEAM communication and provides modern development workflows.

;;; Code:

(require 'project)
(require 'earl)
(require 'elixir-ts-mode)
(require 'treesit)
(require 'comint)
(require 'subr-x)
(require 'map)
(require 'ansi-color)

;;; Customization

(defgroup infixir nil
  "Interactive Elixir development environment."
  :group 'languages
  :prefix "infixir-")

(defcustom infixir-mix-env "dev"
  "Default Mix environment to use."
  :type 'string
  :group 'infixir)

(defcustom infixir-iex-command "iex"
  "Command to start IEx REPL."
  :type 'string
  :group 'infixir)

(defcustom infixir-mix-command "mix"
  "Command to run Mix tasks."
  :type 'string
  :group 'infixir)

(defcustom infixir-node-name-prefix "emacs"
  "Prefix for automatically generated node names."
  :type 'string
  :group 'infixir)

;;; Project Management

(defvar infixir--project-cache (make-hash-table :test 'equal)
  "Cache of project metadata indexed by project root.")

(defvar-local infixir--current-project nil
  "Current project information for the buffer.")

(defvar infixir--connections (make-hash-table :test 'equal)
  "Active BEAM node connections indexed by project root.")

(defclass infixir-project ()
  ((root :initarg :root
         :type string
         :documentation "Project root directory.")
   (name :initarg :name
         :type string
         :documentation "Project name from mix.exs.")
   (env  :initarg :env
         :type string
         :documentation "Mix environment for the project.")
   (deps :initarg :deps
         :type list
         :documentation "Project dependencies.")
   (modules :initarg :modules
           :type list
           :documentation "Available modules in the project."))
  "Represents an Elixir project with its metadata and state.")

(cl-defmethod infixir-project-node-name ((project infixir-project))
  "Generate a unique node name for PROJECT."
  (format "%s_%s@%s"
          infixir-node-name-prefix
          (thread-last (oref project name)
                      (replace-regexp-in-string "[^[:alnum:]]" "_")
                      (downcase))
          (system-name)))

(defun infixir--project-root ()
  "Find the root directory of the current Elixir project."
  (when-let* ((project (project-current))
              (root (project-root project))
              (mix-file (expand-file-name "mix.exs" root)))
    (when (file-exists-p mix-file)
      root)))

(defun infixir--mix-project-p (root)
  "Check if ROOT is a Mix project directory."
  (and root
       (file-exists-p (expand-file-name "mix.exs" root))))

(defun infixir--find-executable (name)
  "Find executable NAME in current environment.
Respects Nix shell and direnv configurations."
  (let ((exec-path (parse-colon-path (getenv "PATH"))))
    (executable-find name t)))

(defun infixir--read-project-info (root)
  "Read Mix project information from ROOT directory.
Returns an `infixir-project' instance with project metadata."
  (let* ((default-directory root)
         (mix-file (expand-file-name "mix.exs" root))
         (mix-executable (infixir--find-executable infixir-mix-command)))
    (unless mix-executable
      (user-error "Cannot find Mix executable. Is Mix installed and accessible in your PATH?"))
    ;; Read project name and metadata using Mix
    (let ((project-info
           (with-temp-buffer
             (let ((process-environment (cons (format "MIX_ENV=%s" infixir-mix-env)
                                            process-environment)))
               (if (zerop (call-process mix-executable nil t nil "compile"))
                   ;; TODO: Parse mix output for project information
                   (list :name (file-name-nondirectory (directory-file-name root))
                         :deps '()
                         :modules '())
                 (user-error "Failed to compile project: %s" (buffer-string)))))))
      (make-instance 'infixir-project
                    :root root
                    :name (plist-get project-info :name)
                    :env infixir-mix-env
                    :deps (plist-get project-info :deps)
                    :modules (plist-get project-info :modules)))))

(defun infixir--get-project ()
  "Get or create project information for the current buffer."
  (when-let ((root (infixir--project-root)))
    (or (gethash root infixir--project-cache)
        (puthash root (infixir--read-project-info root)
                 infixir--project-cache))))

;;; BEAM Connection Management

(defun infixir--ensure-connection (project)
  "Ensure BEAM connection exists for PROJECT.
Returns the connection process."
  (let ((root (oref project root)))
    (or (gethash root infixir--connections)
        (let* ((node-name (infixir-project-node-name project))
               (cookie (earl-cookie))
               ;; Start a distributed IEx node
               (proc (infixir--start-node project node-name)))
          (puthash root proc infixir--connections)
          proc))))

(defvar infixir--debug-mode nil
  "When non-nil, enable additional debugging output.")

(defun infixir--debug (format-string &rest args)
  "Output debug message if debug mode is enabled."
  (when infixir--debug-mode
    (apply #'message (concat "[Infixir Debug] " format-string) args)))

(defun infixir--start-node (project node-name)
  "Start a distributed Erlang node for PROJECT with NODE-NAME."
  (let* ((default-directory (expand-file-name (oref project root)))
         (iex-executable (infixir--find-executable infixir-iex-command))
         (mix-executable (infixir--find-executable infixir-mix-command))
         (process-environment
          (append
           (list
            (format "MIX_ENV=%s" (oref project env))
            (format "ERL_AFLAGS=-kernel shell_history enabled")
            ;; Ensure Mix can find itself in the PATH
            (format "PATH=%s:%s"
                    (or (file-name-directory mix-executable) "")
                    (getenv "PATH"))
            (when (getenv "NIX_PROFILES")
              (format "NIX_PROFILES=%s" (getenv "NIX_PROFILES")))
            (when (getenv "ERL_LIBS")
              (format "ERL_LIBS=%s" (getenv "ERL_LIBS"))))
           process-environment))
         (buffer-name (format "*infixir-%s*" node-name))
         (proc-name (format "infixir-%s" node-name)))

    (infixir--debug "Starting node %s in directory %s" node-name default-directory)
    (infixir--debug "Using IEx executable: %s" iex-executable)
    (infixir--debug "Using Mix executable: %s" mix-executable)

    ;; Kill existing process if it exists
    (when-let ((existing-buffer (get-buffer buffer-name)))
      (when-let ((existing-process (get-buffer-process existing-buffer)))
        (infixir--debug "Killing existing process %s" proc-name)
        (delete-process existing-process))
      (kill-buffer existing-buffer))

    ;; Create and set up the process buffer
    (let ((buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        ;; Enable ANSI color interpretation
        (when (fboundp 'ansi-color-for-comint-mode-on)
          (ansi-color-for-comint-mode-on))

        ;; Start IEx with Mix
        (let ((proc (make-process
                    :name proc-name
                    :buffer buffer
                    :command (list iex-executable
                                 "--name" node-name
                                 "--cookie" (earl-cookie)
                                 "-S" mix-executable)  ; Use full path to mix
                    :coding 'utf-8-emacs-unix
                    :noquery t
                    :file-handler t
                    :filter #'infixir--process-filter
                    :sentinel #'infixir--process-sentinel
                    :connection-type 'pty)))

          ;; Initialize the buffer and process
          (infixir--setup-repl-process proc)

          ;; Store node information
          (process-put proc 'node-name node-name)
          (process-put proc 'project project)

          proc)))))

(defun infixir--process-filter (proc string)
  "Process filter for IEx node output with proper ANSI handling.
PROC is the process and STRING is the output to process."
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (let ((moving (= (point) (process-mark proc)))
            (inhibit-read-only t))
        ;; Insert the text, advancing the process marker
        (save-excursion
          (goto-char (process-mark proc))
          ;; Convert ANSI color sequences properly
          (insert (ansi-color-apply string))
          (set-marker (process-mark proc) (point)))
        (when moving
          (goto-char (process-mark proc)))))))

(defun infixir--process-sentinel (proc event)
  "Process sentinel for IEx node state changes."
  ;; TODO: Implement proper process lifecycle management
  (message "Node %s: %s" (process-name proc) event))

;;; Minor Mode

(defvar infixir-mode-map
  (let ((map (make-sparse-keymap)))
    ;; REPL interaction
    (define-key map (kbd "C-c C-z") #'infixir-repl)
    (define-key map (kbd "C-c C-c") #'infixir-eval-buffer)
    (define-key map (kbd "C-c C-r") #'infixir-eval-region)
    (define-key map (kbd "C-c C-l") #'infixir-eval-line)
    (define-key map (kbd "C-c C-f") #'infixir-eval-function)
    (define-key map (kbd "C-c C-k") #'infixir-compile-buffer)
    (define-key map (kbd "C-c C-t") #'infixir-mix-test)
    map)
  "Keymap for Infixir mode.")

;;;###autoload
(define-minor-mode infixir-mode
  "Minor mode for interactive Elixir development.

This mode provides enhanced REPL integration, code evaluation,
and runtime inspection capabilities for Elixir projects."
  :lighter " Infixir"
  :keymap infixir-mode-map
  (if infixir-mode
      (infixir--setup)
    (infixir--teardown)))

(defun infixir--setup ()
  "Set up the Infixir development environment."
  (when-let ((project (infixir--get-project)))
    (setq infixir--current-project project)
    ;; Ensure we have a BEAM connection
    (infixir--ensure-connection project)))

(defun infixir--teardown ()
  "Clean up the Infixir development environment."
  (when-let* ((project infixir--current-project)
              (proc (gethash (oref project root) infixir--connections)))
    (delete-process proc)
    (remhash (oref project root) infixir--connections))
  (setq infixir--current-project nil))

;;; Interactive Commands

(defcustom infixir-repl-history-file (locate-user-emacs-file ".infixir_history")
  "File to store IEx REPL history."
  :type 'file
  :group 'infixir)

(defcustom infixir-discover-nodes t
  "Whether to discover existing BEAM nodes when connecting."
  :type 'boolean
  :group 'infixir)

(defun infixir--find-beam-executables ()
  "Find all required BEAM executables in the current environment.
Returns an alist of (executable . path) pairs or signals an error if any are missing."
  (let* ((required-executables '("iex" "mix" "epmd" "erl"))
         (paths (mapcar (lambda (cmd)
                         (cons cmd (infixir--find-executable cmd)))
                       required-executables)))
    (if-let ((missing (seq-filter (lambda (pair) (null (cdr pair))) paths)))
        (user-error "Required executables not found: %s. Is Elixir/Erlang properly installed in your Nix environment?"
                   (mapconcat #'car missing ", "))
      paths)))

(defun infixir--discover-nodes ()
  "Discover existing BEAM nodes on the local machine.
Returns a list of node names as strings. Handles Nix environments properly."
  (when infixir-discover-nodes
    (when-let* ((executables (infixir--find-beam-executables))
                (epmd-path (alist-get "epmd" executables nil nil #'string=)))
      (infixir--debug "Using epmd from: %s" epmd-path)
      (with-temp-buffer
        (let ((process-environment
               ;; Ensure we have all necessary environment variables
               (append
                (list
                 (format "PATH=%s" (getenv "PATH"))
                 (format "ERL_LIBS=%s" (or (getenv "ERL_LIBS") ""))
                 (when-let ((nix-profiles (getenv "NIX_PROFILES")))
                   (format "NIX_PROFILES=%s" nix-profiles)))
                process-environment)))
          (when (zerop (call-process epmd-path nil t nil "-names"))
            (goto-char (point-min))
            (let (nodes)
              (while (re-search-forward "^name\\s-+\\([^ ]+\\)\\s-+at" nil t)
                (push (match-string 1) nodes))
              nodes)))))))

(defun infixir--verify-beam-environment ()
  "Verify that the BEAM environment is properly set up.
Returns t if everything is ok, signals user-error otherwise."
  (let ((executables (infixir--find-beam-executables)))
    (infixir--debug "Found BEAM executables:")
    (dolist (pair executables)
      (infixir--debug "  %s: %s" (car pair) (cdr pair)))

    ;; Check if we're in a Nix environment
    (when (getenv "IN_NIX_SHELL")
      (infixir--debug "Running in Nix shell")
      (unless (getenv "ERL_LIBS")
        (infixir--debug "Warning: ERL_LIBS not set in Nix environment")))

    t))

(defun infixir--read-node-name (prompt &optional default)
  "Read a node name from the user with PROMPT.
If DEFAULT is provided, use it as the default value.
Offers completion over existing nodes and an option to create a new one."
  (let* ((existing-nodes (infixir--discover-nodes))
         (create-new "[Create new node]")
         (choices (if existing-nodes
                     (cons create-new existing-nodes)
                   (list create-new)))
         (choice (completing-read prompt choices
                                nil nil nil nil
                                (or default (car-safe existing-nodes)))))
    (if (equal choice create-new)
        (read-string "Enter new node name: "
                    (when-let ((project infixir--current-project))
                      (infixir-project-node-name project)))
      choice)))

(defun infixir--get-or-create-node (node-name)
  "Get or create a BEAM node with NODE-NAME.
Returns a process object representing the node connection.
If a project already has a running node, switches to that node instead."
  (if-let ((project infixir--current-project))
      ;; We're in a project context
      (let ((existing-proc (gethash (oref project root) infixir--connections)))
        (if (and existing-proc (process-live-p existing-proc))
            (progn
              ;; We have an existing connection - switch to it
              (infixir--debug "Switching to existing node: %s"
                           (process-get existing-proc 'node-name))
              existing-proc)
          ;; No existing connection - create new node
          (let ((proc (infixir--start-node project node-name)))
            (puthash (oref project root) proc infixir--connections)
            proc)))
    ;; Not in a project context, create standalone node
    (infixir--start-standalone-node node-name)))

(defun infixir--start-standalone-node (node-name)
  "Start a standalone IEx node with NODE-NAME.
Used when not in a project context."
  (let* ((buffer-name (format "*infixir-%s*" node-name))
         (proc-name (format "infixir-%s" node-name))
         (iex-executable (infixir--find-executable infixir-iex-command)))

    (unless iex-executable
      (user-error "Cannot find IEx executable"))

    (infixir--debug "Starting standalone node %s" node-name)

    ;; Kill existing process if it exists
    (when-let ((existing-buffer (get-buffer buffer-name)))
      (when-let ((existing-process (get-buffer-process existing-buffer)))
        (delete-process existing-process))
      (kill-buffer existing-buffer))

    ;; Start IEx in distributed mode
    (let ((proc (make-process
                :name proc-name
                :buffer buffer-name
                :command (list iex-executable
                             "--name" node-name
                             "--cookie" (earl-cookie))
                :coding 'utf-8-emacs-unix
                :noquery t
                :file-handler t
                :filter #'infixir--process-filter
                :sentinel #'infixir--process-sentinel)))

      (process-put proc 'node-name node-name)
      (unless (process-live-p proc)
        (user-error "Failed to start IEx process"))
      proc)))

(defun infixir-repl (&optional node-name)
  "Start or switch to IEx REPL.
If a REPL already exists for the current project, switches to it immediately.
Otherwise, prompts for node name and creates a new REPL.
With optional NODE-NAME, connects to or creates that specific node."
  (interactive)

  ;; First check for an existing project REPL
  (if-let* ((project infixir--current-project)
            (proc (gethash (oref project root) infixir--connections))
            ((process-live-p proc)))
      (progn
        (infixir--debug "Switching to existing REPL: %s"
                     (process-get proc 'node-name))
        (pop-to-buffer (process-buffer proc))

        ;; Ensure REPL buffer is properly set up
        (with-current-buffer (process-buffer proc)
          (unless (derived-mode-p 'comint-mode)
            (comint-mode)
            (setq-local comint-prompt-regexp "^\\(iex\\|...\\).*> ")
            (setq-local comint-input-sender 'infixir--send-input)
            (setq-local comint-process-echoes t)

            ;; Set up history
            (setq-local comint-input-ring-file-name infixir-repl-history-file)
            (comint-read-input-ring t))))

    ;; No existing REPL - prompt for node name and create new one
    (let ((node-name (or node-name
                        (infixir--read-node-name "Connect to node: "))))
      ;; Get or create the node connection
      (let ((proc (infixir--get-or-create-node node-name)))
        (pop-to-buffer (process-buffer proc))

        ;; Set up the new REPL buffer
        (with-current-buffer (process-buffer proc)
          (unless (derived-mode-p 'comint-mode)
            (comint-mode)
            (setq-local comint-prompt-regexp "^\\(iex\\|...\\).*> ")
            (setq-local comint-input-sender 'infixir--send-input)
            (setq-local comint-process-echoes t)

            ;; Set up history
            (setq-local comint-input-ring-file-name infixir-repl-history-file)
            (comint-read-input-ring t)))))))

(define-derived-mode infixir-repl-mode comint-mode "Infixir-REPL"
  "Major mode for Infixir REPL interaction."
  ;; Syntax table setup for Elixir
  (setq-local comment-start "#")
  (setq-local comment-start-skip "#+ *")

  ;; Use Elixir syntax highlighting in the REPL
  (setq-local syntax-propertize-function #'elixir-ts--syntax-propertize)

  ;; Set up comint customizations
  (setq-local comint-prompt-regexp "^\\(iex\\|\.\.\.\\).*> ")
  (setq-local comint-input-sender 'infixir--send-input)
  (setq-local comint-process-echoes t)

  ;; Enable history
  (setq-local comint-input-ring-file-name infixir-repl-history-file)
  (comint-read-input-ring t)

  ;; Smart indentation for multi-line input
  (setq-local comint-prompt-read-only t)
  (setq-local comint-eol-on-send t)

  ;; Electric pair mode for parentheses/quotes
  (electric-pair-local-mode 1))

(defun infixir--send-input (proc input)
  "Send INPUT to PROC with proper handling of compilation and module loading."
  (let ((cleaned-input (string-trim input)))
    (cond
     ;; Regular input
     (t
      (comint-send-string proc (concat cleaned-input "\n"))))))

(defun infixir--setup-repl-process (proc)
  "Set up a newly created REPL process PROC with proper initialization."
  (with-current-buffer (process-buffer proc)
    ;; Switch to our custom REPL mode
    (infixir-repl-mode)

    ;; Enable ANSI colors
    (setq-local ansi-color-for-comint-mode t)

    ;; Store node information
    (process-put proc 'initialized nil)

    ;; Set up process filter to handle initialization
    (let ((original-filter (process-filter proc)))
      (set-process-filter
       proc
       (lambda (proc string)
         ;; Call original filter first
         (funcall original-filter proc string)

         ;; Look for IEx prompt indicating readiness
         (when (and (not (process-get proc 'initialized))
                    (string-match-p "iex.*>" string))
           ;; Mark as initialized
           (process-put proc 'initialized t)
           ;; Restore original filter
           (set-process-filter proc original-filter)
           ;; Initialize the project environment
           (run-with-timer 0.1 nil #'infixir--initialize-project-env proc)))))))

(defun infixir--initialize-project-env (proc)
  "Initialize the project environment in the REPL process PROC."
  (when-let* ((project infixir--current-project)
              (root (oref project root)))
    (let ((expanded-root (expand-file-name root)))
      (with-current-buffer (process-buffer proc)
        ;; The project is already compiled by `iex -S mix`,
        ;; we just need to ensure we're in the right directory
        (comint-send-string
         proc
         (format "File.cd! \"%s\"\n"
                 (replace-regexp-in-string "\\\\" "/" expanded-root)))))))

(defun infixir--reload-module (proc module-path)
  "Reload a module from MODULE-PATH in REPL process PROC."
  (let ((code (format "c(\"%s\")" module-path)))
    (comint-send-string proc (concat code "\n"))))

(defun infixir--send-region (start end)
  "Send the region between START and END to the current REPL."
  (interactive "r")
  (when-let* ((project infixir--current-project)
              (proc (gethash (oref project root) infixir--connections))
              ((process-live-p proc)))
    (let ((code (buffer-substring-no-properties start end)))
      (with-current-buffer (process-buffer proc)
        (goto-char (point-max))
        (insert code)
        (comint-send-input)))))

(defun infixir-eval-buffer ()
  "Evaluate the current buffer in the REPL."
  (interactive)
  (infixir--send-region (point-min) (point-max)))

(defun infixir-eval-region (start end)
  "Evaluate the region from START to END in the REPL."
  (interactive "r")
  (infixir--send-region start end))

(defun infixir-eval-line ()
  "Evaluate the current line in the REPL."
  (interactive)
  (save-excursion
    (let ((start (progn (beginning-of-line) (point)))
          (end (progn (end-of-line) (point))))
      (infixir--send-region start end))))

(defun infixir-eval-function ()
  "Evaluate the Elixir function at point in the REPL."
  (interactive)
  ;; TODO: Use treesit to properly identify function bounds
  (save-excursion
    (let ((start (progn
                   (beginning-of-defun)
                   (point)))
          (end (progn
                 (end-of-defun)
                 (point))))
      (infixir--send-region start end))))

(defun infixir--run-mix-task (proc task-name callback)
  "Run Mix task TASK-NAME in process PROC and call CALLBACK with result.
Captures and displays output in the compilation buffer."
  (interactive)
  ;; TODO: Implement Mix task running
  (message "Run mix task not implemented yet"))

(defun infixir--reload-all-modules (proc project)
  "Reload all project modules in PROC after successful compilation.
This ensures all code is using the latest compiled versions."
  (interactive)
  ;; TODO: Implement reloading module
  (message "Module reloading not implemented yet"))

(defun infixir--handle-compilation-error (error-message)
  "Handle compilation error by displaying ERROR-MESSAGE.
Shows error in both compilation buffer and echo area."
  (interactive)
  ;; TODO: Implement compilation error handling
  (message "Compilation error handling not implemented yet"))

(defun infixir-load-module ()
  "Load current module in IEx REPL."
  (interactive)
  ;; TODO: Implement module loading
  (message "Module loading not yet implemented"))

(defun infixir-compile-buffer ()
  "Compile current buffer."
  (interactive)
  ;; TODO: Implement buffer compilation
  (message "Buffer compilation not yet implemented"))

(defun infixir-mix-test ()
  "Run Mix tests for current project or module."
  (interactive)
  ;; TODO: Implement test running
  (message "Test running not yet implemented"))

(provide 'infixir)

;;; infixir.el ends here
