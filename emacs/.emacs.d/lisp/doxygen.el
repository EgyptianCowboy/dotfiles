;;; doxygen.el --- Doxygen comment generator with completion-at-point -*- lexical-binding: t -*-
;; Copyright (C) 2025 Sil vaes

;; Author: Sil Vaes <sil.g.vaes@gmail.com>
;; Version: 0.1.0
;; Package-Requires: ((emacs "30.0")
;; Keywords: convenience, documentation, tools
;; URL: https://github.com/yourusername/doxygen

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides functionality for generating Doxygen comments in C/C++
;; modes with completion-at-point support.  It is compatible with Eglot and
;; uses cape-capf-buster for better integration.

;; Features:
;; - Automatic generation of function documentation templates
;; - Automatic generation of variable documentation
;; - Parameter extraction and @param tag generation
;; - Completion for Doxygen keywords using completion-at-point
;; - Eglot compatibility via cape-capf-buster

;; Usage:
;; (require 'doxygen)
;; The command `insert-doxygen-comment' will be bound to C-c d in C/C++ modes
;; Completion for Doxygen keywords will be available after typing @

;;; Code:

(require 'cape)
(require 'treesit)

(defgroup doxygen nil
  "Doxygen comment generator with completion support."
  :group 'tools
  :prefix "doxygen-")

(defcustom doxygen-keywords
  '("@brief" "@param" "@return" "@detail" "@example" "@usage"
    "@note" "@see" "@throws" "@deprecated" "@since" "@bug")
  "List of Doxygen keywords for completion."
  :type '(repeat string)
  :group 'doxygen)

(defcustom doxygen-key
  (kbd "C-c d")
  "Key binding for inserting Doxygen comments."
  :type 'key-sequence
  :group 'doxygen)

(defun doxygen--find-identifier (node &optional field)
  "Find identifier in NODE's declarator field.
If FIELD is non-nil, try to find a field_identifier first.
Returns '_' if no identifier is found or if the parameter has no name."
  (let ((id-type (if field "field_identifier" "identifier")))
    ;; First check if this is a parameter with no name
    (if (and (equal (treesit-node-type node) "parameter_declaration")
             (not (and (treesit-node-child-by-field-name node "declarator")
                     (treesit-search-subtree node "identifier" nil nil))))
        "_"
      (or
       ;; Try to find the identifier in the declarator field
       (when-let ((declarator (treesit-node-child-by-field-name node "declarator")))
         (if (equal (treesit-node-type declarator) id-type)
             (treesit-node-text declarator)
           (doxygen--find-identifier declarator field)))
       ;; If not found via declarator, try more general search
       (when-let ((id-node (treesit-search-subtree node id-type nil nil)))
         (treesit-node-text id-node))
       ;; If nothing found, return "_"
       "_"))))

(defun doxygen--get-function-params ()
  "Extract function parameters using treesit."
  (when-let* ((node (treesit-node-at (point)))
              (func-decl (treesit-parent-until
                         node
                         (lambda (n)
                           (member (treesit-node-type n)
                                 '("function_definition"
                                   "constructor_declaration"
                                   "function_declarator"
                                   "declaration")))))
              (param-list (treesit-search-subtree
                          func-decl "parameter_list" nil nil)))
    (let ((params nil))
      (when param-list
        (let ((child (treesit-node-child param-list 0)))
          (while child
            (when (equal (treesit-node-type child) "parameter_declaration")
              (push (doxygen--find-identifier child) params))
            (setq child (treesit-node-next-sibling child)))))
      (nreverse params))))

(defun doxygen--get-class-member ()
  "Extract class member variable name at point."
  (when-let* ((node (treesit-node-at (point)))
              (decl (treesit-parent-until
                    node
                    (lambda (n)
                      (member (treesit-node-type n)
                             '("field_declaration"))))))
    (doxygen--find-identifier decl t)))

(defun doxygen--insert-function-comment ()
  "Insert a Doxygen comment for a function."
  (let ((params (doxygen--get-function-params)))
    (beginning-of-line)
    (open-line 1)
    (insert "// @brief \n")
    (insert "//\n")
    (insert "// \n")
    (insert "//\n")
    (when params
      (dolist (param params)
        (insert (format "// @param %s \n" param))))
    (when params (insert "//\n"))
    (insert "// @return \n")
    (insert "//\n")
    (insert "// @detail\n")
    (insert "//\n")
    (insert "// @example\n")
    (insert "//\n")
    (insert "// @usage\n")
    (insert "//"))
  (when (fboundp 'eglot-format-buffer)
    (eglot-format-buffer)))

(defun doxygen--insert-variable-comment ()
  "Insert a Doxygen comment for a variable."
  (end-of-line)
  (if-let ((member-name (doxygen--get-class-member)))
      (insert (format "///< %s" member-name)))
  (when (fboundp 'eglot-format-buffer)
    (eglot-format-buffer)))

(defun doxygen--determine-comment-type ()
  "Determine if the current line is a function or variable declaration."
  (when-let* ((node (treesit-node-at (point)))
              (decl (treesit-parent-until
                    node
                    (lambda (n)
                      (member (treesit-node-type n)
                              '("function_definition"
                                "constructor_declaration"
                                "function_declarator"
                                "declaration"
                                "field_declaration"))))))
    (cond
     ((equal (treesit-node-type decl) "field_declaration")
      'variable)
     ((member (treesit-node-type decl)
              '("function_definition"
                "constructor_declaration"
                "function_declarator"))
      'function)
     ((let ((type (treesit-node-type decl)))
        (if (and (string= type "declaration")
                 (not (treesit-search-subtree decl "parameter_list" nil nil)))
            'variable
          'function))))))

;;;###autoload
(defun doxygen-insert-comment ()
  "Insert appropriate Doxygen comment based on context."
  (interactive)
  (if (not (treesit-available-p))
      (user-error "Tree-sitter is not available")
    (pcase (doxygen--determine-comment-type)
      ('function (doxygen--insert-function-comment))
      ('variable (doxygen--insert-variable-comment))
      (_ (user-error "No declaration found at point")))))

;;;###autoload
(defun doxygen-setup ()
  "Setup Doxygen completion with Eglot compatibility."
  (local-set-key doxygen-key #'doxygen-insert-comment))

;;;###autoload
(define-minor-mode doxygen-mode
  "Minor mode for Doxygen comment generation and completion."
  :lighter " Dox"
  :keymap (let ((map (make-sparse-keymap)))
           (define-key map doxygen-key #'doxygen-insert-comment)
           map)
  (if doxygen-mode
      (doxygen-setup)))

;;;###autoload
(add-hook 'c-mode-common-hook #'doxygen-mode)

(provide 'doxygen)

;;; doxygen.el ends here
