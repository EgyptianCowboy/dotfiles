;;; golang.el --- -*- lexical-binding: t; -*-

(use-package go-mode)

(provide 'golang)
;;; golang.el ends here
