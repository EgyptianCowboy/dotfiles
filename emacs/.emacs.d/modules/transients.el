;;; transients.el --- -*- lexical-binding: t; -*-

(use-package transient
  :demand t
  :config
  (transient-define-prefix transient-window ()
    "Transient for window commands."
    [["Resize"
      ("}" "h+" enlarge-window-horizontally :transient t)
      ("{" "h-" shrink-window-horizontally :transient t)
      ("^" "v+" enlarge-window :transient t)
      ("V" "v-" shrink-window :transient t)]
     ["Split"
      ("pv" "vertical" (lambda ()
                         (interactive)
                         (split-window-right)
                         (windmove-right)) :transient t)
      ("ph" "horizontal" (lambda ()
                           (interactive)
                           (split-window-below)
                           (windmove-down)) :transient t)
      ("wv" "win-vertical" (lambda ()
                             (interactive)
                             (select-window (ol/split-window-right))
                             (transient-window)) :transient nil)
      ("wh" "win-horizontal" (lambda ()
                               (interactive)
                               (select-window (ol/split-window-below))
                               (transient-window)) :transient nil)]
     ["Misc"
      ("o" "other window" other-window)
      ("B" "switch buffer" (lambda ()
                             (interactive)
                             (consult-buffer)
                             (transient-window)))
      ;; ("z" "undo" (lambda ()
      ;;               (interactive)
      ;;               (winner-undo)
      ;;               (setq this-command 'winner-undo)) :transient t)
      ;; ("Z" "redo" winner-redo :transient t)
      ]]

    [["Move"
      ("h" "←" windmove-left :transient t)
      ("j" "↓" windmove-down :transient t)
      ("l" "→" windmove-right :transient t)
      ("k" "↑" windmove-up :transient t)]
     ["Swap"
      ("sh" "←" windmove-swap-states-left :transient t)
      ("sj" "↓" windmove-swap-states-down :transient t)
      ("sl" "→" windmove-swap-states-right :transient t)
      ("sk" "↑" windmove-swap-states-up :transient t)]
     ["Delete"
      ("dh" "←" windmove-delete-left :transient t)
      ("dj" "↓" windmove-delete-down :transient t)
      ("dl" "→" windmove-delete-right :transient t)
      ("dk" "↑" windmove-delete-up :transient t)
      ("D" "This" delete-window :transient t)]
     ["Transpose"
      ("tt" "↜" (lambda ()
                  (interactive)
                  (transpose-frame)
                  (ol/transient-window)) :transient nil)
      ("ti" "↕" (lambda ()
                  (interactive)
                  (flip-frame)
                  (ol/transient-window)) :transient nil)
      ("to" "⟷" (lambda ()
                  (interactive)
                  (flop-frame)
                  (ol/transient-window)) :transient nil)
      ("tc" "⟳" (lambda ()
                  (interactive)
                  (rotate-frame-clockwise)
                  (ol/transient-window)) :transient nil)
      ("ta" "⟲" (lambda ()
                  (interactive)
                  (rotate-frame-anticlockwise)
                  (ol/transient-window)) :transient nil)]])

    (transient-define-prefix transient-avy ()
    "Transient for avy commands."
    [["Characters"
      ("j" "goto char timer" avy-goto-char-timer)
      ("cc" "goto char" avy-goto-char)
      ("ct" "goto char 2" avy-goto-char-2)
      ("cl" "goto char in line" avy-goto-char-in-line)
      ("ca" "goto char above" avy-goto-char-2-above)
      ("cb" "goto char below" avy-goto-char-2-below)
      ]
     
     ["Words and symbols"
      ("ww" "goto word" avy-goto-word-1)
      ("sw" "goto subword" avy-goto-subword-1)
      ("wa" "goto word above" avy-goto-word-1-above)
      ("wb" "goto word below" avy-goto-word-1-below)
      ("sw" "goto word or subword" avy-goto-word-or-subword-1)
      ("ss" "goto symbol" avy-goto-symbol-1)
      ("sa" "goto symbol above" avy-goto-symbol-1-above)
      ("sb" "goto symbol below" avy-goto-symbol-1-below)
      ]
     
     ["Lines and others"
      ("ll" "goto line" avy-goto-line)
      ("le" "goto end of line" avy-goto-end-of-line)
      ("la" "goto line above" avy-goto-line-above)
      ("lb" "goto line below" avy-goto-line-below)
      
      ("we" "goto whitespace end" avy-goto-whitespace-end)
      ("wa" "goto whitespace end above" avy-goto-whitespace-end-above)
      ("wb" "goto whitespace end below" avy-goto-whitespace-end-below)
      
      ("oh" "goto org heading timer" avy-org-goto-heading-timer)]])

    (transient-define-prefix transient-file ()
      "Transient for file commands."
      [["File or directory"
        ("f" "find-file" find-file)
        ("F" "find-file-other-window" find-file-other-window)]
       ["Directory only"
        ("d" "dired" dired)
        ("D" "dired-other-window" dired-other-window)]
       ["Documentation"
        ("l" "find-library" find-library)
        ("m" "man" man)]])

    (transient-define-prefix transient-buffer ()
      "Transient for buffer commands."
      [["Switch"
        ("b" "switch buffer" switch-to-buffer)
        ("B" "switch buf other window" switch-to-buffer-other-window)
        ("n" "next-buffer" next-buffer)
        ("p" "previous-buffer" previous-buffer)
        ("m" "buffer-menu" buffer-menu)
        ("q" "bury-buffer" bury-buffer)]
       ["Persist"
        ("c" "clone buffer" clone-indirect-buffer)
        ("C" "clone buf other window" clone-indirect-buffer-other-window)
        ("r" "rename-buffer" rename-buffer)
        ("R" "rename-uniquely" rename-uniquely)
        ("s" "save-buffer" save-buffer)
        ("w" "write-file" write-file)]
       ["Destroy"
        ("k" "kill-current-buffer" kill-current-buffer)
        ("K" "kill-buffer-and-window" kill-buffer-and-window)
        ("r" "revert-buffer" revert-buffer)]])

    (transient-define-prefix transient-search ()
      "Transient for search commands."
      [["Search"
        ("s" "isearch-forward" isearch-forward)
        ("S" "isearch-forward-regexp" isearch-forward-regexp)
        ("r" "isearch-backward" isearch-backward)
        ("R" "isearch-backward-regexp" isearch-backward-regexp)
        ("o" "occur" occur)]
       ["Edit"
        ("f" "flush-lines" flush-lines)
        ("k" "keep-lines" keep-lines)
        ("q" "query-replace" query-replace)
        ("Q" "query-replace-regexp" query-replace-regexp)]])

    (transient-define-prefix transient-consult ()
      "Transient for Consult commands."
      [["Search"
        ("s" "search" consult-line)
        ("rf" "recent file" consult-recent-file)
        ("mn" "man" consult-man)
        ("in" "info" consult-info)
        ("fd" "find" consult-fd)
        ("rg" "grep" consult-ripgrep)
        ("G" "git-grep" consult-git-grep)
        ("k" "keep lines" consult-keep-lines)
        ("u" "focus lines" consult-focus-lines)
        ("o" "occur" occur)]
       
       ["Navigation"
        ("e" "compile error" consult-compile-error)
        ("fm" "flymake" consult-flymake)
        ("l" "line" consult-goto-line)
        ("mk" "mark" consult-mark)
        ;; ("d" "directory" consult-dir)
        ;; ("x" "xref" consult-xref)
        ]
       
       ["Buffer and bookmark"
        ("bb" "buffer" consult-buffer)
        ("bw" "buffer other window" consult-buffer-other-window)
        ("bf" "buffer other frame" consult-buffer-other-frame)
        ("bp" "project buffer" consult-project-buffer)
        ("bm" "bookmark" consult-bookmark)]
       
       ["Outlines"
        ("t" "outline" consult-outline)
        ("im" "imenu" consult-imenu)]

       ["Registers"
        ("rr" "register" consult-register)
        ("rl" "register load" consult-register-load)
        ("rs" "register store" consult-register-store)]

       ["Editing"
        ("y" "yank-pop" consult-yank-pop)]
       ])

    (transient-define-prefix transient-flymake ()
      "Transient for Flymake commands."
      [["Flymake"
        ("s" "start" flymake-start)
        ("d" "buffer diagnostics" flymake-show-buffer-diagnostics)
        ("D" "project diagnostics" flymake-show-project-diagnostics)
        ("n" "next error" flymake-goto-next-error :transient t)
        ("p" "previous error" flymake-goto-prev-error :transient t)]])

    (transient-define-prefix transient-eglot ()
      "Transient for Eglot."
      [["Eglot"
        ("r" "reconnect" eglot-reconnect)
        ("a" "action" eglot-code-actions)
        ("f" "format" eglot-format)
        ("n" "rename" eglot-rename)
        ("d" "eldoc" eldoc)]])
    )

(provide 'transients)
;;; transients.el ends here
