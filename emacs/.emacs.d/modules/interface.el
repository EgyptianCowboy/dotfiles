;;; interface.el --- -*- lexical-binding: t; -*

;; Mode line information
(setopt line-number-mode t)                        ; Show current line in modeline
(setopt column-number-mode t)                      ; Show column as well

(setopt x-underline-at-descent-line nil)           ; Prettier underlines
(setopt switch-to-buffer-obey-display-actions t)   ; Make switching buffers more consistent
(setopt inhibit-compacting-font-caches t)
(setopt underline-minimum-offset 0)
(setopt line-spacing 0)
(setopt text-scale-remap-header-line t)

(setopt show-trailing-whitespace nil)      ; By default, don't underline trailing spaces
;; (setopt indicate-buffer-boundaries 'left)  ; Show buffer top and bottom in the margin

;; Enable horizontal scrolling
(setopt mouse-wheel-tilt-scroll t)
(setopt mouse-wheel-flip-direction t)

;; We won't set these, but they're good to know about
;;
(setopt indent-tabs-mode nil)
(setopt tab-width 4)

;; Misc. UI tweaks
(blink-cursor-mode -1)                                ; Steady cursor
(pixel-scroll-precision-mode)                         ; Smooth scrolling

(use-package display-line-numbers
  :ensure nil
  :hook (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-major-tick 0)
  (display-line-numbers-minor-tick 0)
  (display-line-numbers-grow-only t)
  (display-line-numbers-type t)
  (display-line-numbers-width 3)
  (display-line-numbers-widen t))

(set-face-attribute 'default nil
                    :family "Berkeley Mono" :height 110)
(set-face-attribute 'fixed-pitch nil
                    :family "Berkeley Mono")

(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Etoile"
                    :height 110)

;; (use-package modus-themes
;;   :bind (("<f5>" . modus-themes-toggle)
;;          ("C-<f5>" . modus-themes-select))
;;   :init
;;   (let ((overrides '((cursor blue-intense)
;;                      (keybind green-cooler)
;;                      (comment red-faint)
;;                      (bg-paren-match unspecified)
;;                      (fg-paren-match magenta-intense)
;;                      (underline-paren-match magenta-intense))))
;;     (setq modus-operandi-palette-overrides overrides
;;           modus-vivendi-palette-overrides overrides))
;;   (load-theme 'modus-operandi t)
;;   :custom
;;   (modus-themes-custom-auto-reload nil)
;;   (modus-themes-to-toggle '(modus-operandi modus-vivendi))
;;   (modus-themes-mixed-fonts t)
;;   (modus-themes-variable-pitch-ui t)
;;   (modus-themes-italic-constructs t)
;;   (modus-themes-bold-constructs nil)
;;   (modus-themes-completions '((t . (extrabold))))
;;   (modus-themes-prompts '(extrabold))
;;   (modus-themes-headings
;;    '((agenda-structure . (variable-pitch light 2.2)))
;;    (agenda-date . (variable-pitch regular 1.3))
;;    (t . (regular 1.15))))

(use-package ef-themes
  :init
  (let ((overrides '((cursor blue-intense)
                     (keybind green-cooler)
                     (comment red-faint)
                     (bg-paren-match unspecified)
                     (fg-paren-match magenta-intense)
                     (underline-paren-match magenta-intense))))
    (setq modus-operandi-palette-overrides overrides
          modus-vivendi-palette-overrides overrides))
  (load-theme 'modus-operandi t)
  :custom
  (modus-themes-custom-auto-reload nil)
  (modus-themes-to-toggle '(modus-operandi modus-vivendi))
  ;; (modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
  ;; (modus-themes-to-toggle '(modus-operandi-deuteranopia modus-vivendi-deuteranopia))
  ;; (modus-themes-to-toggle '(modus-operandi-tritanopia modus-vivendi-tritanopia))
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs nil)
  (modus-themes-completions '((t . (extrabold))))
  (modus-themes-prompts '(extrabold))
  (modus-themes-headings
   '((agenda-structure . (variable-pitch light 2.2)))
   (agenda-date . (variable-pitch regular 1.3))
   (t . (regular 1.15))))

;; (use-package mindre-theme
;;   :init
;;   (load-theme 'mindre t))

(use-package spacious-padding
  :ensure (spacious-padding :host github
                            :repo "protesilaos/spacious-padding")
  :init
  (spacious-padding-mode t)
  :custom
  (spacious-padding-widths
   '( :internal-border-width 10
      :header-line-width 4
      :mode-line-width 2
      :tab-width 2
      :right-divider-width 30
      :scroll-bar-width 8))
  
  (spacious-padding-subtle-mode-line
   `( :mode-line-active 'default)
      :mode-line-inactive window-divider))

;;;; Pulsar
;; Read the pulsar manual: <https://protesilaos.com/emacs/pulsar>.
(use-package pulsar
  :custom
  (pulsar-pulse t)
  (pulsar-delay 0.055)
  (pulsar-iterations 10)
  (pulsar-face 'pulsar-magenta)
  (pulsar-highlight-face 'pulsar-cyan)
  :hook
  ((next-error . (pulsar-pulse-line-red pulsar-recenter-top pulsar-reveal-entry))
   (minibuffer-setup . pulsar-pulse-line-red))
  :init
  (pulsar-global-mode 1))

(provide 'interface)
;;; interface.el ends here
