;;; projects.el --- Init file for emacs -*- lexical-binding: t; -*-

(use-package git-commit)
(use-package magit)

(use-package devdocs
  :general
  (:keymaps 'global-map
            "C-h D" 'devdocs-lookup)
  ;; :config
  ;; (add-hook 'c++-ts-mode
  ;;           (lambda () (setq-local devdocs-current-docs '("python~3.9"))))
  )
(provide 'projects)
;;; projects.el ends here

