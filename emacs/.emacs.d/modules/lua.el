;;; lua.el --- -*- lexical-binding: t; -*

(use-package xmake
  :ensure (xmake :type git
                 :host github
                 :repo "MiroYld/xmake-emacs"))

(use-package lua-mode)

(provide 'lua)
;;; lua.el ends here
