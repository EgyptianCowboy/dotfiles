;;; sepples.el --- -*- lexical-binding: t; -*

(add-to-list 'auto-mode-alist '("\\.cppm\\'" . c++-ts-mode))

(use-package cc-mode
  :ensure nil
  :config
  (setq c-default-style "java"
        c-basic-offset 4))

(use-package cmake-mode)

(provide 'sepples)
;;; sepples.el ends here
