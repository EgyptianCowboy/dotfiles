;;; lsp-config.el --- Init file for emacs -*- lexical-binding: t; -*-

(use-package go-mode)

(use-package treesit
  :ensure nil
  :commands (treesit-install-language-grammar sil-treesit-install-all-languages)
  :custom
  (treesit-language-source-alist
   '((bash . ("https://github.com/tree-sitter/tree-sitter-bash"))
     (c . ("https://github.com/tree-sitter/tree-sitter-c"))
     (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"))
     (json . ("https://github.com/tree-sitter/tree-sitter-json"))
     (ocaml . ("https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src"))
     (ocaml-interface . ("https://github.com/tree-sitter/tree-sitter-ocaml" "master" "interface/src"))
     (python . ("https://github.com/tree-sitter/tree-sitter-python"))
     (toml . ("https://github.com/tree-sitter/tree-sitter-toml"))
     (go . ("https://github.com/tree-sitter/tree-sitter-go"))
     (gomod . ("https://github.com/camdencheek/tree-sitter-go-mod"))
     (rust . ("https://github.com/tree-sitter/tree-sitter-rust"))
     ((cmake . ("https://github.com/uyha/tree-sitter-cmake")))))
  :init
  (dolist (mapping
           '((python-mode . python-ts-mode)
             (go-mode . go-ts-mode)
             ;;(c++-mode . c++-ts-mode)
             ;;(c-mode . c-ts-mode)
             (bash-mode . bash-ts-mode)
             (json-mode . json-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping)))

(use-package cmake-mode)

(use-package eglot
  :hook ((c-mode-common . eglot-ensure)
         (python-ts-mode . eglot-ensure)
         (go-ts-mode . eglot-ensure)
         (eglot-managed-mode . eglot-inlay-hints-mode))
  :general (:keymaps 'eglot-mode-map
                     "C-c C-g" 'transient-eglot)
  :config
  (add-to-list 'eglot-server-programs
               '((c-mode c++-mode)
                 . ("clangd"
                    "-j=8"
                    "--log=error"
                    "--malloc-trim"
                    "--background-index"
                    "--clang-tidy"
                    "--completion-style=detailed"
                    "--pch-storage=memory"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0"
                    "--completion-style=bundled")))
  (fset #'jsonrpc--log-event #'ignore)
  :custom
  (eglot-autoshutdown t)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-sync-connect nil)
  (eglot-send-changes-idle-time 3)
  (flymake-no-changes-timeout 5)
  (eglot-connect-timeout nil)
  (eldoc-echo-area-use-multiline-p nil))

(use-package markdown-mode)

(use-package flymake
  :ensure nil
  :general (:keymaps 'flymake-mode-map
                     "C-c f" 'transient-flymake)
  :hook
  ((prog-mode . flymake-mode)))

(use-package flymake-popon
  :ensure (flymake-popon
           :type git
           :repo "https://codeberg.org/akib/emacs-flymake-popon.git")
  :hook (flymake-mode . flymake-popon-mode))

(use-package eldoc-box
  :ensure (eldoc-box :type git
                     :host github
                     :repo "casouri/eldoc-box")
  :hook ((eglot-managed-mode . eldoc-box-hover-mode)
         (lspce-mode . eldoc-box-hover-mode))
  :custom-face
  (eldoc-box-body ((t (:inherit 'variable-pitch)))))

(provide 'lsp-config)
;;; lsp-config.el ends here

