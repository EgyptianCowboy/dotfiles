;;; completion.el --- -*- lexical-binding: t; -*

;; A few useful configurations...
(use-package emacs
  :ensure nil
  :config
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
		  (replace-regexp-in-string
		   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
		   crm-separator)
		  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal 

  (keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell
  :custom
  ;; Do not allow the cursor in the minibuffer prompt
  (minibuffer-prompt-properties
   '(read-only t cursor-intangible t face minibuffer-prompt))
  (enable-recursive-minibuffers t)
  ;; (completion-cycle-threshold 1)                  ; TAB cycles candidates
  ;; (completions-detailed t)                        ; Show annotations
  ;; (tab-always-indent 'complete)
  )


;; Completion packages
(use-package hotfuzz
  :demand t
  :ensure (hotfuzz :host github
                   :repo "axelf4/hotfuzz"
		   :pre-build (("cmake" "-B" "build" "-DCMAKE_BUILD_TYPE=Release" "-DCMAKE_C_FLAGS=-march=native")
			           ("cmake" "--build" "build"))
		   :files (:defaults "hotfuzz-module.so"))
  :custom
  (read-file-name-completion-ignore-case t)
  (read-buffer-completion-ignore-case t)
  (completion-ignore-case t)
  (hotfuzz-max-highlighted-completions most-positive-fixnum)
  (completion-category-overrides '((eglot (styles hotfuzz))
                                   (eglot-capf (styles hotfuzz))))
  :config
  (require 'hotfuzz-module)
  (add-to-list 'completion-styles 'hotfuzz))

(use-package vertico
  :ensure (vertico :host github
                   :repo "minad/vertico"
                   :files (:defaults "extensions/*")
                   :main "vertico.el"
		           :includes (vertico-directory))
  :bind
  (:map vertico-map
    	("<tab>" . vertico-insert)
    	("<escape>" . minibuffer-keyboard-quit)
	    ("C-k" . vertico-exit)
    	("C-n" . vertico-next)
    	("C-p" . vertico-previous)
	    ("M-RET" . vertico-exit-input)
	    
	    ("RET" . vertico-directory-enter)
    	("<backspace>" . vertico-directory-delete-char)
    	("C-w" . vertico-directory-delete-word)
    	("C-<backspace>" . vertico-directory-delete-word)
    	("DEL" . vertico-directory-delete-word)
    	("M-d" . vertico-directory-delete-char)
    	("RET" . vertico-directory-enter)

	    ("?".  minibuffer-completion-help)
	    ("M-TAB" . minibuffer-complete))
  (:map minibuffer-local-map
    	("<backtab>" . minibuffer-complete)
    	("M-d" . backward-kill-word))
  :custom
  (vertico-scroll-margin 0)
  (vertico-count 10)
  :init
  (vertico-mode t))

(use-package marginalia
  :init
  (marginalia-mode t))

(use-package consult
  :after vertico
  :general
  ([remap isearch-forward] 'consult-line
   [remap switch-to-buffer] 'consult-buffer 
   [remap Info-search] 'consult-info
   [remap isearch-backward] 'consult-recent-file
   [remap yank-pop] 'consult-yank-pop)
  :config
  (setq consult--tofu-char #x100000)
  (setq consult--tofu-range #x00fffe)
  (setq xref-show-xrefs-function #'consult-xref)
  (setq xref-show-definitions-function #'consult-xref))

(use-package corfu
  :general
  (:keymaps 'corfu-map
            [tab] 'corfu-next
            [backtab] 'corfu-previous)
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto-prefix 2)          ;; Minimum length of prefix for auto completion.
  (corfu-popupinfo-mode t)       ;; Enable popup information
  (corfu-popupinfo-delay 0.5)    ;; Lower popupinfo delay to 0.5 seconds from 2 seconds
  (corfu-separator ?\s)          ;; Orderless field separator, Use M-SPC to enter separator
  (corfu-preview-current 'insert) ;; Don't insert completion without confirmation
  (corfu-preselect 'prompt)
  (corfu-auto t)
  :init
  (global-corfu-mode t))

(use-package cape)

(use-package prescient
  :init
  (add-to-list 'completion-styles 'prescient))

(use-package corfu-prescient
  :after corfu
  :init
  (corfu-prescient-mode)
  :custom
  (corfu-prescient-completion-styles '(prescient hotfuzz basic)))

(use-package vertico-prescient
  :after vertico
  :init
  (vertico-prescient-mode)
  :custom
  (vertico-prescient-completion-styles '(prescient hotfuzz basic)))

(provide 'completion)
;;; completion.el ends here
