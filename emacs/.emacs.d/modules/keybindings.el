;;; keybindings.el --- -*- lexical-binding: t; -*-

(use-package general
  :demand t)

(elpaca-wait)

(use-package emacs
  :ensure nil
  :preface
  (defun config-visit ()
    (interactive)
    (find-file (concat emacs-home "init.el")))

  (defun split-and-follow-h ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))

  (defun split-and-follow-v ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))

  (defun ol/split-window-right ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'right nil))

  (defun ol/split-window-below ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'below nil))

  (defun my-kill-buffer (arg)
    (interactive "P")
    (if arg
        (call-interactively 'kill-buffer)
      (kill-buffer)))
  ;; :hook (eval-expression-minibuffer-setup . show-paren-mode)
  :bind (:map global-map
	          ("<insert>" . nil)
	          ("M-SPC" . cycle-spacing)
	          ("C-x k" . my-kill-buffer)
	          ("C-c o" . occur)
	          ("C-z" . nil)
	          ("C-c C-z" . nil)
	          ("C-x C-z" . nil)
              ("C-x C-c" . nil)
	          ("C-h h" . nil)
	          ("C-c e" . config-visit)
	          ([remap split-window-below] . split-and-follow-h)
	          ([remap split-window-right] . split-and-follow-v)
              ;; ("C-z 2" . ol/split-window-below)
              ;; ("C-z 3" . ol/split-window-right)
              ))


(provide 'keybindings)
;;; keybindings.el ends here
