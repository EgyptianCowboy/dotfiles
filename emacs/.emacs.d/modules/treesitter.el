;;; treesitter.el --- -*- lexical-binding: t; -*

(use-package treesit
  :ensure nil
  :custom
  ;; Should use:
  ;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))
  ;; at least once per installation or while changing this list
  (treesit-language-source-alist
   '((heex "https://github.com/phoenixframework/tree-sitter-heex")
     (elixir "https://github.com/elixir-lang/tree-sitter-elixir")
     (go . ("https://github.com/tree-sitter/tree-sitter-go"))
     (gomod . ("https://github.com/camdencheek/tree-sitter-go-mod"))
     (cmake ("https://github.com/uyha/tree-sitter-cmake"))
     (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"))
     (c . ("https://github.com/tree-sitter/tree-sitter-c"))))

  (major-mode-remap-alist
   '((elixir-mode . elixir-ts-mode)
     (cmake-mode . cmake-ts-mode)
     (go-mode . go-ts-mode)
     (c++-mode . c++-ts-mode)
     (c-mode . c-ts-mode))))

(provide 'treesitter)
;;; treesitter.el ends here
