;;; ocaml.el --- -*- lexical-binding: t; -*

(use-package tuareg
  :mode ("\\.ml[iylp]?\\'" . tuareg-mode))

(use-package utop
  :hook ((ocaml-ts-mode . utop-minor-mode)
         (tuareg-mode . utop-minor-mode)))

(use-package dune)

(use-package ocamlformat
  :custom
  (ocamlformat-enable 'enable-outside-detected-project))

(provide 'ocaml)
;;; ocaml.el ends here
