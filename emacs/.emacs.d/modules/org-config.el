;;; org-config.el --- Init file for emacs -*- lexical-binding: t; -*-

(use-package ox-pandoc)

(use-package org
  :ensure nil
  :hook (org-mode . variable-pitch-mode)
  :custom
  (org-startup-indented t)
  (org-pretty-entities t)
  (org-use-sub-superscripts "{}")
  (org-hide-emphasis-markers t)
  (org-startup-with-inline-images t)
  (org-image-actual-width '(300))
  (org-ellipsis "…")
  :config
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((C . t))))

;; (use-package master-of-ceremonies
;;   :ensure (master-of-ceremonies 
;;            :host github
;;            :repo "positron-solutions/master-of-ceremonies"))

;; (use-package dslide
;;   :preface
;;   (defun pres-start ()
;;     (delete-other-windows)
;;     (text-scale-set 2))

;;   (defun pres-stop ()
;;     (delete-other-windows)
;;     (text-scale-set 0))

;;   :hook ((dslide-start . (lambda () (text-scale-set 6)
;;                            (setq-local olivetti-body-width 0.1)
;;                            (mc-hide-cursor-mode t)
;;                            (olivetti-mode)))
;;          (dslide-stop . (lambda () (text-scale-set 0))))
;;   :ensure (dslide :host github
;;                   :repo "positron-solutions/dslide"))

(use-package hide-mode-line)

(use-package org-appear
  :hook
  (org-mode . org-appear-mode))

;; (use-package org-modern
;;   :init
;;   (global-org-modern-mode)
;;   :custom
;;   (org-modern-keyword nil)
;;   (org-modern-checkbox nil)
;;   (org-modern-table nil))

(use-package olivetti
  :hook (text-mode . olivetti-mode)
  :bind ("C-c o" . olivetti-mode)
  :custom
  (olivetti-body-width 0.65)
  (olivetti-minimum-body-width 80)
  (olivetti-margin-width 5)
  (olivetti-recall-visual-line-mode-entry-state t)
  (olivetti-style 'fancy)
  (olivetti-enable-borders t)
  (olivetti-window-local t))

(provide 'org-config)
;;; org-config.el ends here
