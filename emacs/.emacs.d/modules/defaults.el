;;; defaults.el --- Init file for emacs -*- lexical-binding: t; -*-

(setq auto-mode-alist(cons '("\\.h$"   . c++-mode)  auto-mode-alist))

(use-package savehist
  :ensure nil
  :init
  (savehist-mode))

(use-package recentf
  :ensure nil
  :after no-littering
  :custom
  (recentf-save-file (locate-user-emacs-file "var/recentf-save.el"))
  (recentf-max-saved-items 1000)
  (recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  (recentf-auto-cleanup 30)
  (recentf-max-menu-items 50)
  (recentf-additional-variables '(kill-ring search-ring regexp-search-ring))
  :config
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  (run-with-idle-timer 30 t 'recentf-save-list)
  :init
  (recentf-mode))

;; Automatically reread from disk if the underlying file changes
(setopt auto-revert-avoid-polling t)
;; Some systems don't do file notifications well; see
(setopt auto-revert-interval 5)
(setopt auto-revert-check-vc-info t)
(global-auto-revert-mode)
;; Fix archaic defaults
(setopt sentence-end-double-space nil)

(use-package no-littering
  :demand t
  :custom
  (no-littering-etc-directory
   (expand-file-name "config/" user-emacs-directory))
  (no-littering-var-directory
   (expand-file-name "data/" user-emacs-directory)))

(setq make-backup-files nil)

;; which-key: shows a popup of available keybindings when typing a long key
(use-package which-key
  :init
  (which-key-mode))

;; Better comments
(use-package comment-dwim-2
  :bind (:map global-map
              ([remap comment-dwim] . comment-dwim-2)))

;; Unique buffer names
(use-package uniquify
  :ensure nil
  :custom
  (uniquify-buffer-name-style 'reverse)
  (uniquify-separator " • ")
  (uniquify-after-kill-buffer-p t)
  (uniquify-ignore-buffers-re "^\\*")
  (uniquify-strip-common-suffix t)
  (uniquify-after-kill-buffer-p t))

;; Direnv
(use-package envrc
  :ensure t
  :init (envrc-global-mode 1))

;; Paren
(show-paren-mode t)
(setq show-paren-context-when-offscreen 'child-frame
      show-paren-when-point-in-periphery t
      show-paren-when-point-inside-paren t)

;; Some small settings
(customize-set-variable 'yank-pop-change-selection t)
(customize-set-variable 'kill-whole-line t)
(customize-set-variable 'track-eol t) ; Keep cursor at end of lines.
(customize-set-variable 'line-move-visual nil) ; To be required by track-eol
(customize-set-variable 'kill-do-not-save-duplicates t)
(customize-set-variable 'use-short-answers t)

;; Disable system-wide dialogs
(customize-set-variable 'use-file-dialog nil)
(customize-set-variable 'use-dialog-box nil)                ; Avoid GUI dialogs
(customize-set-variable 'x-gtk-use-system-tooltips nil)     ; Do not use GTK tooltips
(customize-set-variable 'epa-pinentry-mode 'loopback)

(use-package crux)

(use-package string-inflection)

(provide 'defaults)
;;; defaults.el ends here
