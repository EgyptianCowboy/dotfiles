;;; treesitter.el --- -*- lexical-binding: t; -*

(use-package eglot
  :ensure nil
  :hook ((ocaml-ts-mode . eglot-ensure)
         (tuareg-mode . eglot-ensure)
         (c++-ts-mode . eglot-ensure)
         (go-ts-mode . eglot-ensure))
  :config
  (add-to-list 'eglot-server-programs '(elixir-ts-mode . ("lexical")))
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  (add-to-list 'eglot-server-programs
               '((c-ts-mode c++-ts-mode)
                 . ("clangd"
                    "-j=8"
                    "--log=error"
                    "--malloc-trim"
                    "--background-index"
                    "--clang-tidy"
                    "--cross-file-rename"
                    "--completion-style=detailed"
                    "--pch-storage=memory"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0"
                    "--completion-style=bundled"
                    "--suggest-missing-includes")))
  :custom
  ;; Good default
  (eglot-events-buffer-size 0) ;; No event buffers (Lsp server logs)
  (eglot-autoshutdown t);; Shutdown unused servers.
  (fset #'jsonrpc--log-event #'ignore)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-sync-connect nil)
  (eglot-send-changes-idle-time 3)
  (flymake-no-changes-timeout 5)
  (eglot-connect-timeout nil)
  (eldoc-echo-area-use-multiline-p nil))

(use-package eglot-booster
  :ensure (eglot-booster :type git :host github :repo "jdtsmith/eglot-booster")
  :after eglot
  :hook (eglot-mode . eglot-booster))

(provide 'lsp)
;;; lsp.el ends here
