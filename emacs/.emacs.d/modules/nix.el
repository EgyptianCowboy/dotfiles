;;; nix.el --- -*- lexical-binding: t; -*

(use-package nix-mode)

(use-package nickel-mode)

(provide 'nix)
;;; nix.el ends here
