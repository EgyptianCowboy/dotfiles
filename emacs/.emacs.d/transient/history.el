((eglot-transient-menu nil)
 (ellama-transient-code-menu nil)
 (ellama-transient-main-menu nil)
 (forge-add-repository nil)
 (forge-configure nil)
 (forge-dispatch nil)
 (forge-repositories-menu nil)
 (forge-topic-menu nil)
 (magit-branch nil)
 (magit-commit nil)
 (magit-diff
  ("--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-fetch nil)
 (magit-log
  ("-n256" "--graph" "--decorate"))
 (magit-merge nil)
 (magit-pull nil)
 (magit-push nil
             ("--force"))
 (magit-rebase nil
               ("--rebase-merges=no-rebase-cousins" "--autostash"))
 (magit-revert
  ("--edit"))
 (magit-stash nil)
 (my-main-transient-menu nil)
 (project-transient-menu nil)
 (transient:magit-rebase:--rebase-merges=))
