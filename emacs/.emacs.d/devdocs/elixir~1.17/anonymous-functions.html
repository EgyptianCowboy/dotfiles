<div id="top-content"> <h1> <a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/pages/getting-started/anonymous-functions.md#L1" title="View Source" class="source" rel="help">Source</a> <span>Anonymous functions</span> </h1> <p>Anonymous functions allow us to store and pass executable code around as if it was an integer or a string. Let's learn more.</p>
<h2 id="defining-anonymous-functions" class="section-heading">  <span class="text">Defining anonymous functions</span> </h2> <p>Anonymous functions in Elixir are delimited by the keywords <code class="inline">fn</code> and <code class="inline">end</code>:</p>
<pre data-language="elixir">iex&gt; add = fn a, b -&gt; a + b end
#Function&lt;12.71889879/2 in :erl_eval.expr/5&gt;
iex&gt; add.(1, 2)
3
iex&gt; is_function(add)
true</pre>
<p>In the example above, we defined an anonymous function that receives two arguments, <code class="inline">a</code> and <code class="inline">b</code>, and returns the result of <code class="inline">a + b</code>. The arguments are always on the left-hand side of <code class="inline">-&gt;</code> and the code to be executed on the right-hand side. The anonymous function is stored in the variable <code class="inline">add</code>.</p>
<p>We can invoke anonymous functions by passing arguments to it. Note that a dot (<code class="inline">.</code>) between the variable and parentheses is required to invoke an anonymous function. The dot makes it clear when you are calling an anonymous function, stored in the variable <code class="inline">add</code>, opposed to a function named <code class="inline">add/2</code>. For example, if you have an anonymous function stored in the variable <code class="inline">is_atom</code>, there is no ambiguity between <code class="inline">is_atom.(:foo)</code> and <code class="inline">is_atom(:foo)</code>. If both used the same <code class="inline">is_atom(:foo)</code> syntax, the only way to know the actual behavior of <code class="inline">is_atom(:foo)</code> would be by scanning all code thus far for a possible definition of the <code class="inline">is_atom</code> variable. This scanning hurts maintainability as it requires developers to track additional context in their head when reading and writing code.</p>
<p>Anonymous functions in Elixir are also identified by the number of arguments they receive. We can check if a function is of any given arity by using <a href="kernel#is_function/2"><code class="inline">is_function/2</code></a>:</p>
<pre data-language="elixir"># check if add is a function that expects exactly 2 arguments
iex&gt; is_function(add, 2)
true
# check if add is a function that expects exactly 1 argument
iex&gt; is_function(add, 1)
false</pre>
<h2 id="closures" class="section-heading">  <span class="text">Closures</span> </h2> <p>Anonymous functions can also access variables that are in scope when the function is defined. This is typically referred to as closures, as they close over their scope. Let's define a new anonymous function that uses the <code class="inline">add</code> anonymous function we have previously defined:</p>
<pre data-language="elixir">iex&gt; double = fn a -&gt; add.(a, a) end
#Function&lt;6.71889879/1 in :erl_eval.expr/5&gt;
iex&gt; double.(2)
4</pre>
<p>A variable assigned inside a function does not affect its surrounding environment:</p>
<pre data-language="elixir">iex&gt; x = 42
42
iex&gt; (fn -&gt; x = 0 end).()
0
iex&gt; x
42</pre>
<h2 id="clauses-and-guards" class="section-heading">  <span class="text">Clauses and guards</span> </h2> <p>Similar to <a href="kernel.specialforms#case/2"><code class="inline">case/2</code></a>, we can pattern match on the arguments of anonymous functions as well as define multiple clauses and guards:</p>
<pre data-language="elixir">iex&gt; f = fn
...&gt;   x, y when x &gt; 0 -&gt; x + y
...&gt;   x, y -&gt; x * y
...&gt; end
#Function&lt;12.71889879/2 in :erl_eval.expr/5&gt;
iex&gt; f.(1, 3)
4
iex&gt; f.(-1, 3)
-3</pre>
<p>The number of arguments in each anonymous function clause needs to be the same, otherwise an error is raised.</p>
<pre data-language="elixir">iex&gt; f2 = fn
...&gt;   x, y when x &gt; 0 -&gt; x + y
...&gt;   x, y, z -&gt; x * y + z
...&gt; end
** (CompileError) iex:1: cannot mix clauses with different arities in anonymous functions</pre>
<h2 id="the-capture-operator" class="section-heading">  <span class="text">The capture operator</span> </h2> <p>Throughout this guide, we have been using the notation <code class="inline">name/arity</code> to refer to functions. It happens that this notation can actually be used to capture an existing function into a data-type we can pass around, similar to how anonymous functions behave.</p>
<pre data-language="elixir">iex&gt; fun = &amp;is_atom/1
&amp;:erlang.is_atom/1
iex&gt; is_function(fun)
true
iex&gt; fun.(:hello)
true
iex&gt; fun.(123)
false</pre>
<p>As you can see, once a function is captured, we can pass it as argument or invoke it using the anonymous function notation. The returned value above also hints we can capture functions defined in modules:</p>
<pre data-language="elixir">iex&gt; fun = &amp;String.length/1
&amp;String.length/1
iex&gt; fun.("hello")
5</pre>
<p>You can also capture operators:</p>
<pre data-language="elixir">iex&gt; add = &amp;+/2
&amp;:erlang.+/2
iex&gt; add.(1, 2)
3</pre>
<p>The capture syntax can also be used as a shortcut for creating functions. This is handy when you want to create functions that are mostly wrapping existing functions or operators:</p>
<pre data-language="elixir">iex&gt; fun = &amp;(&amp;1 + 1)
#Function&lt;6.71889879/1 in :erl_eval.expr/5&gt;
iex&gt; fun.(1)
2

iex&gt; fun2 = &amp;"Good #{&amp;1}"
#Function&lt;6.127694169/1 in :erl_eval.expr/5&gt;
iex&gt; fun2.("morning")
"Good morning"</pre>
<p>The <code class="inline">&amp;1</code> represents the first argument passed into the function. <code class="inline">&amp;(&amp;1 + 1)</code> above is exactly the same as <code class="inline">fn x -&gt; x + 1 end</code>. You can read more about the capture operator <code class="inline">&amp;</code> in <a href="kernel.specialforms#&amp;/1">its documentation</a>.</p>
<p>Next let's revisit some of the data-types we learned in the past and dig deeper into how they work.</p> </div> <div class="bottom-actions" id="bottom-actions"> <div class="bottom-actions-item"> <a href="case-cond-and-if" class="bottom-actions-button" rel="prev"> <span class="subheader"> ← Previous Page </span> <span class="title"> case, cond, and if </span> </a> </div> <div class="bottom-actions-item"> <a href="binaries-strings-and-charlists" class="bottom-actions-button" rel="next"> <span class="subheader"> Next Page → </span> <span class="title"> Binaries, strings, and charlists </span> </a> </div> </div> <footer class="footer"> <p> <span class="line"> <button class="a-main footer-button display-quick-switch" title="Search HexDocs packages"> Search HexDocs </button> <a href="elixir.epub" title="ePub version"> Download ePub version </a> </span> </p> <p class="built-using"> Built using <a href="https://github.com/elixir-lang/ex_doc" title="ExDoc" target="_blank" rel="help noopener" translate="no">ExDoc</a> (v0.34.1) for the <a href="https://elixir-lang.org" title="Elixir" target="_blank" translate="no">Elixir programming language</a> </p> </footer><div class="_attribution">
  <p class="_attribution-p">
    &copy; 2012-2024 The Elixir Team<br>Licensed under the Apache License, Version 2.0.<br>
    <a href="https://hexdocs.pm/elixir/1.17.2/anonymous-functions.html" class="_attribution-link">https://hexdocs.pm/elixir/1.17.2/anonymous-functions.html</a>
  </p>
</div>
