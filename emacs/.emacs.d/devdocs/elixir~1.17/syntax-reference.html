<div id="top-content"> <h1> <a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/pages/references/syntax-reference.md#L1" title="View Source" class="source" rel="help">Source</a> <span>Syntax reference</span> </h1> <p>Elixir syntax was designed to have a straightforward conversion to an abstract syntax tree (AST). This means the Elixir syntax is mostly uniform with a handful of "syntax sugar" constructs to reduce the noise in common Elixir idioms.</p>
<p>This document covers all of Elixir syntax constructs as a reference and then discuss their exact AST representation.</p>
<h2 id="reserved-words" class="section-heading">  <span class="text">Reserved words</span> </h2> <p>These are the reserved words in the Elixir language. They are detailed throughout this guide but summed up here for convenience:</p>
<ul>
<li>
<code class="inline">true</code>, <code class="inline">false</code>, <code class="inline">nil</code> - used as atoms</li>
<li>
<code class="inline">when</code>, <code class="inline">and</code>, <code class="inline">or</code>, <code class="inline">not</code>, <code class="inline">in</code> - used as operators</li>
<li>
<code class="inline">fn</code> - used for anonymous function definitions</li>
<li>
<code class="inline">do</code>, <code class="inline">end</code>, <code class="inline">catch</code>, <code class="inline">rescue</code>, <code class="inline">after</code>, <code class="inline">else</code> - used in do-end blocks</li>
</ul>
<h2 id="data-types" class="section-heading">  <span class="text">Data types</span> </h2> <h3 id="numbers" class="section-heading">  <span class="text">Numbers</span> </h3> <p>Integers (<code class="inline">1234</code>) and floats (<code class="inline">123.4</code>) in Elixir are represented as a sequence of digits that may be separated by underscore for readability purposes, such as <code class="inline">1_000_000</code>. Integers never contain a dot (<code class="inline">.</code>) in their representation. Floats contain a dot and at least one other digit after the dot. Floats also support the scientific notation, such as <code class="inline">123.4e10</code> or <code class="inline">123.4E10</code>.</p>
<h3 id="atoms" class="section-heading">  <span class="text">Atoms</span> </h3> <p>Unquoted atoms start with a colon (<code class="inline">:</code>) which must be immediately followed by a Unicode letter or an underscore. The atom may continue using a sequence of Unicode letters, numbers, underscores, and <code class="inline">@</code>. Atoms may end in <code class="inline">!</code> or <code class="inline">?</code>. Valid unquoted atoms are: <code class="inline">:ok</code>, <code class="inline">:ISO8601</code>, and <code class="inline">:integer?</code>.</p>
<p>If the colon is immediately followed by a pair of double- or single-quotes surrounding the atom name, the atom is considered quoted. In contrast with an unquoted atom, this one can be made of any Unicode character (not only letters), such as <code class="inline">:'🌢 Elixir'</code>, <code class="inline">:"++olá++"</code>, and <code class="inline">:"123"</code>.</p>
<p>Quoted and unquoted atoms with the same name are considered equivalent, so <code class="inline">:atom</code>, <code class="inline">:"atom"</code>, and <code class="inline">:'atom'</code> represent the same atom. The only catch is that the compiler will warn when quotes are used in atoms that do not need to be quoted.</p>
<p>All operators in Elixir are also valid atoms. Valid examples are <code class="inline">:foo</code>, <code class="inline">:FOO</code>, <code class="inline">:foo_42</code>, <code class="inline">:foo@bar</code>, and <code class="inline">:++</code>. Invalid examples are <code class="inline">:@foo</code> (<code class="inline">@</code> is not allowed at start), <code class="inline">:123</code> (numbers are not allowed at start), and <code class="inline">:(*)</code> (not a valid operator).</p>
<p><code class="inline">true</code>, <code class="inline">false</code>, and <code class="inline">nil</code> are reserved words that are represented by the atoms <code class="inline">:true</code>, <code class="inline">:false</code> and <code class="inline">:nil</code> respectively.</p>
<p>To learn more about all Unicode characters allowed in atom, see the <a href="unicode-syntax">Unicode syntax</a> document.</p>
<h3 id="strings" class="section-heading">  <span class="text">Strings</span> </h3> <p>Single-line strings in Elixir are written between double-quotes, such as <code class="inline">"foo"</code>. Any double-quote inside the string must be escaped with <code class="inline">\</code>. Strings support Unicode characters and are stored as UTF-8 encoded binaries.</p>
<p>Multi-line strings in Elixir are written with three double-quotes, and can have unescaped quotes within them. The resulting string will end with a newline. The indentation of the last <code class="inline">"""</code> is used to strip indentation from the inner string. For example:</p>
<pre data-language="elixir">iex&gt; test = """
...&gt;     this
...&gt;     is
...&gt;     a
...&gt;     test
...&gt; """
"    this\n    is\n    a\n    test\n"
iex&gt; test = """
...&gt;     This
...&gt;     Is
...&gt;     A
...&gt;     Test
...&gt;     """
"This\nIs\nA\nTest\n"</pre>
<p>Strings are always represented as themselves in the AST.</p>
<h3 id="charlists" class="section-heading">  <span class="text">Charlists</span> </h3> <p>Charlists in Elixir are written in single-quotes, such as <code class="inline">'foo'</code>. Any single-quote inside the string must be escaped with <code class="inline">\</code>. Charlists are made of non-negative integers, where each integer represents a Unicode code point.</p>
<p>Multi-line charlists are written with three single-quotes (<code class="inline">'''</code>), the same way multi-line strings are.</p>
<p>Charlists are always represented as themselves in the AST.</p>
<p>For more in-depth information, please read the "Charlists" section in the <a href="list"><code class="inline">List</code></a> module.</p>
<h3 id="lists-tuples-and-binaries" class="section-heading">  <span class="text">Lists, tuples and binaries</span> </h3> <p>Data structures such as lists, tuples, and binaries are marked respectively by the delimiters <code class="inline">[...]</code>, <code class="inline">{...}</code>, and <code class="inline">&lt;&lt;...&gt;&gt;</code>. Each element is separated by comma. A trailing comma is also allowed, such as in <code class="inline">[1, 2, 3,]</code>.</p>
<h3 id="maps-and-keyword-lists" class="section-heading">  <span class="text">Maps and keyword lists</span> </h3> <p>Maps use the <code class="inline">%{...}</code> notation and each key-value is given by pairs marked with <code class="inline">=&gt;</code>, such as <code class="inline">%{"hello" =&gt; 1, 2 =&gt; "world"}</code>.</p>
<p>Both keyword lists (list of two-element tuples where the first element is atom) and maps with atom keys support a keyword notation where the colon character <code class="inline">:</code> is moved to the end of the atom. <code class="inline">%{hello: "world"}</code> is equivalent to <code class="inline">%{:hello =&gt; "world"}</code> and <code class="inline">[foo: :bar]</code> is equivalent to <code class="inline">[{:foo, :bar}]</code>. This notation is a syntax sugar that emits the same AST representation. It will be explained in later sections.</p>
<h3 id="structs" class="section-heading">  <span class="text">Structs</span> </h3> <p>Structs built on the map syntax by passing the struct name between <code class="inline">%</code> and <code class="inline">{</code>. For example, <code class="inline">%User{...}</code>.</p>
<h2 id="expressions" class="section-heading">  <span class="text">Expressions</span> </h2> <h3 id="variables" class="section-heading">  <span class="text">Variables</span> </h3> <p>Variables in Elixir must start with an underscore or a Unicode letter that is not in uppercase or titlecase. The variable may continue using a sequence of Unicode letters, numbers, and underscores. Variables may end in <code class="inline">?</code> or <code class="inline">!</code>. To learn more about all Unicode characters allowed in variables, see the <a href="unicode-syntax">Unicode syntax</a> document.</p>
<p><a href="naming-conventions">Elixir's naming conventions</a> recommend variables to be in <code class="inline">snake_case</code> format.</p>
<h3 id="non-qualified-calls-local-calls" class="section-heading">  <span class="text">Non-qualified calls (local calls)</span> </h3> <p>Non-qualified calls, such as <code class="inline">add(1, 2)</code>, must start with characters and then follow the same rules as variables, which are optionally followed by parentheses, and then arguments.</p>
<p>Parentheses are required for zero-arity calls (i.e. calls without arguments), to avoid ambiguity with variables. If parentheses are used, they must immediately follow the function name <em>without spaces</em>. For example, <code class="inline">add (1, 2)</code> is a syntax error, since <code class="inline">(1, 2)</code> is treated as an invalid block which is attempted to be given as a single argument to <code class="inline">add</code>.</p>
<p><a href="naming-conventions">Elixir's naming conventions</a> recommend calls to be in <code class="inline">snake_case</code> format.</p>
<h3 id="operators" class="section-heading">  <span class="text">Operators</span> </h3> <p>As many programming languages, Elixir also support operators as non-qualified calls with their precedence and associativity rules. Constructs such as <code class="inline">=</code>, <code class="inline">when</code>, <code class="inline">&amp;</code> and <code class="inline">@</code> are simply treated as operators. See <a href="operators">the Operators page</a> for a full reference.</p>
<h3 id="qualified-calls-remote-calls" class="section-heading">  <span class="text">Qualified calls (remote calls)</span> </h3> <p>Qualified calls, such as <code class="inline">Math.add(1, 2)</code>, must start with characters and then follow the same rules as variables, which are optionally followed by parentheses, and then arguments. Qualified calls also support operators, such as <code class="inline">Kernel.+(1, 2)</code>. Elixir also allows the function name to be written between double- or single-quotes, allowing any character in between the quotes, such as <code class="inline">Math."++add++"(1, 2)</code>.</p>
<p>Similar to non-qualified calls, parentheses have different meaning for zero-arity calls (i.e. calls without arguments). If parentheses are used, such as <code class="inline">mod.fun()</code>, it means a function call. If parenthesis are skipped, such as <code class="inline">map.field</code>, it means accessing a field of a map.</p>
<p><a href="naming-conventions">Elixir's naming conventions</a> recommend calls to be in <code class="inline">snake_case</code> format.</p>
<h3 id="aliases" class="section-heading">  <span class="text">Aliases</span> </h3> <p>Aliases are constructs that expand to atoms at compile-time. The alias <a href="string"><code class="inline">String</code></a> expands to the atom <code class="inline">:"Elixir.String"</code>. Aliases must start with an ASCII uppercase character which may be followed by any ASCII letter, number, or underscore. Non-ASCII characters are not supported in aliases.</p>
<p>Multiple aliases can be joined with <code class="inline">.</code>, such as <code class="inline">MyApp.String</code>, and it expands to the atom <code class="inline">:"Elixir.MyApp.String"</code>. The dot is effectively part of the name but it can also be used for composition. If you define <code class="inline">alias MyApp.Example, as: Example</code> in your code, then <code class="inline">Example</code> will always expand to <code class="inline">:"Elixir.MyApp.Example"</code> and <code class="inline">Example.String</code> will expand to <code class="inline">:"Elixir.MyApp.Example.String"</code>.</p>
<p><a href="naming-conventions">Elixir's naming conventions</a> recommend aliases to be in <code class="inline">CamelCase</code> format.</p>
<h3 id="module-attributes" class="section-heading">  <span class="text">Module attributes</span> </h3> <p>Module attributes are module-specific storage and are written as the composition of the unary operator <code class="inline">@</code> with variables and local calls. For example, to write to a module attribute named <code class="inline">foo</code>, use <code class="inline">@foo "value"</code>, and use <code class="inline">@foo</code> to read from it. Given module attributes are written using existing constructs, they follow the same rules above defined for operators, variables, and local calls.</p>
<h3 id="blocks" class="section-heading">  <span class="text">Blocks</span> </h3> <p>Blocks are multiple Elixir expressions separated by newlines or semi-colons. A new block may be created at any moment by using parentheses.</p>
<h3 id="left-to-right-arrow" class="section-heading">  <span class="text">Left to right arrow</span> </h3> <p>The left to right arrow (<code class="inline">-&gt;</code>) is used to establish a relationship between left and right, commonly referred as clauses. The left side may have zero, one, or more arguments; the right side is zero, one, or more expressions separated by new line. The <code class="inline">-&gt;</code> may appear one or more times between one of the following terminators: <code class="inline">do</code>-<code class="inline">end</code>, <code class="inline">fn</code>-<code class="inline">end</code> or <code class="inline">(</code>-<code class="inline">)</code>. When <code class="inline">-&gt;</code> is used, only other clauses are allowed between those terminators. Mixing clauses and regular expressions is invalid syntax.</p>
<p>It is seen on <code class="inline">case</code> and <code class="inline">cond</code> constructs between <code class="inline">do</code> and <code class="inline">end</code>:</p>
<pre data-language="elixir">case 1 do
  2 -&gt; 3
  4 -&gt; 5
end

cond do
  true -&gt; false
end</pre>
<p>Seen in typespecs between <code class="inline">(</code> and <code class="inline">)</code>:</p>
<pre data-language="elixir">(integer(), boolean() -&gt; integer())</pre>
<p>It is also used between <code class="inline">fn</code> and <code class="inline">end</code> for building anonymous functions:</p>
<pre data-language="elixir">fn
  x, y -&gt; x + y
end</pre>
<h3 id="sigils" class="section-heading">  <span class="text">Sigils</span> </h3> <p>Sigils start with <code class="inline">~</code> and are followed by one lowercase letter or by one or more uppercase letters, immediately followed by one of the following pairs:</p>
<ul>
<li>
<code class="inline">(</code> and <code class="inline">)</code>
</li>
<li>
<code class="inline">{</code> and <code class="inline">}</code>
</li>
<li>
<code class="inline">[</code> and <code class="inline">]</code>
</li>
<li>
<code class="inline">&lt;</code> and <code class="inline">&gt;</code>
</li>
<li>
<code class="inline">"</code> and <code class="inline">"</code>
</li>
<li>
<code class="inline">'</code> and <code class="inline">'</code>
</li>
<li>
<code class="inline">|</code> and <code class="inline">|</code>
</li>
<li>
<code class="inline">/</code> and <code class="inline">/</code>
</li>
</ul>
<p>After closing the pair, zero or more ASCII letters and digits can be given as a modifier. Sigils are expressed as non-qualified calls prefixed with <code class="inline">sigil_</code> where the first argument is the sigil contents as a string and the second argument is a list of integers as modifiers:</p>
<p>If the sigil letter is in uppercase, no interpolation is allowed in the sigil, otherwise its contents may be dynamic. Compare the results of the sigils below for more information:</p>
<pre data-language="elixir">~s/f#{"o"}o/
~S/f#{"o"}o/</pre>
<p>Sigils are useful to encode text with their own escaping rules, such as regular expressions, datetimes, and others.</p>
<h2 id="the-elixir-ast" class="section-heading">  <span class="text">The Elixir AST</span> </h2> <p>Elixir syntax was designed to have a straightforward conversion to an abstract syntax tree (AST). Elixir's AST is a regular Elixir data structure composed of the following elements:</p>
<ul>
<li>atoms - such as <code class="inline">:foo</code>
</li>
<li>integers - such as <code class="inline">42</code>
</li>
<li>floats - such as <code class="inline">13.1</code>
</li>
<li>strings - such as <code class="inline">"hello"</code>
</li>
<li>lists - such as <code class="inline">[1, 2, 3]</code>
</li>
<li>tuples with two elements - such as <code class="inline">{"hello", :world}</code>
</li>
<li>tuples with three elements, representing calls or variables, as explained next</li>
</ul>
<p>The building block of Elixir's AST is a call, such as:</p>
<pre data-language="elixir">sum(1, 2, 3)</pre>
<p>which is represented as a tuple with three elements:</p>
<pre data-language="elixir">{:sum, meta, [1, 2, 3]}</pre>
<p>the first element is an atom (or another tuple), the second element is a list of two-element tuples with metadata (such as line numbers) and the third is a list of arguments.</p>
<p>We can retrieve the AST for any Elixir expression by calling <code class="inline">quote</code>:</p>
<pre data-language="elixir">quote do
  sum()
end
#=&gt; {:sum, [], []}</pre>
<p>Variables are also represented using a tuple with three elements and a combination of lists and atoms, for example:</p>
<pre data-language="elixir">quote do
  sum
end
#=&gt; {:sum, [], Elixir}</pre>
<p>You can see that variables are also represented with a tuple, except the third element is an atom expressing the variable context.</p>
<p>Over the course of this section, we will explore many Elixir syntax constructs alongside their AST representations.</p>
<h3 id="operators-1" class="section-heading">  <span class="text">Operators</span> </h3> <p>Operators are treated as non-qualified calls:</p>
<pre data-language="elixir">quote do
  1 + 2
end
#=&gt; {:+, [], [1, 2]}</pre>
<p>Note that <code class="inline">.</code> is also an operator. Remote calls use the dot in the AST with two arguments, where the second argument is always an atom:</p>
<pre data-language="elixir">quote do
  foo.bar(1, 2, 3)
end
#=&gt; {{:., [], [{:foo, [], Elixir}, :bar]}, [], [1, 2, 3]}</pre>
<p>Calling anonymous functions uses the dot in the AST with a single argument, mirroring the fact the function name is "missing" from right side of the dot:</p>
<pre data-language="elixir">quote do
  foo.(1, 2, 3)
end
#=&gt; {{:., [], [{:foo, [], Elixir}]}, [], [1, 2, 3]}</pre>
<h3 id="aliases-1" class="section-heading">  <span class="text">Aliases</span> </h3> <p>Aliases are represented by an <code class="inline">__aliases__</code> call with each segment separated by a dot as an argument:</p>
<pre data-language="elixir">quote do
  Foo.Bar.Baz
end
#=&gt; {:__aliases__, [], [:Foo, :Bar, :Baz]}

quote do
  __MODULE__.Bar.Baz
end
#=&gt; {:__aliases__, [], [{:__MODULE__, [], Elixir}, :Bar, :Baz]}</pre>
<p>All arguments, except the first, are guaranteed to be atoms.</p>
<h3 id="data-structures" class="section-heading">  <span class="text">Data structures</span> </h3> <p>Remember that lists are literals, so they are represented as themselves in the AST:</p>
<pre data-language="elixir">quote do
  [1, 2, 3]
end
#=&gt; [1, 2, 3]</pre>
<p>Tuples have their own representation, except for two-element tuples, which are represented as themselves:</p>
<pre data-language="elixir">quote do
  {1, 2}
end
#=&gt; {1, 2}

quote do
  {1, 2, 3}
end
#=&gt; {:{}, [], [1, 2, 3]}</pre>
<p>Binaries have a representation similar to tuples, except they are tagged with <code class="inline">:&lt;&lt;&gt;&gt;</code> instead of <code class="inline">:{}</code>:</p>
<pre data-language="elixir">quote do
  &lt;&lt;1, 2, 3&gt;&gt;
end
#=&gt; {:&lt;&lt;&gt;&gt;, [], [1, 2, 3]}</pre>
<p>The same applies to maps, where pairs are treated as a list of tuples with two elements:</p>
<pre data-language="elixir">quote do
  %{1 =&gt; 2, 3 =&gt; 4}
end
#=&gt; {:%{}, [], [{1, 2}, {3, 4}]}</pre>
<h3 id="blocks-1" class="section-heading">  <span class="text">Blocks</span> </h3> <p>Blocks are represented as a <code class="inline">__block__</code> call with each line as a separate argument:</p>
<pre data-language="elixir">quote do
  1
  2
  3
end
#=&gt; {:__block__, [], [1, 2, 3]}

quote do 1; 2; 3; end
#=&gt; {:__block__, [], [1, 2, 3]}</pre>
<h3 id="left-to-right-arrow-1" class="section-heading">  <span class="text">Left to right arrow</span> </h3> <p>The left to right arrow (<code class="inline">-&gt;</code>) is represented similar to operators except that they are always part of a list, its left side represents a list of arguments and the right side is an expression.</p>
<p>For example, in <code class="inline">case</code> and <code class="inline">cond</code>:</p>
<pre data-language="elixir">quote do
  case 1 do
    2 -&gt; 3
    4 -&gt; 5
  end
end
#=&gt; {:case, [], [1, [do: [{:-&gt;, [], [[2], 3]}, {:-&gt;, [], [[4], 5]}]]]}

quote do
  cond do
    true -&gt; false
  end
end
#=&gt; {:cond, [], [[do: [{:-&gt;, [], [[true], false]}]]]}</pre>
<p>Between <code class="inline">(</code> and <code class="inline">)</code>:</p>
<pre data-language="elixir">quote do
  (1, 2 -&gt; 3
   4, 5 -&gt; 6)
end
#=&gt; [{:-&gt;, [], [[1, 2], 3]}, {:-&gt;, [], [[4, 5], 6]}]</pre>
<p>Between <code class="inline">fn</code> and <code class="inline">end</code>:</p>
<pre data-language="elixir">quote do
  fn
    1, 2 -&gt; 3
    4, 5 -&gt; 6
  end
end
#=&gt; {:fn, [], [{:-&gt;, [], [[1, 2], 3]}, {:-&gt;, [], [[4, 5], 6]}]}</pre>
<h3 id="qualified-tuples" class="section-heading">  <span class="text">Qualified tuples</span> </h3> <p>Qualified tuples (<code class="inline">foo.{bar, baz}</code>) are represented by a <code class="inline">{:., [], [expr, :{}]}</code> call, where the <code class="inline">expr</code> represents the left hand side of the dot, and the arguments represent the elements inside the curly braces. This is used in Elixir to provide multi aliases:</p>
<pre data-language="elixir">quote do
  Foo.{Bar, Baz}
end
#=&gt; {{:., [], [{:__aliases__, [], [:Foo]}, :{}]}, [], [{:__aliases__, [], [:Bar]}, {:__aliases__, [], [:Baz]}]}</pre>
<h2 id="optional-syntax" class="section-heading">  <span class="text">Optional syntax</span> </h2> <p>All of the constructs above are part of Elixir's syntax and have their own representation as part of the Elixir AST. This section will discuss the remaining constructs that are alternative representations of the constructs above. In other words, the constructs below can be represented in more than one way in your Elixir code and retain AST equivalence. We call this "Optional Syntax".</p>
<p>For a lightweight introduction to Elixir's Optional Syntax, <a href="optional-syntax">see this document</a>. Below we continue with a more complete reference.</p>
<h3 id="integers-in-other-bases-and-unicode-code-points" class="section-heading">  <span class="text">Integers in other bases and Unicode code points</span> </h3> <p>Elixir allows integers to contain <code class="inline">_</code> to separate digits and provides conveniences to represent integers in other bases:</p>
<pre data-language="elixir">1_000_000
#=&gt; 1000000

0xABCD
#=&gt; 43981 (Hexadecimal base)

0o01234567
#=&gt; 342391 (Octal base)

0b10101010
#=&gt; 170 (Binary base)

?é
#=&gt; 233 (Unicode code point)</pre>
<p>Those constructs exist only at the syntax level. All of the examples above are represented as their underlying integers in the AST.</p>
<h3 id="access-syntax" class="section-heading">  <span class="text">Access syntax</span> </h3> <p>The access syntax is represented as a call to <a href="access#get/2"><code class="inline">Access.get/2</code></a>:</p>
<pre data-language="elixir">quote do
  opts[arg]
end
#=&gt; {{:., [], [Access, :get]}, [], [{:opts, [], Elixir}, {:arg, [], Elixir}]}</pre>
<h3 id="optional-parentheses" class="section-heading">  <span class="text">Optional parentheses</span> </h3> <p>Elixir provides optional parentheses on local and remote calls with one or more arguments:</p>
<pre data-language="elixir">quote do
  sum 1, 2, 3
end
#=&gt; {:sum, [], [1, 2, 3]}</pre>
<p>The above is treated the same as <code class="inline">sum(1, 2, 3)</code> by the parser. You can remove the parentheses on all calls with at least one argument.</p>
<p>You can also skip parentheses on qualified calls, such as <code class="inline">Foo.bar 1, 2, 3</code>. Parentheses are required when invoking anonymous functions, such as <code class="inline">f.(1, 2, 3)</code>.</p>
<p>In practice, developers prefer to add parentheses to most of their calls. They are skipped mainly in Elixir's control-flow constructs, such as <code class="inline">defmodule</code>, <code class="inline">if</code>, <code class="inline">case</code>, etc, and in certain DSLs.</p>
<h3 id="keywords" class="section-heading">  <span class="text">Keywords</span> </h3> <p>Keywords in Elixir are a list of tuples of two elements, where the first element is an atom. Using the base constructs, they would be represented as:</p>
<pre data-language="elixir">[{:foo, 1}, {:bar, 2}]</pre>
<p>However, Elixir introduces a syntax sugar where the keywords above may be written as follows:</p>
<pre data-language="elixir">[foo: 1, bar: 2]</pre>
<p>Atoms with foreign characters, such as whitespace, must be wrapped in quotes. This rule applies to keywords as well:</p>
<pre data-language="elixir">[{:"foo bar", 1}, {:"bar baz", 2}] == ["foo bar": 1, "bar baz": 2]</pre>
<p>Remember that, because lists and two-element tuples are quoted literals, by definition keywords are also literals (in fact, the only reason tuples with two elements are quoted literals is to support keywords as literals).</p>
<p>In order to be valid keyword syntax, <code class="inline">:</code> cannot be preceded by any whitespace (<code class="inline">foo : 1</code> is invalid) and has to be followed by whitespace (<code class="inline">foo:1</code> is invalid).</p>
<h3 id="keywords-as-last-arguments" class="section-heading">  <span class="text">Keywords as last arguments</span> </h3> <p>Elixir also supports a syntax where if the last argument of a call is a keyword list then the square brackets can be skipped. This means that the following:</p>
<pre data-language="elixir">if(condition, do: this, else: that)</pre>
<p>is the same as</p>
<pre data-language="elixir">if(condition, [do: this, else: that])</pre>
<p>which in turn is the same as</p>
<pre data-language="elixir">if(condition, [{:do, this}, {:else, that}])</pre>
<h3 id="do-end-blocks" class="section-heading">  <span class="text"><code class="inline">do</code>-<code class="inline">end</code> blocks</span> </h3> <p>The last syntax convenience are <code class="inline">do</code>-<code class="inline">end</code> blocks. <code class="inline">do</code>-<code class="inline">end</code> blocks are equivalent to keywords as the last argument of a function call, where the block contents are wrapped in parentheses. For example:</p>
<pre data-language="elixir">if true do
  this
else
  that
end</pre>
<p>is the same as:</p>
<pre data-language="elixir">if(true, do: (this), else: (that))</pre>
<p>which we have explored in the previous section.</p>
<p>Parentheses are important to support multiple expressions. This:</p>
<pre data-language="elixir">if true do
  this
  that
end</pre>
<p>is the same as:</p>
<pre data-language="elixir">if(true, do: (
  this
  that
))</pre>
<p>Inside <code class="inline">do</code>-<code class="inline">end</code> blocks you may introduce other keywords, such as <code class="inline">else</code> used in the <code class="inline">if</code> above. The supported keywords between <code class="inline">do</code>-<code class="inline">end</code> are static and are:</p>
<ul>
<li><code class="inline">after</code></li>
<li><code class="inline">catch</code></li>
<li><code class="inline">else</code></li>
<li><code class="inline">rescue</code></li>
</ul>
<p>You can see them being used in constructs such as <code class="inline">receive</code>, <code class="inline">try</code>, and others.</p> </div> <div class="bottom-actions" id="bottom-actions"> <div class="bottom-actions-item"> <a href="patterns-and-guards" class="bottom-actions-button" rel="prev"> <span class="subheader"> ← Previous Page </span> <span class="title"> Patterns and guards </span> </a> </div> <div class="bottom-actions-item"> <a href="typespecs" class="bottom-actions-button" rel="next"> <span class="subheader"> Next Page → </span> <span class="title"> Typespecs reference </span> </a> </div> </div> <footer class="footer"> <p> <span class="line"> <button class="a-main footer-button display-quick-switch" title="Search HexDocs packages"> Search HexDocs </button> <a href="elixir.epub" title="ePub version"> Download ePub version </a> </span> </p> <p class="built-using"> Built using <a href="https://github.com/elixir-lang/ex_doc" title="ExDoc" target="_blank" rel="help noopener" translate="no">ExDoc</a> (v0.34.1) for the <a href="https://elixir-lang.org" title="Elixir" target="_blank" translate="no">Elixir programming language</a> </p> </footer><div class="_attribution">
  <p class="_attribution-p">
    &copy; 2012-2024 The Elixir Team<br>Licensed under the Apache License, Version 2.0.<br>
    <a href="https://hexdocs.pm/elixir/1.17.2/syntax-reference.html" class="_attribution-link">https://hexdocs.pm/elixir/1.17.2/syntax-reference.html</a>
  </p>
</div>
