<div id="top-content"> <h1> <a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/pages/getting-started/keywords-and-maps.md#L1" title="View Source" class="source" rel="help">Source</a> <span>Keyword lists and maps</span> </h1> <p>Now let's talk about associative data structures. Associative data structures are able to associate a key to a certain value. Different languages call these different names like dictionaries, hashes, associative arrays, etc.</p>
<p>In Elixir, we have two main associative data structures: keyword lists and maps.</p>
<h2 id="keyword-lists" class="section-heading">  <span class="text">Keyword lists</span> </h2> <p>Keyword lists are a data-structure used to pass options to functions. Imagine you want to split a string of numbers. We can use <a href="string#split/2"><code class="inline">String.split/2</code></a>:</p>
<pre data-language="elixir">iex&gt; String.split("1 2 3", " ")
["1", "2", "3"]</pre>
<p>However, what happens if there is an additional space between the numbers:</p>
<pre data-language="elixir">iex&gt; String.split("1  2  3", " ")
["1", "", "2", "", "3"]</pre>
<p>As you can see, there are now empty strings in our results. Luckily, the <a href="string#split/3"><code class="inline">String.split/3</code></a> function allows the <code class="inline">trim</code> option to be set to true:</p>
<pre data-language="elixir">iex&gt; String.split("1  2  3", " ", [trim: true])
["1", "2", "3"]</pre>
<p><code class="inline">[trim: true]</code> is a keyword list. Furthermore, when a keyword list is the last argument of a function, we can skip the brackets and write:</p>
<pre data-language="elixir">iex&gt; String.split("1  2  3", " ", trim: true)
["1", "2", "3"]</pre>
<p>As shown in the example above, keyword lists are mostly used as optional arguments to functions.</p>
<p>As the name implies, keyword lists are simply lists. In particular, they are lists consisting of 2-item tuples where the first element (the key) is an atom and the second element can be any value. Both representations are the same:</p>
<pre data-language="elixir">iex&gt; [{:trim, true}] == [trim: true]
true</pre>
<p>Since keyword lists are lists, we can use all operations available to lists. For example, we can use <code class="inline">++</code> to add new values to a keyword list:</p>
<pre data-language="elixir">iex&gt; list = [a: 1, b: 2]
[a: 1, b: 2]
iex&gt; list ++ [c: 3]
[a: 1, b: 2, c: 3]
iex&gt; [a: 0] ++ list
[a: 0, a: 1, b: 2]</pre>
<p>You can read the value of a keyword list using the brackets syntax. This is also known as the access syntax, as it is defined by the <a href="access"><code class="inline">Access</code></a> module:</p>
<pre data-language="elixir">iex&gt; list[:a]
1
iex&gt; list[:b]
2</pre>
<p>In case of duplicate keys, values added to the front are the ones fetched:</p>
<pre data-language="elixir">iex&gt; new_list = [a: 0] ++ list
[a: 0, a: 1, b: 2]
iex&gt; new_list[:a]
0</pre>
<p>Keyword lists are important because they have three special characteristics:</p>
<ul>
<li>Keys must be atoms.</li>
<li>Keys are ordered, as specified by the developer.</li>
<li>Keys can be given more than once.</li>
</ul>
<p>For example, <a href="https://github.com/elixir-lang/ecto">the Ecto library</a> makes use of these features to provide an elegant DSL for writing database queries:</p>
<pre data-language="elixir">query =
  from w in Weather,
    where: w.prcp &gt; 0,
    where: w.temp &lt; 20,
    select: w</pre>
<p>Although we can pattern match on keyword lists, it is not done in practice since pattern matching on lists requires the number of items and their order to match:</p>
<pre data-language="elixir">iex&gt; [a: a] = [a: 1]
[a: 1]
iex&gt; a
1
iex&gt; [a: a] = [a: 1, b: 2]
** (MatchError) no match of right hand side value: [a: 1, b: 2]
iex&gt; [b: b, a: a] = [a: 1, b: 2]
** (MatchError) no match of right hand side value: [a: 1, b: 2]</pre>
<p>Furthermore, given keyword lists are often used as optional arguments, they are used in situations where not all keys may be present, which would make it impossible to match on them. In a nutshell, do not pattern match on keyword lists.</p>
<p>In order to manipulate keyword lists, Elixir provides the <a href="keyword"><code class="inline">Keyword</code></a> module. Remember, though, keyword lists are simply lists, and as such they provide the same linear performance characteristics as them: the longer the list, the longer it will take to find a key, to count the number of items, and so on. If you need to store a large amount of keys in a key-value data structure, Elixir offers maps, which we will soon learn.</p>
<h3 id="do-blocks-and-keywords" class="section-heading">  <span class="text"><code class="inline">do</code>-blocks and keywords</span> </h3> <p>As we have seen, keywords are mostly used in the language to pass optional values. In fact, we have used keywords in earlier chapters. For example, we have seen:</p>
<pre data-language="elixir">iex&gt; if true do
...&gt;   "This will be seen"
...&gt; else
...&gt;   "This won't"
...&gt; end
"This will be seen"</pre>
<p>It happens that <code class="inline">do</code> blocks are nothing more than a syntax convenience on top of keywords. We can rewrite the above to:</p>
<pre data-language="elixir">iex&gt; if true, do: "This will be seen", else: "This won't"
"This will be seen"</pre>
<p>Pay close attention to both syntaxes. In the keyword list format, we separate each key-value pair with commas, and each key is followed by <code class="inline">:</code>. In the <code class="inline">do</code>-blocks, we get rid of the colons, the commas, and separate each keyword by a newline. They are useful exactly because they remove the verbosity when writing blocks of code. Most of the time, you will use the block syntax, but it is good to know they are equivalent.</p>
<p>This plays an important role in the language as it allows Elixir syntax to stay small but still expressive. We only need few data structures to represent the language, a topic we will come back to when talking about <a href="optional-syntax">optional syntax</a> and go in-depth when discussing <a href="quote-and-unquote">meta-programming</a>.</p>
<p>With this out of the way, let's talk about maps.</p>
<h2 id="maps-as-key-value-pairs" class="section-heading">  <span class="text">Maps as key-value pairs</span> </h2> <p>Whenever you need to store key-value pairs, maps are the "go to" data structure in Elixir. A map is created using the <code class="inline">%{}</code> syntax:</p>
<pre data-language="elixir">iex&gt; map = %{:a =&gt; 1, 2 =&gt; :b}
%{2 =&gt; :b, :a =&gt; 1}
iex&gt; map[:a]
1
iex&gt; map[2]
:b
iex&gt; map[:c]
nil</pre>
<p>Compared to keyword lists, we can already see two differences:</p>
<ul>
<li>Maps allow any value as a key.</li>
<li>Maps' keys do not follow any ordering.</li>
</ul>
<p>In contrast to keyword lists, maps are very useful with pattern matching. When a map is used in a pattern, it will always match on a subset of the given value:</p>
<pre data-language="elixir">iex&gt; %{} = %{:a =&gt; 1, 2 =&gt; :b}
%{2 =&gt; :b, :a =&gt; 1}
iex&gt; %{:a =&gt; a} = %{:a =&gt; 1, 2 =&gt; :b}
%{2 =&gt; :b, :a =&gt; 1}
iex&gt; a
1
iex&gt; %{:c =&gt; c} = %{:a =&gt; 1, 2 =&gt; :b}
** (MatchError) no match of right hand side value: %{2 =&gt; :b, :a =&gt; 1}</pre>
<p>As shown above, a map matches as long as the keys in the pattern exist in the given map. Therefore, an empty map matches all maps.</p>
<p>The <a href="map"><code class="inline">Map</code></a> module provides a very similar API to the <a href="keyword"><code class="inline">Keyword</code></a> module with convenience functions to add, remove, and update maps keys:</p>
<pre data-language="elixir">iex&gt; Map.get(%{:a =&gt; 1, 2 =&gt; :b}, :a)
1
iex&gt; Map.put(%{:a =&gt; 1, 2 =&gt; :b}, :c, 3)
%{2 =&gt; :b, :a =&gt; 1, :c =&gt; 3}
iex&gt; Map.to_list(%{:a =&gt; 1, 2 =&gt; :b})
[{2, :b}, {:a, 1}]</pre>
<h2 id="maps-of-predefined-keys" class="section-heading">  <span class="text">Maps of predefined keys</span> </h2> <p>In the previous section, we have used maps as a key-value data structure where keys can be added or removed at any time. However, it is also common to create maps with a pre-defined set of keys. Their values may be updated, but new keys are never added nor removed. This is useful when we know the shape of the data we are working with and, if we get a different key, it likely means a mistake was done elsewhere.</p>
<p>We define such maps using the same syntax as in the previous section, except that all keys must be atoms:</p>
<pre data-language="elixir">iex&gt; map = %{:name =&gt; "John", :age =&gt; 23}
%{name: "John", age: 23}</pre>
<p>As you can see from the printed result above, Elixir also allows you to write maps of atom keys using the same <code class="inline">key: value</code> syntax as keyword lists.</p>
<p>When the keys are atoms, in particular when working with maps of predefined keys, we can also access them using the <code class="inline">map.key</code> syntax:</p>
<pre data-language="elixir">iex&gt; map = %{name: "John", age: 23}
%{name: "John", age: 23}

iex&gt; map.name
"John"
iex&gt; map.agee
** (KeyError) key :agee not found in: %{name: "John", age: 23}</pre>
<p>There is also syntax for updating keys, which also raises if the key has not yet been defined:</p>
<pre data-language="elixir">iex&gt; %{map | name: "Mary"}
%{name: "Mary", age: 23}
iex&gt; %{map | agee: 27}
** (KeyError) key :agee not found in: %{name: "John", age: 23}</pre>
<p>These operations have one large benefit in that they raise if the key does not exist in the map and the compiler may even detect and warn when possible. This makes them useful to get quick feedback and spot bugs and typos early on. This is also the syntax used to power another Elixir feature called "Structs", which we will learn later on.</p>
<p>Elixir developers typically prefer to use the <code class="inline">map.key</code> syntax and pattern matching instead of the functions in the <a href="map"><code class="inline">Map</code></a> module when working with maps because they lead to an assertive style of programming. <a href="https://dashbit.co/blog/writing-assertive-code-with-elixir">This blog post by José Valim</a> provides insight and examples on how you get more concise and faster software by writing assertive code in Elixir.</p>
<h2 id="nested-data-structures" class="section-heading">  <span class="text">Nested data structures</span> </h2> <p>Often we will have maps inside maps, or even keywords lists inside maps, and so forth. Elixir provides conveniences for manipulating nested data structures via the <a href="kernel#get_in/1"><code class="inline">get_in/1</code></a>, <a href="kernel#put_in/2"><code class="inline">put_in/2</code></a>, <a href="kernel#update_in/2"><code class="inline">update_in/2</code></a>, and other macros giving the same conveniences you would find in imperative languages while keeping the immutable properties of the language.</p>
<p>Imagine you have the following structure:</p>
<pre data-language="elixir">iex&gt; users = [
  john: %{name: "John", age: 27, languages: ["Erlang", "Ruby", "Elixir"]},
  mary: %{name: "Mary", age: 29, languages: ["Elixir", "F#", "Clojure"]}
]
[
  john: %{age: 27, languages: ["Erlang", "Ruby", "Elixir"], name: "John"},
  mary: %{age: 29, languages: ["Elixir", "F#", "Clojure"], name: "Mary"}
]</pre>
<p>We have a keyword list of users where each value is a map containing the name, age and a list of programming languages each user likes. If we wanted to access the age for john, we could write:</p>
<pre data-language="elixir">iex&gt; users[:john].age
27</pre>
<p>It happens we can also use this same syntax for updating the value:</p>
<pre data-language="elixir">iex&gt; users = put_in users[:john].age, 31
[
  john: %{age: 31, languages: ["Erlang", "Ruby", "Elixir"], name: "John"},
  mary: %{age: 29, languages: ["Elixir", "F#", "Clojure"], name: "Mary"}
]</pre>
<p>The <a href="kernel#update_in/2"><code class="inline">update_in/2</code></a> macro is similar but allows us to pass a function that controls how the value changes. For example, let's remove "Clojure" from Mary's list of languages:</p>
<pre data-language="elixir">iex&gt; users = update_in users[:mary].languages, fn languages -&gt; List.delete(languages, "Clojure") end
[
  john: %{age: 31, languages: ["Erlang", "Ruby", "Elixir"], name: "John"},
  mary: %{age: 29, languages: ["Elixir", "F#"], name: "Mary"}
]</pre>
<p>There is more to learn about <a href="kernel#get_in/1"><code class="inline">get_in/1</code></a>, <a href="kernel#pop_in/1"><code class="inline">pop_in/1</code></a> and others, including the <a href="kernel#get_and_update_in/2"><code class="inline">get_and_update_in/2</code></a> that allows us to extract a value and update the data structure at once. There are also <code class="inline">get_in/3</code>, <a href="kernel#put_in/3"><code class="inline">put_in/3</code></a>, <a href="kernel#update_in/3"><code class="inline">update_in/3</code></a>, <a href="kernel#get_and_update_in/3"><code class="inline">get_and_update_in/3</code></a>, <a href="kernel#pop_in/2"><code class="inline">pop_in/2</code></a> which allow dynamic access into the data structure.</p>
<h2 id="summary" class="section-heading">  <span class="text">Summary</span> </h2> <p>There are two different data structures for working with key-value stores in Elixir. Alongside the <a href="access"><code class="inline">Access</code></a> module and pattern matching, they provide a rich set of tools for manipulating complex, potentially nested, data structures.</p>
<p>As we conclude this chapter, remember that you should:</p>
<ul>
<li><p>Use keyword lists for passing optional values to functions</p></li>
<li><p>Use maps for general key-value data structures</p></li>
<li><p>Use maps when working with data that has a predefined set of keys</p></li>
</ul>
<p>Now let's talk about modules and functions.</p> </div> <div class="bottom-actions" id="bottom-actions"> <div class="bottom-actions-item"> <a href="binaries-strings-and-charlists" class="bottom-actions-button" rel="prev"> <span class="subheader"> ← Previous Page </span> <span class="title"> Binaries, strings, and charlists </span> </a> </div> <div class="bottom-actions-item"> <a href="modules-and-functions" class="bottom-actions-button" rel="next"> <span class="subheader"> Next Page → </span> <span class="title"> Modules and functions </span> </a> </div> </div> <footer class="footer"> <p> <span class="line"> <button class="a-main footer-button display-quick-switch" title="Search HexDocs packages"> Search HexDocs </button> <a href="elixir.epub" title="ePub version"> Download ePub version </a> </span> </p> <p class="built-using"> Built using <a href="https://github.com/elixir-lang/ex_doc" title="ExDoc" target="_blank" rel="help noopener" translate="no">ExDoc</a> (v0.34.1) for the <a href="https://elixir-lang.org" title="Elixir" target="_blank" translate="no">Elixir programming language</a> </p> </footer><div class="_attribution">
  <p class="_attribution-p">
    &copy; 2012-2024 The Elixir Team<br>Licensed under the Apache License, Version 2.0.<br>
    <a href="https://hexdocs.pm/elixir/1.17.2/keywords-and-maps.html" class="_attribution-link">https://hexdocs.pm/elixir/1.17.2/keywords-and-maps.html</a>
  </p>
</div>
