<div id="top-content"> <h1> <a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L1" title="View Source" class="source" rel="help">Source</a> <span translate="no">Protocol</span>  </h1> <section id="moduledoc"> <p>Reference and functions for working with protocols.</p>
<p>A protocol specifies an API that should be defined by its implementations. A protocol is defined with <a href="kernel#defprotocol/2"><code class="inline">Kernel.defprotocol/2</code></a> and its implementations with <a href="kernel#defimpl/3"><code class="inline">Kernel.defimpl/3</code></a>.</p>
<h2 id="module-a-real-case" class="section-heading">  <span class="text">A real case</span> </h2> <p>In Elixir, we have two nouns for checking how many items there are in a data structure: <code class="inline">length</code> and <code class="inline">size</code>. <code class="inline">length</code> means the information must be computed. For example, <code class="inline">length(list)</code> needs to traverse the whole list to calculate its length. On the other hand, <code class="inline">tuple_size(tuple)</code> and <code class="inline">byte_size(binary)</code> do not depend on the tuple and binary size as the size information is precomputed in the data structure.</p>
<p>Although Elixir includes specific functions such as <code class="inline">tuple_size</code>, <code class="inline">binary_size</code> and <code class="inline">map_size</code>, sometimes we want to be able to retrieve the size of a data structure regardless of its type. In Elixir we can write polymorphic code, i.e. code that works with different shapes/types, by using protocols. A size protocol could be implemented as follows:</p>
<pre data-language="elixir">defprotocol Size do
  @doc "Calculates the size (and not the length!) of a data structure"
  def size(data)
end</pre>
<p>Now that the protocol can be implemented for every data structure the protocol may have a compliant implementation for:</p>
<pre data-language="elixir">defimpl Size, for: BitString do
  def size(binary), do: byte_size(binary)
end

defimpl Size, for: Map do
  def size(map), do: map_size(map)
end

defimpl Size, for: Tuple do
  def size(tuple), do: tuple_size(tuple)
end</pre>
<p>Finally, we can use the <code class="inline">Size</code> protocol to call the correct implementation:</p>
<pre data-language="elixir">Size.size({1, 2})
# =&gt; 2
Size.size(%{key: :value})
# =&gt; 1</pre>
<p>Note that we didn't implement it for lists as we don't have the <code class="inline">size</code> information on lists, rather its value needs to be computed with <code class="inline">length</code>.</p>
<p>The data structure you are implementing the protocol for must be the first argument to all functions defined in the protocol.</p>
<p>It is possible to implement protocols for all Elixir types:</p>
<ul>
<li>Structs (see the "Protocols and Structs" section below)</li>
<li><a href="tuple"><code class="inline">Tuple</code></a></li>
<li><a href="atom"><code class="inline">Atom</code></a></li>
<li><a href="list"><code class="inline">List</code></a></li>
<li><code class="inline">BitString</code></li>
<li><a href="integer"><code class="inline">Integer</code></a></li>
<li><a href="float"><code class="inline">Float</code></a></li>
<li><a href="function"><code class="inline">Function</code></a></li>
<li><code class="inline">PID</code></li>
<li><a href="map"><code class="inline">Map</code></a></li>
<li><a href="port"><code class="inline">Port</code></a></li>
<li><code class="inline">Reference</code></li>
<li>
<code class="inline">Any</code> (see the "Fallback to <code class="inline">Any</code>" section below)</li>
</ul>
<h2 id="module-protocols-and-structs" class="section-heading">  <span class="text">Protocols and Structs</span> </h2> <p>The real benefit of protocols comes when mixed with structs. For instance, Elixir ships with many data types implemented as structs, like <a href="mapset"><code class="inline">MapSet</code></a>. We can implement the <code class="inline">Size</code> protocol for those types as well:</p>
<pre data-language="elixir">defimpl Size, for: MapSet do
  def size(map_set), do: MapSet.size(map_set)
end</pre>
<p>When implementing a protocol for a struct, the <code class="inline">:for</code> option can be omitted if the <a href="kernel#defimpl/3"><code class="inline">defimpl/3</code></a> call is inside the module that defines the struct:</p>
<pre data-language="elixir">defmodule User do
  defstruct [:email, :name]

  defimpl Size do
    # two fields
    def size(%User{}), do: 2
  end
end</pre>
<p>If a protocol implementation is not found for a given type, invoking the protocol will raise unless it is configured to fall back to <code class="inline">Any</code>. Conveniences for building implementations on top of existing ones are also available, look at <a href="kernel#defstruct/1"><code class="inline">defstruct/1</code></a> for more information about deriving protocols.</p>
<h2 id="module-fallback-to-any" class="section-heading">  <span class="text">Fallback to <code class="inline">Any</code></span> </h2> <p>In some cases, it may be convenient to provide a default implementation for all types. This can be achieved by setting the <code class="inline">@fallback_to_any</code> attribute to <code class="inline">true</code> in the protocol definition:</p>
<pre data-language="elixir">defprotocol Size do
  @fallback_to_any true
  def size(data)
end</pre>
<p>The <code class="inline">Size</code> protocol can now be implemented for <code class="inline">Any</code>:</p>
<pre data-language="elixir">defimpl Size, for: Any do
  def size(_), do: 0
end</pre>
<p>Although the implementation above is arguably not a reasonable one. For example, it makes no sense to say a PID or an integer have a size of <code class="inline">0</code>. That's one of the reasons why <code class="inline">@fallback_to_any</code> is an opt-in behavior. For the majority of protocols, raising an error when a protocol is not implemented is the proper behavior.</p>
<h2 id="module-multiple-implementations" class="section-heading">  <span class="text">Multiple implementations</span> </h2> <p>Protocols can also be implemented for multiple types at once:</p>
<pre data-language="elixir">defprotocol Reversible do
  def reverse(term)
end

defimpl Reversible, for: [Map, List] do
  def reverse(term), do: Enum.reverse(term)
end</pre>
<p>Inside <a href="kernel#defimpl/3"><code class="inline">defimpl/3</code></a>, you can use <code class="inline">@protocol</code> to access the protocol being implemented and <code class="inline">@for</code> to access the module it is being defined for.</p>
<h2 id="module-types" class="section-heading">  <span class="text">Types</span> </h2> <p>Defining a protocol automatically defines a zero-arity type named <code class="inline">t</code>, which can be used as follows:</p>
<pre data-language="elixir">@spec print_size(Size.t()) :: :ok
def print_size(data) do
  result =
    case Size.size(data) do
      0 -&gt; "data has no items"
      1 -&gt; "data has one item"
      n -&gt; "data has #{n} items"
    end

  IO.puts(result)
end</pre>
<p>The <code class="inline">@spec</code> above expresses that all types allowed to implement the given protocol are valid argument types for the given function.</p>
<h2 id="module-reflection" class="section-heading">  <span class="text">Reflection</span> </h2> <p>Any protocol module contains three extra functions:</p>
<ul>
<li>
<p><code class="inline">__protocol__/1</code> - returns the protocol information. The function takes one of the following atoms:</p>
<ul>
<li><p><code class="inline">:consolidated?</code> - returns whether the protocol is consolidated</p></li>
<li><p><code class="inline">:functions</code> - returns a keyword list of protocol functions and their arities</p></li>
<li><p><code class="inline">:impls</code> - if consolidated, returns <code class="inline">{:consolidated, modules}</code> with the list of modules implementing the protocol, otherwise <code class="inline">:not_consolidated</code></p></li>
<li><p><code class="inline">:module</code> - the protocol module atom name</p></li>
</ul>
</li>
<li><p><code class="inline">impl_for/1</code> - returns the module that implements the protocol for the given argument, <code class="inline">nil</code> otherwise</p></li>
<li><p><code class="inline">impl_for!/1</code> - same as above but raises <a href="protocol.undefinederror"><code class="inline">Protocol.UndefinedError</code></a> if an implementation is not found</p></li>
</ul>
<p>For example, for the <a href="enumerable"><code class="inline">Enumerable</code></a> protocol we have:</p>
<pre data-language="elixir">iex&gt; Enumerable.__protocol__(:functions)
[count: 1, member?: 2, reduce: 3, slice: 1]

iex&gt; Enumerable.impl_for([])
Enumerable.List

iex&gt; Enumerable.impl_for(42)
nil</pre>
<p>In addition, every protocol implementation module contains the <code class="inline">__impl__/1</code> function. The function takes one of the following atoms:</p>
<ul>
<li><p><code class="inline">:for</code> - returns the module responsible for the data structure of the protocol implementation</p></li>
<li><p><code class="inline">:protocol</code> - returns the protocol module for which this implementation is provided</p></li>
</ul>
<p>For example, the module implementing the <a href="enumerable"><code class="inline">Enumerable</code></a> protocol for lists is <code class="inline">Enumerable.List</code>. Therefore, we can invoke <code class="inline">__impl__/1</code> on this module:</p>
<pre data-language="elixir">iex(1)&gt; Enumerable.List.__impl__(:for)
List

iex(2)&gt; Enumerable.List.__impl__(:protocol)
Enumerable</pre>
<h2 id="module-consolidation" class="section-heading">  <span class="text">Consolidation</span> </h2> <p>In order to speed up protocol dispatching, whenever all protocol implementations are known up-front, typically after all Elixir code in a project is compiled, Elixir provides a feature called <em>protocol consolidation</em>. Consolidation directly links protocols to their implementations in a way that invoking a function from a consolidated protocol is equivalent to invoking two remote functions.</p>
<p>Protocol consolidation is applied by default to all Mix projects during compilation. This may be an issue during test. For instance, if you want to implement a protocol during test, the implementation will have no effect, as the protocol has already been consolidated. One possible solution is to include compilation directories that are specific to your test environment in your mix.exs:</p>
<pre data-language="elixir">def project do
  ...
  elixirc_paths: elixirc_paths(Mix.env())
  ...
end

defp elixirc_paths(:test), do: ["lib", "test/support"]
defp elixirc_paths(_), do: ["lib"]</pre>
<p>And then you can define the implementations specific to the test environment inside <code class="inline">test/support/some_file.ex</code>.</p>
<p>Another approach is to disable protocol consolidation during tests in your mix.exs:</p>
<pre data-language="elixir">def project do
  ...
  consolidate_protocols: Mix.env() != :test
  ...
end</pre>
<p>If you are using <a href="https://hexdocs.pm/mix/Mix.html#install/2"><code class="inline">Mix.install/2</code></a>, you can do by passing the <code class="inline">consolidate_protocols</code> option:</p>
<pre data-language="elixir">Mix.install(
  deps,
  consolidate_protocols: false
)</pre>
<p>Although doing so is not recommended as it may affect the performance of your code.</p>
<p>Finally, note all protocols are compiled with <code class="inline">debug_info</code> set to <code class="inline">true</code>, regardless of the option set by the <code class="inline">elixirc</code> compiler. The debug info is used for consolidation and it is removed after consolidation unless globally set.</p> </section> </div> <section id="summary" class="details-list"> <h1 class="section-heading">  <span class="text">Summary</span> </h1> <h2> Functions </h2>
<dl class="summary-functions summary">  <div class="summary-row"> <dt class="summary-signature"> <a href="#assert_impl!/2" data-no-tooltip translate="no">assert_impl!(protocol, base)</a> </dt> <dd class="summary-synopsis"><p>Checks if the given module is loaded and is an implementation of the given protocol.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#assert_protocol!/1" data-no-tooltip translate="no">assert_protocol!(module)</a> </dt> <dd class="summary-synopsis"><p>Checks if the given module is loaded and is protocol.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#consolidate/2" data-no-tooltip translate="no">consolidate(protocol, types)</a> </dt> <dd class="summary-synopsis"><p>Receives a protocol and a list of implementations and consolidates the given protocol.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#consolidated?/1" data-no-tooltip translate="no">consolidated?(protocol)</a> </dt> <dd class="summary-synopsis"><p>Returns <code class="inline">true</code> if the protocol was consolidated.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#derive/3" data-no-tooltip translate="no">derive(protocol, module, options \\ [])</a> </dt> <dd class="summary-synopsis"><p>Derives the <code class="inline">protocol</code> for <code class="inline">module</code> with the given options.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#extract_impls/2" data-no-tooltip translate="no">extract_impls(protocol, paths)</a> </dt> <dd class="summary-synopsis"><p>Extracts all types implemented for the given protocol from the given paths.</p></dd> </div> <div class="summary-row"> <dt class="summary-signature"> <a href="#extract_protocols/1" data-no-tooltip translate="no">extract_protocols(paths)</a> </dt> <dd class="summary-synopsis"><p>Extracts all protocols from the given paths.</p></dd> </div> </dl> </section> <section id="functions" class="details-list"> <h1 class="section-heading">  <span class="text">Functions</span> </h1> <div class="functions-list"> <section class="detail"> <h3 class="detail-header" id="assert_impl!/2">assert_impl!(protocol, base)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L350" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec assert_impl!(module(), module()) :: :ok</pre> </div> <p>Checks if the given module is loaded and is an implementation of the given protocol.</p>
<p>Returns <code class="inline">:ok</code> if so, otherwise raises <a href="argumenterror"><code class="inline">ArgumentError</code></a>.</p> </section> </section> <section class="detail"> <h3 class="detail-header" id="assert_protocol!/1">assert_protocol!(module)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L321" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec assert_protocol!(module()) :: :ok</pre> </div> <p>Checks if the given module is loaded and is protocol.</p>
<p>Returns <code class="inline">:ok</code> if so, otherwise raises <a href="argumenterror"><code class="inline">ArgumentError</code></a>.</p> </section> </section> <section class="detail"> <h3 class="detail-header" id="consolidate/2">consolidate(protocol, types)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L565" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec consolidate(module(), [module()]) ::
  {:ok, binary()} | {:error, :not_a_protocol} | {:error, :no_beam_info}</pre> </div> <p>Receives a protocol and a list of implementations and consolidates the given protocol.</p>
<p>Consolidation happens by changing the protocol <code class="inline">impl_for</code> in the abstract format to have fast lookup rules. Usually the list of implementations to use during consolidation are retrieved with the help of <a href="#extract_impls/2"><code class="inline">extract_impls/2</code></a>.</p>
<p>It returns the updated version of the protocol bytecode. If the first element of the tuple is <code class="inline">:ok</code>, it means the protocol was consolidated.</p>
<p>A given bytecode or protocol implementation can be checked to be consolidated or not by analyzing the protocol attribute:</p>
<pre data-language="elixir">Protocol.consolidated?(Enumerable)</pre>
<p>This function does not load the protocol at any point nor loads the new bytecode for the compiled module. However, each implementation must be available and it will be loaded.</p> </section> </section> <section class="detail"> <h3 class="detail-header" id="consolidated?/1">consolidated?(protocol)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L533" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec consolidated?(module()) :: boolean()</pre> </div> <p>Returns <code class="inline">true</code> if the protocol was consolidated.</p> </section> </section> <section class="detail">  <h3 class="detail-header" id="derive/3">derive(protocol, module, options \\ [])<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L431" class="source">Source</a>
</h3> <section class="docstring"> <p>Derives the <code class="inline">protocol</code> for <code class="inline">module</code> with the given options.</p>
<p>If your implementation passes options or if you are generating custom code based on the struct, you will also need to implement a macro defined as <code class="inline">__deriving__(module, struct, options)</code> to get the options that were passed.</p>
<h4 id="derive/3-examples" class="section-heading">  <span class="text">Examples</span> </h4> <pre data-language="elixir">defprotocol Derivable do
  def ok(arg)
end

defimpl Derivable, for: Any do
  defmacro __deriving__(module, struct, options) do
    quote do
      defimpl Derivable, for: unquote(module) do
        def ok(arg) do
          {:ok, arg, unquote(Macro.escape(struct)), unquote(options)}
        end
      end
    end
  end

  def ok(arg) do
    {:ok, arg}
  end
end

defmodule ImplStruct do
  @derive [Derivable]
  defstruct a: 0, b: 0
end

Derivable.ok(%ImplStruct{})
#=&gt; {:ok, %ImplStruct{a: 0, b: 0}, %ImplStruct{a: 0, b: 0}, []}</pre>
<p>Explicit derivations can now be called via <code class="inline">__deriving__/3</code>:</p>
<pre data-language="elixir"># Explicitly derived via `__deriving__/3`
Derivable.ok(%ImplStruct{a: 1, b: 1})
#=&gt; {:ok, %ImplStruct{a: 1, b: 1}, %ImplStruct{a: 0, b: 0}, []}

# Explicitly derived by API via `__deriving__/3`
require Protocol
Protocol.derive(Derivable, ImplStruct, :oops)
Derivable.ok(%ImplStruct{a: 1, b: 1})
#=&gt; {:ok, %ImplStruct{a: 1, b: 1}, %ImplStruct{a: 0, b: 0}, :oops}</pre> </section> </section> <section class="detail"> <h3 class="detail-header" id="extract_impls/2">extract_impls(protocol, paths)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L487" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec extract_impls(module(), [charlist() | String.t()]) :: [atom()]</pre> </div> <p>Extracts all types implemented for the given protocol from the given paths.</p>
<p>The paths can be either a charlist or a string. Internally they are worked on as charlists, so passing them as lists avoid extra conversion.</p>
<p>Does not load any of the implementations.</p>
<h4 id="extract_impls/2-examples" class="section-heading">  <span class="text">Examples</span> </h4> <pre data-language="elixir"># Get Elixir's ebin directory path and retrieve all protocols
iex&gt; path = Application.app_dir(:elixir, "ebin")
iex&gt; mods = Protocol.extract_impls(Enumerable, [path])
iex&gt; List in mods
true</pre> </section> </section> <section class="detail"> <h3 class="detail-header" id="extract_protocols/1">extract_protocols(paths)<a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/lib/protocol.ex#L458" class="source">Source</a>
</h3> <section class="docstring"> <div class="specs"> <pre translate="no" data-language="elixir">@spec extract_protocols([charlist() | String.t()]) :: [atom()]</pre> </div> <p>Extracts all protocols from the given paths.</p>
<p>The paths can be either a charlist or a string. Internally they are worked on as charlists, so passing them as lists avoid extra conversion.</p>
<p>Does not load any of the protocols.</p>
<h4 id="extract_protocols/1-examples" class="section-heading">  <span class="text">Examples</span> </h4> <pre data-language="elixir"># Get Elixir's ebin directory path and retrieve all protocols
iex&gt; path = Application.app_dir(:elixir, "ebin")
iex&gt; mods = Protocol.extract_protocols([path])
iex&gt; Enumerable in mods
true</pre> </section> </section> </div> </section> <footer class="footer"> <p> <span class="line"> <button class="a-main footer-button display-quick-switch" title="Search HexDocs packages"> Search HexDocs </button> <a href="elixir.epub" title="ePub version"> Download ePub version </a> </span> </p> <p class="built-using"> Built using <a href="https://github.com/elixir-lang/ex_doc" title="ExDoc" target="_blank" rel="help noopener" translate="no">ExDoc</a> (v0.34.1) for the <a href="https://elixir-lang.org" title="Elixir" target="_blank" translate="no">Elixir programming language</a> </p> </footer><div class="_attribution">
  <p class="_attribution-p">
    &copy; 2012-2024 The Elixir Team<br>Licensed under the Apache License, Version 2.0.<br>
    <a href="https://hexdocs.pm/elixir/1.17.2/Protocol.html" class="_attribution-link">https://hexdocs.pm/elixir/1.17.2/Protocol.html</a>
  </p>
</div>
