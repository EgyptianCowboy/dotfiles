<div id="top-content"> <h1> <a href="https://github.com/elixir-lang/elixir/blob/v1.17.2/lib/elixir/pages/anti-patterns/macro-anti-patterns.md#L1" title="View Source" class="source" rel="help">Source</a> <span>Meta-programming anti-patterns</span> </h1> <p>This document outlines potential anti-patterns related to meta-programming.</p>
<h2 id="large-code-generation" class="section-heading">  <span class="text">Large code generation</span> </h2> <h4>Problem</h4>
<p>This anti-pattern is related to macros that generate too much code. When a macro generates a large amount of code, it impacts how the compiler and/or the runtime work. The reason for this is that Elixir may have to expand, compile, and execute the code multiple times, which will make compilation slower and the resulting compiled artifacts larger.</p>
<h4>Example</h4>
<p>Imagine you are defining a router for a web application, where you could have macros like <code class="inline">get/2</code>. On every invocation of the macro (which could be hundreds), the code inside <code class="inline">get/2</code> will be expanded and compiled, which can generate a large volume of code overall.</p>
<pre data-language="elixir">defmodule Routes do
  defmacro get(route, handler) do
    quote do
      route = unquote(route)
      handler = unquote(handler)

      if not is_binary(route) do
        raise ArgumentError, "route must be a binary"
      end

      if not is_atom(handler) do
        raise ArgumentError, "handler must be a module"
      end

      @store_route_for_compilation {route, handler}
    end
  end
end</pre>
<h4>Refactoring</h4>
<p>To remove this anti-pattern, the developer should simplify the macro, delegating part of its work to other functions. As shown below, by encapsulating the code inside <code class="inline">quote/1</code> inside the function <code class="inline">__define__/3</code> instead, we reduce the code that is expanded and compiled on every invocation of the macro, and instead we dispatch to a function to do the bulk of the work.</p>
<pre data-language="elixir">defmodule Routes do
  defmacro get(route, handler) do
    quote do
      Routes.__define__(__MODULE__, unquote(route), unquote(handler))
    end
  end

  def __define__(module, route, handler) do
    if not is_binary(route) do
      raise ArgumentError, "route must be a binary"
    end

    if not is_atom(handler) do
      raise ArgumentError, "handler must be a module"
    end

    Module.put_attribute(module, :store_route_for_compilation, {route, handler})
  end
end</pre>
<h2 id="unnecessary-macros" class="section-heading">  <span class="text">Unnecessary macros</span> </h2> <h4>Problem</h4>
<p><em>Macros</em> are powerful meta-programming mechanisms that can be used in Elixir to extend the language. While using macros is not an anti-pattern in itself, this meta-programming mechanism should only be used when absolutely necessary. Whenever a macro is used, but it would have been possible to solve the same problem using functions or other existing Elixir structures, the code becomes unnecessarily more complex and less readable. Because macros are more difficult to implement and reason about, their indiscriminate use can compromise the evolution of a system, reducing its maintainability.</p>
<h4>Example</h4>
<p>The <code class="inline">MyMath</code> module implements the <code class="inline">sum/2</code> macro to perform the sum of two numbers received as parameters. While this code has no syntax errors and can be executed correctly to get the desired result, it is unnecessarily more complex. By implementing this functionality as a macro rather than a conventional function, the code became less clear:</p>
<pre data-language="elixir">defmodule MyMath do
  defmacro sum(v1, v2) do
    quote do
      unquote(v1) + unquote(v2)
    end
  end
end</pre>
<pre data-language="elixir">iex&gt; require MyMath
MyMath
iex&gt; MyMath.sum(3, 5)
8
iex&gt; MyMath.sum(3 + 1, 5 + 6)
15</pre>
<h4>Refactoring</h4>
<p>To remove this anti-pattern, the developer must replace the unnecessary macro with structures that are simpler to write and understand, such as named functions. The code shown below is the result of the refactoring of the previous example. Basically, the <code class="inline">sum/2</code> macro has been transformed into a conventional named function. Note that the <a href="kernel.specialforms#require/2"><code class="inline">require/2</code></a> call is no longer needed:</p>
<pre data-language="elixir">defmodule MyMath do
  def sum(v1, v2) do # &lt;= The macro became a named function
    v1 + v2
  end
end</pre>
<pre data-language="elixir">iex&gt; MyMath.sum(3, 5)
8
iex&gt; MyMath.sum(3+1, 5+6)
15</pre>
<h2 id="use-instead-of-import" class="section-heading">  <span class="text"><code class="inline">use</code> instead of <code class="inline">import</code></span> </h2> <h4>Problem</h4>
<p>Elixir has mechanisms such as <code class="inline">import/1</code>, <code class="inline">alias/1</code>, and <a href="kernel#use/1"><code class="inline">use/1</code></a> to establish dependencies between modules. Code implemented with these mechanisms does not characterize a smell by itself. However, while the <code class="inline">import/1</code> and <code class="inline">alias/1</code> directives have lexical scope and only facilitate a module calling functions of another, the <a href="kernel#use/1"><code class="inline">use/1</code></a> directive has a <em>broader scope</em>, which can be problematic.</p>
<p>The <a href="kernel#use/1"><code class="inline">use/1</code></a> directive allows a module to inject any type of code into another, including propagating dependencies. In this way, using the <a href="kernel#use/1"><code class="inline">use/1</code></a> directive makes code harder to read, because to understand exactly what will happen when it references a module, it is necessary to have knowledge of the internal details of the referenced module.</p>
<h4>Example</h4>
<p>The code shown below is an example of this anti-pattern. It defines three modules -- <code class="inline">ModuleA</code>, <code class="inline">Library</code>, and <code class="inline">ClientApp</code>. <code class="inline">ClientApp</code> is reusing code from the <code class="inline">Library</code> via the <a href="kernel#use/1"><code class="inline">use/1</code></a> directive, but is unaware of its internal details. This makes it harder for the author of <code class="inline">ClientApp</code> to visualize which modules and functionality are now available within its module. To make matters worse, <code class="inline">Library</code> also imports <code class="inline">ModuleA</code>, which defines a <code class="inline">foo/0</code> function that conflicts with a local function defined in <code class="inline">ClientApp</code>:</p>
<pre data-language="elixir">defmodule ModuleA do
  def foo do
    "From Module A"
  end
end</pre>
<pre data-language="elixir">defmodule Library do
  defmacro __using__(_opts) do
    quote do
      import Library
      import ModuleA  # &lt;= propagating dependencies!
    end
  end

  def from_lib do
    "From Library"
  end
end</pre>
<pre data-language="elixir">defmodule ClientApp do
  use Library

  def foo do
    "Local function from client app"
  end

  def from_client_app do
    from_lib() &lt;&gt; " - " &lt;&gt; foo()
  end
end</pre>
<p>When we try to compile <code class="inline">ClientApp</code>, Elixir detects the conflict and throws the following error:</p>
<pre data-language="elixir">error: imported ModuleA.foo/0 conflicts with local function
  └ client_app.ex:4:</pre>
<h4>Refactoring</h4>
<p>To remove this anti-pattern, we recommend library authors avoid providing <code class="inline">__using__/1</code> callbacks whenever it can be replaced by <code class="inline">alias/1</code> or <code class="inline">import/1</code> directives. In the following code, we assume <code class="inline">use Library</code> is no longer available and <code class="inline">ClientApp</code> was refactored in this way, and with that, the code is clearer and the conflict as previously shown no longer exists:</p>
<pre data-language="elixir">defmodule ClientApp do
  import Library

  def foo do
    "Local function from client app"
  end

  def from_client_app do
    from_lib() &lt;&gt; " - " &lt;&gt; foo()
  end
end</pre>
<pre data-language="elixir">iex&gt; ClientApp.from_client_app()
"From Library - Local function from client app"</pre>
<h4>Additional remarks</h4>
<p>In situations where you need to do more than importing and aliasing modules, providing <code class="inline">use MyModule</code> may be necessary, as it provides a common extension point within the Elixir ecosystem.</p>
<p>Therefore, to provide guidance and clarity, we recommend library authors to include an admonition block in their <code class="inline">@moduledoc</code> that explains how <code class="inline">use MyModule</code> impacts the developer's code. As an example, the <a href="genserver"><code class="inline">GenServer</code></a> documentation outlines:</p>
<blockquote>
<h4 class="info"><code class="inline">use GenServer</code></h4>
<p>When you <code class="inline">use GenServer</code>, the <a href="genserver"><code class="inline">GenServer</code></a> module will set <code class="inline">@behaviour GenServer</code> and define a <code class="inline">child_spec/1</code> function, so your module can be used as a child in a supervision tree.</p>
</blockquote>
<p>Think of this summary as a <a href="https://en.wikipedia.org/wiki/Nutrition_facts_label">"Nutrition facts label"</a> for code generation. Make sure to only list changes made to the public API of the module. For example, if <code class="inline">use Library</code> sets an internal attribute called <code class="inline">@_some_module_info</code> and this attribute is never meant to be public, avoid documenting it in the nutrition facts.</p>
<p>For convenience, the markup notation to generate the admonition block above is this:</p>
<pre data-language="elixir">&gt; #### `use GenServer` {: .info}
&gt;
&gt; When you `use GenServer`, the `GenServer` module will
&gt; set `@behaviour GenServer` and define a `child_spec/1`
&gt; function, so your module can be used as a child
&gt; in a supervision tree.</pre> </div> <div class="bottom-actions" id="bottom-actions"> <div class="bottom-actions-item"> <a href="process-anti-patterns" class="bottom-actions-button" rel="prev"> <span class="subheader"> ← Previous Page </span> <span class="title"> Process-related anti-patterns </span> </a> </div> <div class="bottom-actions-item"> <a href="quote-and-unquote" class="bottom-actions-button" rel="next"> <span class="subheader"> Next Page → </span> <span class="title"> Quote and unquote </span> </a> </div> </div> <footer class="footer"> <p> <span class="line"> <button class="a-main footer-button display-quick-switch" title="Search HexDocs packages"> Search HexDocs </button> <a href="elixir.epub" title="ePub version"> Download ePub version </a> </span> </p> <p class="built-using"> Built using <a href="https://github.com/elixir-lang/ex_doc" title="ExDoc" target="_blank" rel="help noopener" translate="no">ExDoc</a> (v0.34.1) for the <a href="https://elixir-lang.org" title="Elixir" target="_blank" translate="no">Elixir programming language</a> </p> </footer><div class="_attribution">
  <p class="_attribution-p">
    &copy; 2012-2024 The Elixir Team<br>Licensed under the Apache License, Version 2.0.<br>
    <a href="https://hexdocs.pm/elixir/1.17.2/macro-anti-patterns.html" class="_attribution-link">https://hexdocs.pm/elixir/1.17.2/macro-anti-patterns.html</a>
  </p>
</div>
