;;; -*- lexical-binding: t -*-
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("90185f1d8362727f2aeac7a3d67d3aec789f55c10bb47dada4eefb2e14aa5d01"
     "c20358be3b98db42aeea2b437d8e683177a96015b692e90e6b7a78642624b939" default))
 '(safe-local-variable-values
   '((eval
      (progn
        (defun mcx-root nil
          "Get project root directory"
          (file-name-as-directory
           (expand-file-name (project-root (project-current)))))
        (defun mcx-command (cmd)
          "Resolve command aliases to their actual command"
          (let ((command (cdr (assoc cmd compile-commands))))
            (if (stringp command)
                (if (assoc command compile-commands)
                    (cdr (assoc command compile-commands))
                  command)
              command)))
        (local-set-key [f5] 'compile)
        (defun mcx-run nil
          "Run the project's executable" (interactive)
          (let ((default-directory (mcx-root))) (compile (mcx-command "run"))))
        (local-set-key [f6] 'mcx-run)
        (defun mcx-compile nil
          "Select and run a compilation command from compile-commands."
          (interactive)
          (let*
              ((default-directory (mcx-root))
               (commands (mapcar 'car compile-commands))
               (command (completing-read "Compile command: " commands nil t))
               (compile-command (mcx-command command)))
            (compile compile-command)))
        (local-set-key [(shift f5)] 'mcx-compile)
        (defun mcx-build-and-run nil
          "Build and then run if build succeeds" (interactive)
          (let
              ((default-directory (mcx-root))
               (compile-command (mcx-command "build")))
            (compile compile-command)
            (add-hook 'compilation-finish-functions
                      (lambda (buffer status)
                        (when
                            (and (string-match "finished" status)
                                 (string= (buffer-name buffer) "*compilation*"))
                          (mcx-run)
                          (remove-hook 'compilation-finish-functions
                                       'mcx-build-and-run))))))
        (local-set-key [(shift f6)] 'mcx-build-and-run)))
     (eval progn
           (defun mcx-root nil
             "Get project root directory"
             (file-name-as-directory
              (expand-file-name (project-root (project-current)))))
           (defun mcx-command (cmd)
             "Resolve command aliases to their actual command"
             (let ((command (cdr (assoc cmd compile-commands))))
               (if (stringp command)
                   (if (assoc command compile-commands)
                       (cdr (assoc command compile-commands))
                     command)
                 command)))
           (local-set-key [f5] 'compile)
           (defun mcx-run nil
             "Run the project's executable" (interactive)
             (let ((default-directory (mcx-root)))
               (compile (mcx-command "run"))))
           (local-set-key [f6] 'mcx-run)
           (defun mcx-compile nil
             "Select and run a compilation command from compile-commands."
             (interactive)
             (let*
                 ((default-directory (mcx-root))
                  (commands (mapcar 'car compile-commands))
                  (command (completing-read "Compile command: " commands nil t))
                  (compile-command (mcx-command command)))
               (compile compile-command)))
           (local-set-key [(shift f5)] 'mcx-compile)
           (defun mcx-build-and-run nil
             "Build and then run if build succeeds" (interactive)
             (let
                 ((default-directory (mcx-root))
                  (compile-command (mcx-command "build")))
               (compile compile-command)
               (add-hook 'compilation-finish-functions
                         (lambda (buffer status)
                           (when
                               (and (string-match "finished" status)
                                    (string= (buffer-name buffer)
                                             "*compilation*"))
                             (mcx-run)
                             (remove-hook 'compilation-finish-functions
                                          'mcx-build-and-run))))))
           (local-set-key [(shift f6)] 'mcx-build-and-run))
     (eval progn
           (defun mcx-root nil
             "Get project root directory"
             (file-name-as-directory
              (expand-file-name (project-root (project-current)))))
           (defun mcx-command (cmd)
             "Resolve command aliases to their actual command"
             (let ((command (cdr (assoc cmd compile-commands))))
               (if (stringp command)
                   (if (assoc command compile-commands)
                       (cdr (assoc command compile-commands))
                     command)
                 command)))
           (local-set-key [f5] 'compile)
           (defun mcx-run nil
             "Run the project's executable" (interactive)
             (let ((default-directory (mcx-root)))
               (compile (mcx-command "run"))))
           (local-set-key [f6] 'mcx-run)
           (defun mcx-compile nil
             "Select and run a compilation command from compile-commands."
             (interactive)
             (let*
                 ((default-directory (mcx-root))
                  (commands (mapcar 'car compile-commands))
                  (command (completing-read "Compile command: " commands nil t))
                  (compile-command (mcx-command command)))
               (compile compile-command)))
           (local-set-key [(shift f5)] 'mcx-compile)
           (defun mcx-build-and-run nil
             "Build and then run if build succeeds" (interactive)
             (let
                 ((default-directory (mcx-root))
                  (compile-command (my-resolve-command "build")))
               (compile compile-command)
               (add-hook 'compilation-finish-functions
                         (lambda (buffer status)
                           (when
                               (and (string-match "finished" status)
                                    (string= (buffer-name buffer)
                                             "*compilation*"))
                             (mcx-run)
                             (remove-hook 'compilation-finish-functions
                                          'mcx-build-and-run))))))
           (local-set-key [(shift f6)] 'mcx-build-and-run))
     (eval progn (local-set-key [f5] 'compile)
           (defun mcx-run nil
             "Run the project's executable" (interactive)
             (compile
              "cd build/debug/apps/MCClient && ./MCClient -c ./assets/alea.config.toml -u ./assets/alea.users.toml"))
           (local-set-key [f6] 'mcx-run)
           (defun mcx-compile nil
             "Select and run a compilation command from compile-commands."
             (interactive)
             (let*
                 ((commands (mapcar 'car compile-commands))
                  (command (completing-read "Compile command: " commands nil t))
                  (compile-command (cdr (assoc command compile-commands))))
               (compile compile-command)))
           (local-set-key [(shift f5)] 'mcx-compile)
           (defun mcx-build-and-run nil
             "Build and then run if build succeeds" (interactive)
             (let ((compile-command (cdr (assoc "build" compile-commands))))
               (compile compile-command)
               (add-hook 'compilation-finish-functions
                         (lambda (buffer status)
                           (when
                               (and (string-match "finished" status)
                                    (string= (buffer-name buffer)
                                             "*compilation*"))
                             (my-project-run)
                             (remove-hook 'compilation-finish-functions
                                          'my-project-build-and-run))))))
           (local-set-key [(shift f6)] 'mcx-build-and-run))
     (dape-configs
      (lldb-dap command "lldb-dap" :type "lldb-dap" modes
                (c-mode c-ts-mode c++-mode c++-ts-mode) command-cwd
                (lambda nil
                  (locate-dominating-file default-directory "CMakeLists.txt"))
                :cwd "./build/debug/apps/MCClient" :program
                "./build/debug/apps/MCClient/MCClient" :args
                ["--config" "./assets/alea.config.toml" "--users"
                 "./assets/alea.users.toml"])))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
