;;; post-init.el --- Pre init -*- no-byte-compile: t; lexical-binding: t; -*-

(org-babel-load-file (expand-file-name "config.org" sil-emacs-user-directory))

(provide 'post-init)
;;; post-init.el ends here
