;;; Automatically generated by ‘recentf’ on Sun Sep 15 14:33:37 2024.

(setq recentf-list
      '(
        "~/.dotfiles/emacs/.emacs.d/config.org"
        "~/.dotfiles/emacs/.emacs.d/init.el"
        "~/.emacs.d/init.el"
        ))

(setq recentf-filter-changer-current 'nil)


;; Local Variables:
;; coding: utf-8-emacs
;; End:
