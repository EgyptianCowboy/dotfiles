(use-package request
  :ensure t)

(put 'compile-commands 'safe-local-variable #'listp)
(put 'compile-command 'safe-local-variable #'stringp)
(put 'dape-configs 'safe-local-variable #'listp)
(put 'eval 'safe-local-variable 'listp)

(use-package general
  :demand t)
(elpaca-wait)
(use-package key-chord
  :init
  (key-chord-mode 1)
  :config
  (setq key-chord-one-key-delay 0.2)           ; e.g. "jj", default 0.2
  (setq key-chord-two-keys-delay 0.1)          ; e.g. "jk", default 0.1
  (setq key-chord-safety-interval-backward 0.5) ; default 0.1 is too close to key delays
  (setq key-chord-safety-interval-forward 0) ; default 0.35 causes laggy experience
  (key-chord-define-global "jj" 'avy-goto-word-1)
  (key-chord-define-global "jl" 'avy-goto-line)
  (key-chord-define-global "gk" 'avy-goto-char)
  (key-chord-define-global "XX" 'execute-extended-command)
  (key-chord-define-global "yy" 'consult-yank-pop)
  (key-chord-define-global "g;" 'ace-window)
  (key-chord-define-global ";g" 'ace-window))

(use-package emacs
  :ensure nil
  :config
  (which-key-mode t)
  :preface
  (defun config-visit ()
    (interactive)
    (find-file (concat emacs-home "init.el")))

  (defun split-and-follow-h ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))

  (defun split-and-follow-v ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))

  (defun my-kill-buffer (arg)
    (interactive "P")
    (if arg
	    (call-interactively 'kill-buffer)
      (kill-buffer)))

  :bind (:map global-map
              ("<insert>" . nil)
              ("M-SPC" . cycle-spacing)
              ("C-x k" . my-kill-buffer)
              ("C-c o" . occur)
              ("C-z" . nil)
              ("C-c C-z" . nil)
              ("C-x C-z" . nil)
              ("C-x C-c" . nil)
              ("C-h h" . nil)
              ("C-c e" . config-visit)
              ([remap split-window-below] . split-and-follow-h)
              ([remap split-window-right] . split-and-follow-v)
              ))

(customize-set-variable 'yank-pop-change-selection t)
(customize-set-variable 'kill-whole-line t)
(customize-set-variable 'track-eol t) ; Keep cursor at end of lines.
(customize-set-variable 'line-move-visual nil) ; To be required by track-eol
(customize-set-variable 'use-short-answers t)
(customize-set-variable 'use-file-dialog nil)
(customize-set-variable 'use-dialog-box nil)                ; Avoid GUI dialogs
(customize-set-variable 'x-gtk-use-system-tooltips nil)     ; Do not use GTK tooltips
(customize-set-variable 'epa-pinentry-mode 'loopback)

(setopt inhibit-startup-message t)
(setopt tab-always-indent 'complete)
(setopt tab-first-completion 'word)

;; MORE INTERACTIONS
;; http://ergoemacs.org/emacs/emacs_save_restore_opened_files.html
(use-package delsel
  :ensure nil ; no need to install it as it is built-in
  :hook (after-init . delete-selection-mode))
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(defun prot/keyboard-quit-dwim ()
  "Do-What-I-Mean behaviour for a general `keyboard-quit'.

The generic `keyboard-quit' does not do the expected thing when
the minibuffer is open.  Whereas we want it to close the
minibuffer, even without explicitly focusing it.

The DWIM behaviour of this command is as follows:

- When the region is active, disable it.
- When a minibuffer is open, but not focused, close the minibuffer.
- When the Completions buffer is selected, close it.
- In every other case use the regular `keyboard-quit'."
  (interactive)
  (cond
   ((region-active-p)
    (keyboard-quit))
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))
   (t
    (keyboard-quit))))

(define-key global-map (kbd "C-g") #'prot/keyboard-quit-dwim)
(define-key global-map (kbd "C-x C-m") #'execute-extended-command)

(use-package comment-dwim-2
  :bind (:map global-map
              ([remap comment-dwim] . comment-dwim-2)))

(use-package recentf
  :ensure nil
  :custom
  (recentf-save-file (expand-file-name "recentf-save.el" sil-emacs-var-dir))
  (recentf-max-saved-items 300)
  (recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  (recentf-auto-cleanup 'mode)
  (recentf-max-menu-items 50)
  (recentf-additional-variables '(kill-ring search-ring regexp-search-ring))
  :config
  (add-to-list 'recentf-exclude sil-emacs-var-dir)
  (add-to-list 'recentf-exclude sil-emacs-etc-dir)
  (run-with-idle-timer 30 t 'recentf-save-list)
  :init
  (recentf-mode))

(use-package helpful ; h/t systemcrafters.net
  ;; https://github.com/Wilfred/helpful
  :ensure t
  :bind (("C-h f" . #'helpful-callable)
         ("C-h F" . #'helpful-function) ; exclude macros
         ("C-h v" . #'helpful-variable)
         ("C-h k" . #'helpful-key)
         ("C-h x" . #'helpful-command)
         ;; Lookup the current symbol at point. C-c C-d is
         ;; a common keybinding for this in lisp modes.
         ("C-c C-d" . #'helpful-at-point)))

(use-package envrc
  :ensure t
  :init (envrc-global-mode 1))

(use-package emacs
  :ensure nil
  :config
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
		          (replace-regexp-in-string
		           "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
		           crm-separator)
		          (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal

  (keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell
  )

(use-package hotfuzz
  :custom
  (read-file-name-completion-ignore-case t)
  (read-buffer-completion-ignore-case t)
  (completion-ignore-case t)
  (hotfuzz-max-highlighted-completions most-positive-fixnum)
  :config
  (require 'hotfuzz-module)
  (add-to-list 'completion-styles 'hotfuzz))

(use-package vertico
  :ensure (vertico :host github
                   :repo "minad/vertico"
                   :files (:defaults "extensions/*")
                   :main "vertico.el"
		           :includes (vertico-directory))
  :bind
  (:map vertico-map
    	("<tab>" . vertico-insert)
    	("<escape>" . minibuffer-keyboard-quit)
	    ("C-k" . vertico-exit)
    	("C-n" . vertico-next)
    	("C-p" . vertico-previous)
	    ("M-RET" . vertico-exit-input)

	    ("RET" . vertico-directory-enter)
    	("<backspace>" . vertico-directory-delete-char)
    	("C-w" . vertico-directory-delete-word)
    	("C-<backspace>" . vertico-directory-delete-word)
    	("DEL" . vertico-directory-delete-word)
    	("M-d" . vertico-directory-delete-char)
    	("RET" . vertico-directory-enter)

	    ("?".  minibuffer-completion-help)
	    ("M-TAB" . minibuffer-complete))
  (:map minibuffer-local-map
    	("<backtab>" . minibuffer-complete)
    	("M-d" . backward-kill-word))
  :custom
  (vertico-scroll-margin 0)
  (vertico-count 10)
  :init
  (vertico-mode t))

;; (use-package vertico-posframe
;;   :after vertico
;;   :config
;;   (vertico-posframe-mode))

(use-package marginalia
  :init
  (marginalia-mode t))

(use-package consult
  :after vertico
  :general
  ([remap isearch-forward] 'consult-line
   [remap switch-to-buffer] 'consult-buffer
   [remap Info-search] 'consult-info
   [remap isearch-backward] 'consult-recent-file
   [remap yank-pop] 'consult-yank-pop
   "C-c i" 'consult-imenu)
  :config
  (setq consult--tofu-char #x100000)
  (setq consult--tofu-range #x00fffe)
  (setq xref-show-xrefs-function #'consult-xref)
  (setq xref-show-definitions-function #'consult-xref))

(use-package corfu
  :general
  (:keymaps 'prog-mode-map
            "C-c C-o" 'completion-at-point)
  (:keymaps 'corfu-map
            "<tab>" 'corfu-complete
            "C-n" 'corfu-next
            "C-p" 'corfu-previous)
  :custom
  (corfu-auto t)
  (corfu-preview-current nil)
  (corfu-min-width 20)
  (corfu-auto-prefix 2)          ;; Minimum length of prefix for auto completion.
  (corfu-popupinfo-mode t)       ;; Enable popup information
  (corfu-popupinfo-delay '(1.25 . 0.5))
  (text-mode-ispell-word-completion nil)
  :config
  (with-eval-after-load 'savehist
    (corfu-history-mode 1)
    (add-to-list 'savehist-additional-variables 'corfu-history))
  :init
  (global-corfu-mode t))

(use-package cape)

(use-package prescient
  :init
  (add-to-list 'completion-styles 'prescient))

(use-package corfu-prescient
  :after corfu
  :init
  (corfu-prescient-mode)
  :custom
  (corfu-prescient-completion-styles '(prescient hotfuzz basic)))

(use-package vertico-prescient
  :after vertico
  :init
  (vertico-prescient-mode)
  :custom
  (vertico-prescient-completion-styles '(prescient hotfuzz basic)))

;; Icons
(use-package nerd-icons
  :custom
  (nerd-icons-font-family "Symbols Nerd Font Mono"))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package nerd-icons-corfu
  :init
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))

;; (use-package completion-preview
;;   :ensure nil
;;   :hook ((prog-mode . completion-preview-mode)
;;          (text-mode . completion-preview-mode)
;;          (comint-mode . completion-preview-mode))
;;   :general
;;   (:keymaps 'completion-preview-active-mode-map
;;             "M-n" 'completion-preview-next-candidate
;;             "M-p" 'completion-preview-prev-candidate
;;             "M-i" 'completion-preview-insert)
;;   :config
;;   ;; Org mode has a custom `self-insert-command'
;;   (push 'org-self-insert-command completion-preview-commands)
;;   ;; Paredit has a custom `delete-backward-char' command
;;   (push 'paredit-backward-delete completion-preview-commands)
;;   :custom
;;   (completion-preview-minimum-symbol-length 2))

(use-package embark)

(use-package embark-consult)

(setopt x-underline-at-descent-line nil)           ; Prettier underlines
(setopt inhibit-compacting-font-caches t)
(setopt underline-minimum-offset 0)
(setopt line-spacing 0)
(setopt text-scale-remap-header-line t)

(use-package spacious-padding
  :ensure (spacious-padding :host github
                            :repo "protesilaos/spacious-padding")
  ;; :init
  ;; (spacious-padding-mode t)
  :custom
  (spacious-padding-widths
   '( :internal-border-width 10
      :header-line-width 4
      :mode-line-width 2
      :tab-width 2
      :right-divider-width 30
      :scroll-bar-width 8))

  (spacious-padding-subtle-mode-line
   '(:mode-line-active default
                       :mode-line-inactive shadow)))

;; (use-package prot-modeline
;;   :ensure nil
;;   :config
;;   (setq mode-line-compact nil) ; Emacs 28
;;   (setq mode-line-right-align-edge 'right-margin) ; Emacs 30
;;   (setq-default mode-line-format
;;                 '("%e"
;;                   prot-modeline-buffer-status
;;                   prot-modeline-window-dedicated-status
;;                   prot-modeline-input-method
;;                   "  "
;;                   prot-modeline-buffer-identification
;;                   "  "
;;                   prot-modeline-major-mode
;;                   prot-modeline-process
;;                   "  "
;;                   prot-modeline-vc-branch
;;                   "  "
;;                   prot-modeline-eglot
;;                   "  "
;;                   prot-modeline-flymake
;;                   "  "
;;                   mode-line-format-right-align ; Emacs 30
;;                   "  "
;;                   prot-modeline-misc-info))
;;
;;   (with-eval-after-load 'spacious-padding
;;     (defun prot/modeline-spacious-indicators ()
;;       "Set box attribute to `'prot-modeline-indicator-button' if spacious-padding is enabled."
;;       (if (bound-and-true-p spacious-padding-mode)
;;           (set-face-attribute 'prot-modeline-indicator-button nil :box t)
;;         (set-face-attribute 'prot-modeline-indicator-button nil :box 'unspecified)))
;;
;;     ;; Run it at startup and then afterwards whenever
;;     ;; `spacious-padding-mode' is toggled on/off.
;;     (prot/modeline-spacious-indicators)
;;
;;     (add-hook 'spacious-padding-mode-hook #'prot/modeline-spacious-indicators)))

(use-package mini-echo
  :ensure
  (mini-echo :repo "eki3z/mini-echo.el" :host github)
  :init
  (mini-echo-mode)
  :custom
  (mini-echo-persistent-rule
   '(:long ("major-mode" "shrink-path" "vcs" "buffer-position" "buffer-size" "flymake")
           :short ("buffer-name" "buffer-position" "flymake"))))

;; (set-face-attribute 'default nil
;;                     :family "Departure Mono" :height 110)
;; (set-face-attribute 'fixed-pitch nil
;;                     :family "Departure Mono")

(set-face-attribute 'default nil
                    :family "Berkeley Mono" :height 110)
(set-face-attribute 'fixed-pitch nil
                    :family "Berkeley Mono"
                    :height 1.0)

(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Etoile"
                    :height 110)

(use-package display-line-numbers
  :ensure nil
  :hook (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-major-tick 0)
  (display-line-numbers-minor-tick 0)
  (display-line-numbers-grow-only t)
  (display-line-numbers-type t)
  (display-line-numbers-width 3)
  (display-line-numbers-widen t))

(use-package punpun-themes
  :ensure (punpun-themes :type git :url "https://depp.brause.cc/punpun-themes.git"))

(use-package modus-themes
  ;; :init
  ;; (load-theme 'modus-operandi-tinted t)
  :custom
  (modus-themes-custom-auto-reload nil)
  (modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
  ;; (modus-themes-to-toggle '(modus-operandi-tinted modus-vivendi-tinted))
  ;; (modus-themes-to-toggle '(modus-operandi-deuteranopia modus-vivendi-deuteranopia))
  ;; (modus-themes-to-toggle '(modus-operandi-tritanopia modus-vivendi-tritanopia))
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs nil)
  (modus-themes-completions '((t . (extrabold))))
  (modus-themes-prompts '(extrabold))
  (modus-themes-headings
   '((agenda-structure . (variable-pitch light 2.2)))
   (agenda-date . (variable-pitch regular 1.3))
   (t . (regular 1.15))))

(use-package standard-themes
  ;; Read the doc string of each of those user options.  These are some
  ;; sample values.
  :custom
  (standard-themes-bold-constructs t)
  (standard-themes-italic-constructs t)
  (standard-themes-disable-other-themes t)
  (standard-themes-mixed-fonts t)
  (standard-themes-variable-pitch-ui t)
  (standard-themes-prompts '(extrabold italic))

  ;; more complex alist to set weight, height, and optional
  ;; `variable-pitch' per heading level (t is for any level not
  ;; specified):
  (standard-themes-headings
   '((0 . (variable-pitch light 1.5))
     (1 . (variable-pitch light 1.4))
     (2 . (variable-pitch light 1.3))
     (3 . (variable-pitch semilight 1.2))
     (4 . (variable-pitch semilight 1.1))
     (5 . (variable-pitch 1.1))
     (6 . (variable-pitch 1.1))
     (7 . (variable-pitch 1.1))
     (agenda-date . (1.2))
     (agenda-structure . (variable-pitch light 1.3))
     (t . (variable-pitch 1.1)))))

(use-package stimmung-themes
  :ensure (stimmung-themes :host github :repo "motform/stimmung-themes"))

(use-package ef-themes
  :custom
  (ef-themes-variable-pitch-ui t)
  (ef-themes-mixed-fonts t)
  (ef-themes-rotate ef-themes-items)
  (ef-themes-headings ; read the manual's entry of the doc string
   '((0 . (variable-pitch light 1.4))
     (1 . (variable-pitch light 1.35))
     (2 . (variable-pitch regular 1.3))
     (3 . (variable-pitch regular 1.25))
     (4 . (variable-pitch regular 1.2))
     (5 . (variable-pitch 1.15)) ; absence of weight means `bold'
     (6 . (variable-pitch 1.1))
     (7 . (variable-pitch 1.05))
     (agenda-date . (semilight 1.2))
     (agenda-structure . (variable-pitch light 1.4))
     (t . (variable-pitch 1.0))))
  :init (load-theme 'ef-melissa-light t))

(use-package rainbow-mode)

(use-package cursory
  :ensure t
  :demand t
  :if (display-graphic-p)
  :config
  (setq cursory-presets
        '((box
           :blink-cursor-interval 1.2)
          (box-no-blink
           :blink-cursor-mode -1)
          (bar
           :cursor-type (bar . 2)
           :blink-cursor-interval 0.8)
          (bar-no-other-window
           :inherit bar
           :cursor-in-non-selected-windows nil)
          (bar-no-blink
           :cursor-type (bar . 2)
           :blink-cursor-mode -1)
          (underscore
           :cursor-type (hbar . 3)
           :blink-cursor-interval 0.3
           :blink-cursor-blinks 50)
          (underscore-no-blink
           :blink-cursor-mode -1
           :cursor-type (hbar . 3))
          (underscore-no-other-window
           :inherit underscore
           :cursor-in-non-selected-windows nil)
          (underscore-thick
           :cursor-type (hbar . 8)
           :blink-cursor-interval 0.3
           :blink-cursor-blinks 50
           :cursor-in-non-selected-windows (hbar . 3))
          (underscore-thick-no-blink
           :blink-cursor-mode -1
           :cursor-type (hbar . 8)
           :cursor-in-non-selected-windows (hbar . 3))
          (t ; the default values
           :cursor-type box
           :cursor-in-non-selected-windows hollow
           :blink-cursor-mode 1
           :blink-cursor-blinks 10
           :blink-cursor-interval 0.2
           :blink-cursor-delay 0.2)))

  ;; I am using the default values of `cursory-latest-state-file'.

  ;; Set last preset or fall back to desired style from `cursory-presets'.
  (cursory-set-preset (or (cursory-restore-latest-preset) 'box))

  (cursory-mode 1)
  :bind
  ;; We have to use the "point" mnemonic, because C-c c is often the
  ;; suggested binding for `org-capture' and is the one I use as well.
  ("C-c p" . cursory-set-preset))

(use-package org
  :ensure nil
  ;; :hook (org-mode . variable-pitch-mode)
  :custom
  ;; Edit settings
  (org-auto-align-tags nil)
  (org-tags-column 0)
  (org-catch-invisible-edits 'show-and-error)
  (org-special-ctrl-a/e t)
  (org-insert-heading-respect-content t)

  ;; Org styling, hide markup etc.
  (org-hide-emphasis-markers t)
  (org-pretty-entities t)

  (org-startup-indented t)
  (org-use-sub-superscripts "{}")
  (org-startup-with-inline-images t)
  ;; Agenda styling
  (org-agenda-tags-column 0)
  (org-agenda-block-separator ?─)
  (org-agenda-time-grid
   '((daily today require-timed)
     (800 1000 1200 1400 1600 1800 2000)
     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄"))
  (org-agenda-current-time-string
   "◀── now ─────────────────────────────────────────────────")
  ;; Ellipsis styling
  (org-ellipsis "⤵")
  ;; Src blocks
  (org-edit-src-content-indentation 0)
  (org-src-preserve-indentation nil)

  (org-src-window-setup 'current-window)

  (org-babel-clojure-backend 'cider)

  (org-babel-lisp-eval-fn 'sly-eval)
  (org-babel-python-command "python3")
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (C . t)
     (lisp . t)
     (clojure . t)))
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil)

  (add-to-list 'org-structure-template-alist '("cpp" . "src cpp"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("clj" . "src clojure"))
  (add-to-list 'org-structure-template-alist '("p" . "src python"))
  (add-to-list 'org-structure-template-alist '("cl" . "src lisp"))
  (require 'org-tempo)
  (require 'ob-clojure))
;; (use-package ox-pandoc)

(use-package ob-mermaid)

(use-package ox-typst
  :ensure (ox-typst :type git
                    :host github
                    :repo "jmpunkt/ox-typst"))

(use-package org-modern
  :preface
  (defun indent-region-advice (&rest ignored)
    (let ((deactivate deactivate-mark))
      (if (region-active-p)
          (indent-region (region-beginning) (region-end))
        (indent-region (line-beginning-position) (line-end-position)))
      (setq deactivate-mark deactivate)))
  :custom
  (org-modern-hide-stars nil)		; adds extra indentation
  (org-modern-table nil)
  (org-modern-list
   '(;; (?- . "-")
     (?* . "•")
     (?+ . "‣")))
  (org-modern-block-name '("" . "")) ; or other chars; so top bracket is drawn promptly
  :hook
  (org-mode . org-modern-mode)
  (org-agenda-finalize . org-modern-agenda)
  :config
  (advice-add 'move-text-up :after 'indent-region-advice)
  (advice-add 'move-text-down :after 'indent-region-advice))

;; (use-package org-modern-indent
;;   :ensure (org-modern-indent :type git :host github :repo "jdtsmith/org-modern-indent")
;;   :config ; add late to hook
;;   (add-hook 'org-mode-hook #'org-modern-indent-mode 90))

(use-package org-bullets
  :hook org-mode)

(use-package org-appear
  :hook
  (org-mode . org-appear-mode))

(use-package denote
  :custom
  (denote-directory "~/Documents/Notes"))

(use-package ace-window
  :general
  ([remap other-window] 'ace-window))

(use-package popper
  :custom
  (popper-reference-buffers
   '("\\*Messages\\*"
     "Output\\*$"
     "\\*Async Shell Command\\*"
     help-mode
     compilation-mode))
  :config
  (global-set-key (kbd "C-`") 'popper-toggle)
  (global-set-key (kbd "M-`") 'popper-cycle)
  (global-set-key (kbd "C-M-`") 'popper-toggle-type)
  :init
  (popper-mode +1)
  (popper-echo-mode +1))

(use-package ultra-scroll
  :ensure (ultra-scroll :type git :host github :repo  "jdtsmith/ultra-scroll")
  :config
  (ultra-scroll-mode 1))

;;; Header line context of symbol/heading (breadcrumb.el)
(use-package breadcrumb
  :ensure t
  :functions (prot/breadcrumb-local-mode)
  :hook ((text-mode prog-mode) . prot/breadcrumb-local-mode)
  :custom
  (breadcrumb-project-max-length 0.5)
  (breadcrumb-project-crumb-separator "/")
  (breadcrumb-imenu-max-length 1.0)
  (breadcrumb-imenu-crumb-separator " > ")
  :config
  (defun prot/breadcrumb-local-mode ()
    "Enable `breadcrumb-local-mode' if the buffer is visiting a file."
    (when buffer-file-name
      (breadcrumb-local-mode 1))))

;; Enable narrowings to enhance focus, and reduce accidental
;; edits of nonfocus areas (thanks to save-restrictions).
;; h/t bbatsov/prelude
;; Note: `C-x n w` makes all visible again.
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(use-package avy
  :demand t)

(use-package move-text
  :init
  (move-text-default-bindings))

(use-package undo-fu
  :demand t
  :custom
  (undo-fu-ignore-keyboard-quit t)
  :general
  ("C-z" 'undo-fu-only-undo)
   ([remap undo] 'undo-fu-only-undo)
   ("C-S-z" 'undo-fu-only-redo)
   ([remap undo-redo] 'undo-fu-only-redo))

(use-package olivetti
  :hook (text-mode . olivetti-mode)
  ;; :bind ("C-c o" . olivetti-mode)
  :custom
  (olivetti-body-width 0.65)
  (olivetti-minimum-body-width 80)
  (olivetti-margin-width 5)
  (olivetti-recall-visual-line-mode-entry-state t)
  (olivetti-style 'fancy)
  (olivetti-enable-borders t)
  (olivetti-window-local t))

;; (require 'jinx)
;; (add-hook 'emacs-startup-hook #'global-jinx-mode)
(keymap-global-set "M-$" #'jinx-correct)
(keymap-global-set "C-M-$" #'jinx-languages)

(use-package markdown-mode)

(use-package pdf-tools
  :ensure nil
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :custom
  (pdf-annot-activate-created-annotations t)
  (pdf-view-midnight-colors '("#839496" . "#002b36"))
  ;; Smoother scrolling
  (pdf-view-continuous t)
  (pdf-view-display-size 'fit-page)

  ;; Better quality rendering
  (pdf-view-quality 300)
  :config
  (require 'pdf-tools)
  (require 'pdf-history)
  (require 'pdf-occur)
  (require 'pdf-links)
  (require 'pdf-info)
  (require 'pdf-misc)
  (require 'pdf-sync)
  (require 'pdf-outline)
  (require 'pdf-annot)
  (pdf-tools-install)
  ;; More intuitive zooming
  (define-key pdf-view-mode-map (kbd "C-=") 'pdf-view-enlarge)
  (define-key pdf-view-mode-map (kbd "C--") 'pdf-view-shrink)
  ;; Midnight mode for better night reading
  (add-hook 'pdf-view-mode-hook (lambda ()
                                  (pdf-view-midnight-minor-mode)))
  ;; Enable image-mode for better scaling
  (auto-image-file-mode 1)

  ;; Custom faces for annotations
  (custom-set-faces
   '(pdf-view-rectangle ((t (:background "blue" :foreground "black"))))
   '(pdf-view-highlight ((t (:background "yellow" :foreground "black"))))
   '(pdf-view-strike-through ((t (:background "red" :foreground "black")))))
  (require 'pdf-tools)
  (require 'pdf-view))

;; (use-package visual-fill-column)

(use-package nov
  ;; :after visual-fill-column
  :mode ("\\.epub\\'" . nov-mode)
  :custom
  ;; Variable settings
  (nov-text-width t)
  ;; (visual-fill-column-center-text t)
  :hook
  ;; Hooks
  ((nov-mode . olivetti-mode)
   ;; (nov-mode . visual-line-mode)
   ;; (nov-mode . visual-fill-column-mode)
   ))

(use-package dired
  :ensure nil
  :commands (dired)
  :hook
  ((dired-mode . dired-hide-details-mode)
   (dired-mode . hl-line-mode))
  :custom
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)
  (delete-by-moving-to-trash t)
  (dired-dwim-target t))

(use-package dired-subtree
  :after dired
  :bind
  ( :map dired-mode-map
    ("<tab>" . dired-subtree-toggle)
    ("TAB" . dired-subtree-toggle)
    ("<backtab>" . dired-subtree-remove)
    ("S-TAB" . dired-subtree-remove))
  :custom
  (dired-subtree-use-backgrounds nil))

(use-package trashed
  :commands (trashed)
  :custom
  (trashed-action-confirmer 'y-or-n-p)
  (trashed-use-header-line t)
  (trashed-sort-key '("Date deleted" . t))
  (trashed-date-format "%Y-%m-%d %H:%M:%S"))

(use-package projtree
  :ensure (projtree :type git :host github
                    :repo "petergardfjall/emacs-projtree")
  :commands (projtree-mode)
  :custom
  (projtree-window-width 40)
  :config
  ;; Git status colors
  (custom-set-faces
   '(projtree-git-modified ((t (:foreground "#ff9933"))))
   '(projtree-git-added ((t (:foreground "#98c379"))))
   '(projtree-git-renamed ((t (:foreground "#61afef"))))
   '(projtree-git-ignored ((t (:foreground "#5c6370"))))
   '(projtree-git-untracked ((t (:foreground "#e06c75"))))
   '(projtree-git-conflict ((t (:foreground "#be5046")))))


  (defun my/projtree-get-chevron (expanded)
    "Get chevron icon based on EXPANDED state."
    (propertize
     (if expanded
         (nerd-icons-mdicon "nf-md-chevron_down")
       (nerd-icons-mdicon "nf-md-chevron_right"))
     'face `(:family "Symbols Nerd Font Mono" :height 0.8 :foreground ,(face-attribute 'mode-line-inactive :foreground))
     'font-lock-face `(:family "Symbols Nerd Font Mono" :height 0.8 :foreground ,(face-attribute 'mode-line-inactive :foreground))
     'display '(raise 0.0)))

  ;; Advice projtree's internal render function
  (advice-add 'projtree->_render-tree-entry :override
              (lambda (self path &optional git-statuses)
                (let* ((filename (file-name-nondirectory path))
                       (is-dir (file-directory-p path))
                       (git-status (when git-statuses
                                   (projtree->_git-status self git-statuses path)))
                       (git-face (when git-status
                                 (projtree-git--status-face git-status)))
                       (icon (if is-dir
                               (nerd-icons-icon-for-dir path nil
                                                      `(:height 0.9 ))
                             (nerd-icons-icon-for-file filename nil
                                                      `(:height 0.9 ))))
                       ;; Remove underscores from icon
                       (icon (propertize icon
                                       'face `(:family "Symbols Nerd Font Mono" :height 1.0)
                                       'font-lock-face `(:family "Symbols Nerd Font Mono" :height 1.0)
                                       'display '(raise 0.0))))
                  (if is-dir
                      ;; Directory
                      (progn
                        (let ((chevron (my/projtree-get-chevron
                                      (projtree->expanded-p self path))))
                          (insert chevron)
                          (insert " ")
                          (insert icon)
                          (insert " ")
                          (insert (propertize filename
                                            'face (or git-face 'projtree-dir)))))
                    ;; Regular file
                    (insert "  ") ; indent to align with directories
                    (insert icon)
                    (insert " ")
                    (insert (propertize filename
                                      'face (or git-face 'projtree-file))))))))

(use-package imenu-list
  :general ("C-'" 'imenu-list-smart-toggle)
  :custom
  (imenu-list-focus-after-activation t)
  (imenu-list-auto-resize t)
  ;; (imenu-list-size 30)
  (imenu-list-position 'right))

(use-package eat
  :ensure  (eat :type git
                :host codeberg
                :repo "akib/emacs-eat"
                :files ("*.el" ("term" "term/*.el") "*.texi"
                        "*.ti" ("terminfo/e" "terminfo/e/*")
                        ("terminfo/65" "terminfo/65/*")
                        ("integration" "integration/*")
                        (:exclude ".dir-locals.el" "*-tests.el")))
  :config
  (add-hook 'eshell-load-hook #'eat-eshell-mode)
  :custom
  (eat-shell "bash"))

(use-package transient)
(use-package magit)
(use-package magit-todos)

(eval-after-load 'smerge-mode
  (lambda ()
    (define-key smerge-mode-map (kbd "C-c v") smerge-basic-map)
    (define-key smerge-mode-map (kbd "C-c C-v") smerge-basic-map)))

;; (use-package ghub
;;   :ensure (ghub :type git :host github
;;                 :repo "magit/ghub"
;;                 :branch "main"))
;; (use-package forge
;;   :ensure (forge :type git :host github
;;                  :repo "magit/forge"
;;                  :branch "main")
;;   :after magit ghub
;;   :config
;;   (add-to-list 'forge-alist
;;                '("bitbucket.org"
;;                  "api.bitbucket.org/2.0"
;;                  "api.bitbucket.org/2.0"
;;                  forge-bitbucket-repository))
;;
;;   (setq forge-auth-sources '("~/.authinfo" "~/.authinfo.gpg"))
;;   (setq auth-sources '("~/.authinfo" "~/.authinfo.gpg")))

(use-package devdocs
  :ensure (devdocs :type git :host github :repo "astoff/devdocs.el")
  :bind (:map global-map
              ("C-h D" . devdocs-lookup))
  :hook ((python-ts-mode . (lambda () (setq-local devdocs-current-docs '("python~3.12"))))
         (c++-ts-mode . (lambda () (setq-local devdocs-current-docs '("cpp"))))
         (elixir-ts-mode . (lambda () (setq-local devdocs-current-docs '("elixir"))))))

;; (use-package dtrt-indent
;;   :hook
;;   (prog-mode . dtrt-indent-mode))

(use-package consult-project-extra)

(use-package rg)

(use-package dumb-jump
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package project
  :ensure nil
  :after transient
  :config
  (rg-define-search rg-todo
                    :query "(TODO|FIXME)"
                    :format regexp
                    :dir project
                    :files current)

  (transient-define-prefix project-transient-menu ()
    "Transient menu for project operations with Consult integration."
    :info-manual "(project) Top"
    ["Search & Find"
     ["Files"
      ("f" "Find file" project-find-file)
      ("C-f" "Consult find file" consult-project-extra-find
       :if (lambda () (fboundp 'consult-project-extra-find)))
      ("r" "Recent files" consult-project-extra-recent
       :if (lambda () (fboundp 'consult-project-extra-recent)))]
     ["Search"
      ("g" "Grep (ripgrep)" consult-ripgrep
       :if (lambda () (fboundp 'consult-ripgrep)))
      ("o" "Occur in project" consult-project-extra-occur
       :if (lambda () (fboundp 'consult-project-extra-occur)))]]

    ["Buffer Management"
     ["Buffers"
      ("b" "Switch buffer" project-switch-to-buffer)
      ("C-b" "Consult buffer" consult-project-buffer
       :if (lambda () (fboundp 'consult-project-buffer)))
      ("k" "Kill project buffers" project-kill-buffers)]
     ["Windows"
      ("v" "Display buffer" project-display-buffer)
      ("x" "Display buffer other frame" project-display-buffer-other-frame)]]

    ["Project Control"
     ["Navigation"
      ("d" "Dired" project-dired)
      ("t" "Tree" projtree-mode)
      ("p" "Switch project" project-switch-project)]
     ["Version Control"
      ("m" "Git" magit)
      ("d" "Todo/Fixme" rg-todo)]])
  :bind (:map global-map
              ("C-c p" . project-transient-menu)))

(use-package projection
  ;; Enable the `projection-hook' feature.
  :hook (after-init . global-projection-hook-mode)

  ;; Require projections immediately after project.el.
  :config
  (with-eval-after-load 'project
    (require 'projection))

  :config
  ;; Uncomment if you want to disable prompts for compile commands customized in .dir-locals.el
  ;; (put 'projection-commands-configure-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-build-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-test-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-run-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-package-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-install-project 'safe-local-variable #'stringp)

  ;; Access pre-configured projection commands from a keybinding of your choice.
  ;; Run `M-x describe-keymap projection-map` for a list of available commands.
  :bind-keymap
  ("C-x P" . projection-map))

(use-package projection-multi
  ;; Allow interactively selecting available compilation targets from the current
  ;; project type.
  :bind ( :map project-prefix-map
          ("RET" . projection-multi-compile)))

(use-package projection-multi-embark
  :after embark
  :after projection-multi
  ;; Add the projection set-command bindings to `compile-multi-embark-command-map'.
  :config (projection-multi-embark-setup-command-map))

;; (use-package project-x
;;   :ensure (project-x :type git
;;                 :host github
;;                 :repo "karthink/project-x")
;;   :after project
;;   :config
;;   (add-hook 'project-find-functions 'project-x-try-local 90)
;;   :custom
;;   (project-x-local-identifier '("package.json" "mix.exs" "Project.toml" ".project")))

(use-package emacs
  :ensure nil
  :hook (prog-mode . electric-pair-local-mode)
  :custom
  (electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit))

;; (use-package puni
;;   :hook ((prog-mode sgml-mode nxml-mode tex-mode eval-expression-minibuffer-setup) . puni-mode))

(use-package fancy-compilation
  :commands (fancy-compilation-mode)
  :custom
  (fancy-compilation-override-colors nil)
  :init
  (fancy-compilation-mode))

(use-package compile-multi)

(use-package consult-compile-multi
  :after compile-multi
  :config (consult-compile-multi-mode))

(use-package compile-multi-nerd-icons
  :after nerd-icons-completion
  :after compile-multi)

(use-package go-mode)

(use-package cider)

(use-package clojure-mode)

(use-package clojure-ts-mode)

;; (use-package clj-refactor)

(use-package flymake-kondor
  :hook (clojure-ts-mode . flymake-kondor-setup))

(add-hook 'clojure-ts-mode-hook #'cider-mode)
;; (add-hook 'clojure-ts-mode-hook #'clj-refactor-mode)

;; Improve C++ mode settings
(add-hook 'c++-ts-mode-hook
          (lambda ()
            (setq-local tab-width 4)
            (setq-local c-basic-offset 4)
            (c-set-offset 'innamespace 0)
            (c-set-offset 'inline-open 0)))

(use-package cmake-mode)
(add-to-list 'auto-mode-alist '("\\.cppm\\'" . c++-ts-mode))
(add-to-list 'auto-mode-alist '("\\.mpp\\'" . c++-ts-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;; (use-package doc-show-inline
;;   :commands (doc-show-inline-mode)
;;
;;   :config
;;   (define-key c-ts-mode-map (kbd "C-;") 'doc-show-inline-mode)
;;   (define-key c++-ts-mode-map (kbd "C-;") 'doc-show-inline-mode)
;;
;;   :hook ((c--ts-mode . doc-show-inline-mode)
;;          (c++-ts-mode . doc-show-inline-mode)))

(use-package beardbolt)

(use-package meson-mode)

(defun cpp-insert-include-guard ()
  "Insert an include guard in the current buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (insert "#pragma once\n")))

(defun cpp-create-implementation-file ()
  "Create a source file for the current header file, replacing 'include' with 'src' in the path."
  (interactive)
  (unless (buffer-file-name)
    (error "Buffer is not visiting a file"))

  (when (cpp-file-is-header-p (buffer-file-name))
    (let* ((header-file (buffer-file-name))
           (header-ext (file-name-extension header-file t))
           (cpp-file (replace-regexp-in-string
                     (regexp-quote header-ext)
                     (cpp-preferred-source-extension)
                     (replace-regexp-in-string "/include/" "/src/" header-file)))
           (cpp-dir (file-name-directory cpp-file))
           (relative-header-path
            (if (string-match "/include/\\(.*\\)$" header-file)
                (match-string 1 header-file)
              (file-name-nondirectory header-file))))

      ;; Create src directory if it doesn't exist
      (unless (file-exists-p cpp-dir)
        (if (yes-or-no-p (format "Directory %s does not exist. Create it? " cpp-dir))
            (make-directory cpp-dir t)
          (error "Aborted: Directory creation cancelled")))

      (find-file cpp-file)
      (insert "#include \"" relative-header-path "\"\n\n"))))

(defvar cpp-header-extensions '(".h" ".hpp" ".hxx" ".hh")
  "List of possible C++ header file extensions.")

(defvar cpp-source-extensions '(".cpp" ".cxx" ".cc")
  "List of possible C++ source file extensions.")

(defun cpp-file-is-header-p (filename)
  "Return t if FILENAME has a header extension."
  (when filename
    (let ((ext (file-name-extension filename t)))
      (member ext cpp-header-extensions))))

(defun cpp-file-is-source-p (filename)
  "Return t if FILENAME has a source extension."
  (when filename
    (let ((ext (file-name-extension filename t)))
      (member ext cpp-source-extensions))))

(defun cpp-preferred-source-extension ()
  "Return the preferred source extension (first in the list)."
  (car cpp-source-extensions))

(defun cpp-switch-between-header-source ()
  "Switch between header and source file with the same name."
  (interactive)
  (let* ((file (buffer-file-name))
         (other-file
          (cond
           ;; Header to source
           ((cpp-file-is-header-p file)
            (let* ((base (file-name-sans-extension file))
                  (found nil))
              ;; Try each source extension
              (catch 'found
                (dolist (ext cpp-source-extensions)
                  (let ((try-file (replace-regexp-in-string
                                 "/include/" "/src/"
                                 (concat base ext))))
                    (when (file-exists-p try-file)
                      (setq found try-file)
                      (throw 'found t))))
                ;; If no existing file found, use preferred extension
                (replace-regexp-in-string
                 "/include/" "/src/"
                 (concat base (cpp-preferred-source-extension))))
              (or found
                  (replace-regexp-in-string
                   "/include/" "/src/"
                   (concat base (cpp-preferred-source-extension))))))

           ;; Source to header
           ((cpp-file-is-source-p file)
            (let* ((base (file-name-sans-extension file))
                  (found nil))
              ;; Try each header extension
              (catch 'found
                (dolist (ext cpp-header-extensions)
                  (let ((try-file (replace-regexp-in-string
                                 "/src/" "/include/"
                                 (concat base ext))))
                    (when (file-exists-p try-file)
                      (setq found try-file)
                      (throw 'found t))))
                ;; If no existing file found, use preferred extension
                (replace-regexp-in-string
                 "/src/" "/include/"
                 (concat base (car cpp-header-extensions))))
              (or found
                  (replace-regexp-in-string
                   "/src/" "/include/"
                   (concat base (car cpp-header-extensions)))))))))

    (if (and other-file (file-exists-p other-file))
        (find-file other-file)
      (if (yes-or-no-p (format "File %s does not exist. Create it? " other-file))
          (find-file other-file)
        (message "Switching aborted")))))

(defun cpp-parse-class-declaration ()
  "Parse the class declaration at point using treesit.
Returns a plist with :class-name, :namespace, and :methods."
  (let* ((node (treesit-node-at (point)))
         (class-node (treesit-parent-until node (lambda (n)
                                                 (string= (treesit-node-type n) "class_specifier"))))
         methods namespace class-name)
    (when class-node
      ;; Get class name
      (setq class-name (treesit-node-text
                       (treesit-node-child-by-field-name class-node "name")))

      ;; Find namespace (if any)
      (let ((ns-node (treesit-parent-until class-node
                                          (lambda (n)
                                            (string= (treesit-node-type n) "namespace_definition")))))
        (when ns-node
          (setq namespace (treesit-node-text
                         (treesit-node-child-by-field-name ns-node "name")))))

      ;; Collect all method declarations using treesit-query-capture
      (let ((captures (treesit-query-capture
                      class-node
                      '((function_declarator) @method))))
        (setq methods
              (mapcar (lambda (cap)
                        (cpp-parse-method-declaration (cdr cap)))
                      captures)))

      (list :class-name class-name
            :namespace namespace
            :methods methods))))

(defun cpp-parse-method-declaration (node)
  "Parse a method declaration node into a plist."
  (let* ((declarator-node (treesit-node-parent node))
         (declaration-node (treesit-node-parent declarator-node))
         ;; For return type, look for everything before the function_declarator
         ;; in the declaration_node's children
         (declaration-children (treesit-node-children declaration-node))
         (siblings-before (seq-take-while
                         (lambda (n) (not (equal n declarator-node)))
                         declaration-children))
         (return-type (mapconcat #'treesit-node-text siblings-before " "))
         ;; Get modifiers like const/noexcept by looking at declaration's children
         (modifiers (treesit-filter-child
                    declaration-node
                    (lambda (n)
                      (member (treesit-node-type n)
                             '("noexcept" "const")))))
         (params (mapcar #'cpp-parse-parameter
                        (mapcar #'cdr
                               (treesit-query-capture
                                node '((parameter_declaration) @param))))))
    (list :name (treesit-node-text
                 (treesit-node-child-by-field-name node "declarator"))
          :return-type (string-trim return-type)
          :params params
          :modifiers (mapcar #'treesit-node-text modifiers))))

(defun cpp-parse-parameter (node)
  "Parse a parameter declaration node into a plist."
  (list :type (treesit-node-text (treesit-node-child-by-field-name node "type"))
        :name (or (treesit-node-text
                  (treesit-node-child-by-field-name node "declarator"))
                 "")))

(defun cpp-method-to-implementation (method-info class-name &optional namespace)
  "Convert a method info plist to its implementation string."
  (let* ((return-type (plist-get method-info :return-type))
         (method-name (plist-get method-info :name))
         (params (plist-get method-info :params))
         (modifiers (plist-get method-info :modifiers))
         (param-str (mapconcat
                    (lambda (p)
                      (format "%s %s"
                             (plist-get p :type)
                             (plist-get p :name)))
                    params ", "))
         (implementation-str
          (format "    %s%s::%s(%s)%s\n    {\n    }\n"
                  (if (string-empty-p return-type)
                      "void "
                    (concat return-type " "))
                  class-name
                  method-name
                  param-str
                  (if modifiers
                      (concat " " (string-join modifiers " "))
                    ""))))
    (if (not (string-empty-p namespace))
        (format "namespace %s {\n%s}" namespace implementation-str)
      implementation-str)))

(defun cpp-generate-implementation ()
  "Generate or update implementation stubs in the cpp file for the class at point."
  (interactive)
  (let* ((class-info (cpp-parse-class-declaration))
         (source-file (cpp-get-source-file (buffer-file-name))))
    (when (and class-info source-file)
      (let ((existing-content (when (file-exists-p source-file)
                               (with-temp-buffer
                                 (insert-file-contents source-file)
                                 (buffer-string))))
            (new-implementations ""))

        ;; Generate implementation for each method
        (dolist (method (plist-get class-info :methods))
          (let ((impl (cpp-method-to-implementation
                      method
                      (plist-get class-info :class-name)
                      (plist-get class-info :namespace))))
            ;; Only add implementation if it doesn't exist
            (unless (and existing-content
                        (string-match-p (regexp-quote (car (split-string impl "\n")))
                                      existing-content))
              (setq new-implementations (concat new-implementations impl)))))

        ;; Create or update the implementation file
        (find-file source-file)
        (unless (file-exists-p source-file)
          (insert "#include \""
                  (string-replace "/include/" "/"
                                (file-relative-name (buffer-file-name)
                                                  (file-name-directory source-file)))
                  "\"\n\n"))
        (goto-char (point-max))
        (insert new-implementations)))))

(defun cpp-get-source-file (header-file)
  "Get the corresponding source file path for a header file."
  (when (string-match "\\.\\(h\\|hpp\\|hxx\\)$" header-file)
    (let* ((header-ext (match-string 0 header-file))
           (cpp-file (replace-regexp-in-string
                     (regexp-quote header-ext) ".cpp"
                     (replace-regexp-in-string "/include/" "/src/" header-file))))
      cpp-file)))

(defun my/cpp-keys ()
  "Set up C++-specific keybindings"
  (local-set-key (kbd "C-c g") 'cpp-insert-include-guard)
  (local-set-key (kbd "C-c m") 'cpp-create-implementation-file)
  (local-set-key (kbd "C-c c") 'cpp-generate-implementation)
  (local-set-key (kbd "C-c h") 'cpp-switch-between-header-source))

(add-hook 'c++-ts-mode-hook #'my/cpp-keys)

(use-package doxygen
  :hook (c++-ts-mode . doxygen-mode)
  :ensure nil
  :after cape treesit)

(use-package geiser)
(use-package geiser-guile)

(use-package zig-mode)

(use-package web-mode
  :mode
  (("\\.html\\'" . web-mode)
   ("\\.phtml\\'" . web-mode)
   ("\\.php\\'" . web-mode)
   ("\\.tpl\\'" . web-mode)
   ("\\.[agj]sp\\'" . web-mode)
   ("\\.as[cp]x\\'" . web-mode)
   ("\\.erb\\'" . web-mode)
   ("\\.mustache\\'" . web-mode)
   ("\\.djhtml\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 4)
  (web-mode-css-indent-offset 4)
  (web-mode-code-indent-offset 4)
  (web-mode-enable-auto-pairing t)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-comment-interpolation t)
  (web-mode-enable-current-element-highlight t))

(use-package nix-mode)

(use-package nickel-mode)

(use-package flymake
  :ensure nil
  :hook ((prog-mode . flymake-mode)
         (text-mode . flymake-mode))
  ;; :bind (:map global-map
  ;;             ("M-n" . #'flymake-goto-next-error)
  ;;             ("M-p" . #'flymake-goto-prev-error)
  ;;             )
  :custom
  (flymake-no-changes-timeout 0.5)
  (flymake-start-on-flymake-mode t)
  (flymake-proc-allowed-file-name-masks nil)
  (help-at-pt-display-when-idle t)
  (flymake-show-diagnostics-at-end-of-line t))

;; (use-package flymake-popon
;;   :ensure (flymake-popon
;;            :type git
;;            :repo "https://codeberg.org/akib/emacs-flymake-popon.git")
;;   :hook (flymake-mode . flymake-popon-mode))

(use-package treesit
  :ensure nil
  :custom
(treesit-extra-load-path
 (list "~/.guix-profile/lib"
       (expand-file-name "~/.nix-profile/lib/tree-sitter")))
  (major-mode-remap-alist
   '(
     ;; (elixir-mode . elixir-ts-mode)
     ;; (cmake-mode . cmake-ts-mode)
     ;; (go-mode . go-ts-mode)
     (c++-mode . c++-ts-mode)
     (c-mode . c-ts-mode)
     ;; (zig-mode . zig-ts-mode)
     (js-mode . typescript-ts-mode)
     (js2-mode . typescript-ts-mode)
     (python-mode . python-ts-mode))))

(use-package consult-eglot)

(use-package yasnippet
  :hook (prog-mode . yas-minor-mode)
  :config
  (add-to-list 'yas-snippet-dirs  (expand-file-name "snippets" user-emacs-directory)))

(use-package eglot
  :config
  (transient-define-prefix eglot-transient-menu ()
    "Transient menu for Eglot operations."
    :info-manual "(eglot) Top"
    ["Server"
     ["Control"
      ("s" "Start" eglot)
      ("q" "Shutdown" eglot-shutdown)
      ("r" "Reconnect" eglot-reconnect)]
     ["Info"
      ("i" "Server info" eglot-events-buffer)
      ("l" "LSP stderr" eglot-stderr-buffer)
      ("x" "Project diagnostics" flymake-show-project-diagnostics)]]

    ["Current Buffer"
     ["Navigation"
      ("d" "Find declaration" eglot-find-declaration)
      ("D" "Find definition" eglot-find-implementation)
      ("t" "Find typeDefinition" eglot-find-typeDefinition)
      ("u" "Find references" xref-find-references)]
     ["Help"
      ("h" "Hover" eldoc-box-help-at-point)
      ("a" "Code actions" eglot-code-actions)
      ("n" "Symbol rename" eglot-rename)]]

    ["Workspace"
     ["Actions"
      ("w" "Workspace symbol" consult-eglot-symbols)
      ("f" "Format buffer" eglot-format-buffer)
      ("F" "Format region" eglot-format)]
     ["Diagnostics"
      ("e" "Show all errors" flymake-show-diagnostics-buffer)
      ("c" "Check buffer" flymake-start)]])
  :hook ((c++-ts-mode . eglot-ensure)
         (go-ts-mode . eglot-ensure)
         (python-ts-mode . eglot-ensure)
         (elixir-ts-mode . eglot-ensure)
         ((tsx-ts-mode
           typescript-ts-mode
           js-ts-mode) . eglot-ensure)
         (web-mode . eglot-ensure)
         (zig-mode . eglot-ensure)
         (zig-mode . eglot-ensure)
         (julia-mode . eglot-ensure))
  :config
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  (add-to-list 'eglot-server-programs
               '((c++-ts-mode)
                 . ("clangd"
                    "-j=8"
                    "--log=error"
                    "--malloc-trim"
                    "--background-index"
                    "--clang-tidy"
                    "--completion-style=detailed"
                    "--pch-storage=memory"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0"
                    "--completion-style=bundled")))
  (add-to-list 'eglot-server-programs
               '((html-ts-mode)
                 . ("superhtml"
                    "lsp")))
  :custom
  ;; Good default
  (eglot-events-buffer-size 0) ;; No event buffers (Lsp server logs)
  (eglot-autoshutdown t);; Shutdown unused servers.
  (eglot-report-progress nil)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-sync-connect nil)
  (eglot-send-changes-idle-time 3)
  (eglot-connect-timeout nil)
  (eglot-stay-out-of '())
  :config
  (fset #'jsonrpc--log-event #'ignore)
  (setq jsonrpc-event-hook nil)
  (add-to-list 'eglot-ignored-server-capabilites :hoverProvider)
  ;; Custom key bindings
  :bind (:map eglot-mode-map
              ("C-c e" . eglot-transient-menu)))

;; (use-package eglot-booster
;;   :ensure (eglot-booster :type git :host github :repo "jdtsmith/eglot-booster")
;;   :after eglot
;;   :hook ((eglot-managed-mode . eglot-booster-mode)))

(use-package eglot-inactive-regions

  :ensure (eglot-inactive-regions :type git
                                  :host github
                                  :repo "fargiolas/eglot-inactive-regions")
  :custom
  (eglot-inactive-regions-style 'darken-foreground)
  (eglot-inactive-regions-opacity 0.4)
  :config
  (eglot-inactive-regions-mode t))

(use-package eldoc-box
  :ensure (eldoc-box :type git
                     :host github
                     :repo "casouri/eldoc-box")
  :hook ((eglot-managed-mode . eldoc-box-hover-mode)
         (lsp-copilot-mode . eldoc-box-hover-mode))
  :custom-face
  (eldoc-box-body ((t (:inherit 'variable-pitch)))))

(use-package dape
  ;; :preface
  ;; By default dape shares the same keybinding prefix as `gud'
  ;; If you do not want to use any prefix, set it to nil.
  ;; (setq dape-key-prefix "\C-x\C-a")

  ;; :hook
  ;; Save breakpoints on quit
  ;; ((kill-emacs . dape-breakpoint-save)
  ;; Load breakpoints on startup
  ;;  (after-init . dape-breakpoint-load))

  :custom
  ;; Turn on global bindings for setting breakpoints with mouse
  ;; (dape-breakpoint-global-mode)

  ;; Info buffers to the right
  ;; (setq dape-buffer-window-arrangement 'right)

  ;; Info buffers like gud (gdb-mi)
  ;; (dape-buffer-window-arrangement 'gud)
  ;; (setq dape-info-hide-mode-line nil)

  ;; Pulse source line (performance hit)
  ;; (add-hook 'dape-display-source-hook 'pulse-momentary-highlight-one-line)

  ;; Showing inlay hints
  (dape-inlay-hints t)

  ;; Save buffers on startup, useful for interpreted languages
  ;; (add-hook 'dape-start-hook (lambda () (save-some-buffers t t)))

  ;; Kill compile buffer on build success
  ;; (add-hook 'dape-compile-hook 'kill-buffer)
  :config
  (add-hook 'dape-display-source-hook 'pulse-momentary-highlight-one-line)
  (remove-hook 'dape-start-hook 'dape-repl))

;; Enable repeat mode for more ergonomic `dape' use
(use-package repeat
  :ensure nil
  :init
  (repeat-mode))

(use-package gptel
  :config
  ;; (setq gptel-model "claude-3-5-sonnet-latest") ;  "claude-3-opus-20240229" also available
  ;; (setq gptel-backend (gptel-make-anthropic "Claude"
  ;;                       :stream t
  ;;                       :key #'gptel-api-key-from-auth-source))
  (setq gptel-model  'gpt-4o
        gptel-backend
        (gptel-make-openai "Github Models" ;Any name you want
          :host "models.inference.ai.azure.com"
          :endpoint "/chat/completions?api-version=2024-05-01-preview"
          :stream t
          :key #'gptel-api-key-from-auth-source
          :models '(gpt-4o))))

(use-package elysium
  :custom
  ;; Below are the default values
  (elysium-window-size 0.33) ; The elysium buffer will be 1/3 your screen
  (elysium-window-style 'vertical))

(use-package ellama
  :bind ("C-c l" . ellama-transient-main-menu)
  :init
  ;; setup key bindings
  ;; (setopt ellama-keymap-prefix "C-c e")
  ;; language you want ellama to translate to
  (setopt ellama-language "English")
  ;; could be llm-openai for example
  (require 'llm-ollama)
  (setopt ellama-summarization-provider
	    (make-llm-ollama
	     :chat-model "qwen2.5:3b"
	     :embedding-model "nomic-embed-text"
	     :default-chat-non-standard-params '(("num_ctx" . 32768))))
  (setopt ellama-coding-provider
	    (make-llm-ollama
	     :chat-model "qwen2.5-coder"
	     :embedding-model "nomic-embed-text"
	     :default-chat-non-standard-params '(("num_ctx" . 32768)))))

;; (use-package lisp
;;   :ensure nil
;;   :hook
;;   (after-save . check-parens))

(use-package highlight-sexp
  :ensure (highlight-sexp :repo "daimrod/highlight-sexp" :host github)
  :hook
  (clojure-mode . highlight-sexp-mode)
  (emacs-lisp-mode . highlight-sexp-mode)
  (lisp-mode . highlight-sexp-mode))

(use-package lispy
  :ensure (lispy
           :type git
           :host github
           :repo "enzuru/lispy")
  :hook ((emacs-lisp-mode . lispy-mode)
         (ielm-mode . lispy-mode)
         (lisp-mode . lispy-mode)
         (lisp-interaction-mode . lispy-mode)
         (geiser-repl-mode . lispy-mode)
         (sly-mrepl-mode . lispy-mode)
         (cider-repl-mode . lispy-mode)
         (clojure-mode . lispy-mode)
         (scheme-mode . lispy-mode)))

(use-package elisp-mode
  :ensure nil
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-d C-d" . describe-function)
        ("C-c C-d d" . describe-function)
        ("C-c C-k" . eval-buffer)))

(use-package highlight-defined
  :custom
  (highlight-defined-face-use-itself t)
  :hook
  (help-mode . highlight-defined-mode)
  (emacs-lisp-mode . highlight-defined-mode))

(use-package highlight-quoted
  :hook
  (emacs-lisp-mode . highlight-quoted-mode))


(use-package suggest)

(use-package ipretty
  :config
  (ipretty-mode 1))

;; (use-package elsa)
;;
;; (use-package flymake-elsa
;;   :ensure (flymake-elsa :repo "flymake/flymake-elsa" :host github)
;;   :hook
;;   (emacs-lisp-mode . flymake-elsa-load))

(use-package eros
  :hook
  (emacs-lisp-mode . eros-mode))

(use-package macrostep)

(use-package sly
  :preface
  (defun my/sly-load-project ()
    "Load the current project's ASDF system."
    (message "Starting my/sly-load-project...")  ; Debug message
    (when-let* ((project (project-current))
                (root (project-root project))
                (asd-files (directory-files root t "\\.asd$")))
      (message "Found ASD files: %s" asd-files)  ; Debug message
      (when (= (length asd-files) 1)
        (let ((system-name (file-name-base (car asd-files))))
          (message "Loading system: %s" system-name)  ; Debug message
          (sly-eval `(cl:progn
                      (cl:load ,(car asd-files))
                      (asdf:load-system ,(intern (concat ":" system-name)))))))))

  (defun my/set-sly-directory (&rest _)
    "Set SLY's default directory to project root."
    (when-let* ((project (project-current))
                (root (project-root project)))
      (setq default-directory root)))
  :config
  (add-hook 'sly-connected-hook #'my/sly-load-project)
  (advice-add 'sly :before #'my/set-sly-directory))

(use-package sly-overlay
  :after sly
  :ensure  (sly-overlay :host sourcehut :repo "fosskers/sly-overlay")
  :custom-face
  ;; Customize the overlay face - adjust colors to your preference
  (sly-overlay-result-overlay-face ((((class color) (background light))
                                    :background "#e8e8e8" :box (:line-width -1 :color "#d0d0d0"))
                                   (((class color) (background dark))
                                    :background "#303030" :box (:line-width -1 :color "#404040"))))
  :config
  (defun sly-overlay-eval-last-expression ()
    "Evaluate the expression before point and overlay the results."
    (interactive)
    (let* ((end (point))
           (start (save-excursion (backward-sexp) (point)))
           (expr (buffer-substring-no-properties start end))
           (result (sly-eval `(slynk:pprint-eval ,expr))))
      (sly-overlay--eval-overlay result end)
      (message "%s" result)))
  :bind (:map sly-mode-map
              ([remap sly-eval-defun] . sly-overlay-eval-defun)
              ([remap sly-eval-last-expression] . sly-overlay-eval-last-expression)))

(use-package op-transient
  :after transient
  :ensure nil
  :custom
  (op-transient-debug t)
  (op-transient-account "rideontrack")
  (op-transient-email "sil.vaes@rideontrack.com"))
