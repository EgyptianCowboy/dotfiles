;;; org-config.el --- Org mode config                -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package org
  :ensure nil
  :hook (org-mode . variable-pitch-mode)
  :custom
  ;; Edit settings
  (org-auto-align-tags nil)
  (org-tags-column 0)
  (org-catch-invisible-edits 'show-and-error)
  (org-special-ctrl-a/e t)
  (org-insert-heading-respect-content t)

  ;; Org styling, hide markup etc.
  (org-hide-emphasis-markers t)
  (org-pretty-entities t)
  (org-ellipsis "…")

  ;; Agenda styling
  (org-agenda-tags-column 0)
  (org-agenda-block-separator ?─)
  (org-agenda-time-grid
   '((daily today require-timed)
     (800 1000 1200 1400 1600 1800 2000)
     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄"))
  (org-agenda-current-time-string
   "◀── now ─────────────────────────────────────────────────"))

(use-package org-modern
  :ensure t
  :init
  (global-org-modern-mode))

(use-package olivetti
  :hook (text-mode . olivetti-mode)
  :bind ("C-c o" . olivetti-mode)
  :custom
  (olivetti-body-width 0.65)
  (olivetti-minimum-body-width 80)
  (olivetti-margin-width 5)
  (olivetti-recall-visual-line-mode-entry-state t)
  (olivetti-style 'fancy)
  (olivetti-enable-borders t)
  (olivetti-window-local t))

;; (use-package org-auctex
;;   :ensure (org-auctex :type git :host github :repo "karthink/org-auctex")
;;   :hook (org-mode . org-auctex-mode))

(use-package org-present
  :ensure t
  :hook
  ((org-present-mode . (lambda ()
                         (org-present-big)
                         (org-display-inline-images)
                         (org-present-hide-cursor)
                         (org-present-read-only)))
   (org-present-mode-quit . (lambda ()
                              (org-present-small)
                              (org-remove-inline-images)
                              (org-present-show-cursor)
                              (org-present-read-write)))))

(provide 'org-config)
;;; org-config.el ends here
