;;; ocaml-config.el ---                              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(eval-and-compile
  (defun opam-emacs-load-path ()
    (expand-file-name
     "share/emacs/site-lisp"
     (file-name-directory
      (shell-command-to-string "opam var share")))))

;; Major mode for OCaml programming
(use-package tuareg
  :mode ("\\.ml[ip]?\\'" . tuareg-mode)
  :custom
  (tuareg-mode-name "🐫"))

(use-package utop
  :bind
  (:map tuareg-mode-map
        ([remap tuareg-eval-phrase] . utop-eval-phrase)
        ([remap tuareg-eval-buffer] . utop-eval-buffer)
        ([remap tuareg-eval-region] . utop-eval-region))
  :hook
  (tuareg-mode . utop-minor-mode)
  :custom
  (utop-command "dune utop . -- -emacs"))

(use-package dune)

(provide 'ocaml-config)
;;; ocaml-config.el ends here
