;;; lsp-config.el --- LSP settings                   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; (use-package lsp-mode
;;   :hook ((c++-ts-mode . lsp-deferred)
;;          (terraform-mode . lsp-deferred)
;;          (python-ts-mode . lsp-deferred)
;;          (nix-mode . lsp-deferred))
;;   :custom
;;   (lsp-completion-provider :none) ;; we use Corfu!
;;   :init
;;   (defun my/lsp-mode-setup-completion ()
;;     (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
;;           '(flex))) ;; Configure flex
;;   :hook
;;   (lsp-completion-mode . my/lsp-mode-setup-completion))

;; (use-package lsp-ui)


;; (use-package consult-lsp
;;   :bind
;;   (:map lsp-mode-map
;;         ([remap xref-find-apropos] . consult-lsp-symbols)))

(use-package eglot
  :bind (:map eglot-mode-map
              ("C-c C-d" . eldoc)
              ("C-c C-e" . eglot-rename)
              ("C-c C-f" . eglot-format-buffer))
  :hook ((c++-ts-mode . eglot-ensure)
         (terraform-mode . eglot-ensure)
         (python-ts-mode . eglot-ensure)
         (nix-mode . eglot-ensure)
         (latex-mode . eglot-ensure)
         (haskell-mode . eglot-ensure)
         ;; (typst-ts-mode . eglot-ensure)
         (tuareg-mode . eglot-ensure)
         (cmake-ts-mode . eglot-ensure)
         (clojure-mode . eglot-ensure)
         (elm-mode . eglot-ensure)
         (eglot-managed-mode . eglot-inlay-hints-mode))
  :config
  (add-to-list 'eglot-server-programs '(nix-mode . ("nil")))
  (add-to-list 'eglot-server-programs '((python-ts-mode python-mode) . ("pylsp")))
  (add-to-list 'eglot-server-programs '((latex-mode) . ("texlab")))
  (add-to-list 'eglot-server-programs '((typst-ts-mode) . ("typst-lsp")))
  (add-to-list 'eglot-server-programs
               '((c-mode c++-mode)
                 . ("clangd"
                    "-j=8"
                    "--log=error"
                    "--malloc-trim"
                    "--background-index"
                    "--clang-tidy"
                    "--clang-format"
                    "--cross-file-rename"
                    "--completion-style=detailed"
                    "--pch-storage=memory"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0"
                    "--completion-style=bundled"
                    "--suggest-missing-includes")))
  
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  (setq-default eglot-workspace-configuration
                '((:pylsp . (:configurationSources ["flake8"]
                                                   :plugins
                                                   (
                                                    :pycodestyle (:enabled nil)
                                                    :mccabe (:enabled nil)
                                                    :pyflakes (:enabled nil)
                                                    :flake8 (:enabled nil)
                                                    :ruff (:enabled t
                                                                    :lineLength 120
                                                                    :select ["F" "E" "W" "N" "D" "UP" "YTT" "ANN" "ASYNC" "FBT" "B" "A" "COM" "C4" "PT" "RET" "SLF" "SIM" "TID" "TCH" "ARG" "PTH" "PL" "PERF" "FURB" "RUF" "Q"]
                                                                    :targetVersion "py311")
                                                    :mypy (:enabled t
                                                                    :live_mode t)
                                                    :pydocstyle (:enabled nil)
                                                    :yapf (:enabled nil)
                                                    :autopep8 (:enabled nil)
                                                    :black (:enabled nil))))))
  
  :custom
  (fset #'jsonrpc--log-event #'ignore)
  (eglot-autoshutdown t)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-sync-connect nil)
  (eglot-send-changes-idle-time 3)
  (flymake-no-changes-timeout 5)
  (eglot-connect-timeout nil)
  (eldoc-echo-area-use-multiline-p nil)
  ;; (eglot-ignored-server-capabilities
  ;;  '(:documentFormattingProvider
  ;;    :documentRangeFormattingProvider
  ;;    :documentOnTypeFormattingProvider
  ;;    :colorProvider
  ;;    :foldingRangeProvider))
  )

(use-package eglot-booster
  :elpaca (:type git :host github :repo "jdtsmith/eglot-booster")
  :after eglot
  :config
  (eglot-booster-mode))


;; (use-package dape
;;   ;; Currently only on github
;;   :ensure (dape :type git :host github :repo "svaante/dape")
;;   :custom
;;   ;; Add inline variable hints, this feature is highly experimental
;;   (dape-inline-variables t)
;;   ;; (dape-cwd-fn 'projectile-project-root)
;;   (dape-adapter-dir "~/.emacs.d/debug-adapters")
;;   ;; To remove info buffer on startup
;;   ;; (remove-hook 'dape-on-start-hooks 'dape-info)

;;   ;; To remove repl buffer on startup
;;   ;; (remove-hook 'dape-on-start-hooks 'dape-repl)

;;   ;; By default dape uses gdb keybinding prefix
;;   ;; (setq dape-key-prefix "\C-x\C-a")

;;   ;; Kill compile buffer on build success
;;   ;; (add-hook 'dape-compile-compile-hooks 'kill-buffer)

;;   ;; Projectile users
;;   ;; (setq dape-cwd-fn 'projectile-project-root)
;;   ;; :config
;;   ;; (add-to-list 'dape-configs
;;   ;;              '(cpptools
;;   ;;                modes (c-mode c-ts-mode
;;   ;;                              c++-mode c++-ts-mode)
;;   ;;                command "/home/sil/.emacs.d/debug-adapters/cpptools/extension/debugAdapters/bin/OpenDebugAD7"
;;   ;;                host "localhost"
;;   ;;                port 5818
;;   ;;                command-args ("--port" "5818")))
;;   )

;; (use-package eldoc-box
;;   :hook (eglot-managed-mode . eldoc-box-hover-mode))

;; (use-package sideline
;;   :custom
;;   (sideline-backends-left-skip-current-line t)   ; don't display on current line (left)
;;   (sideline-backends-right-skip-current-line t)  ; don't display on current line (right)
;;   (sideline-order-left 'down)                    ; or 'up
;;   (sideline-order-right 'up)                     ; or 'down
;;   (sideline-format-left "%s   ")                 ; format for left aligment
;;   (sideline-format-right "   %s")                ; format for right aligment
;;   (sideline-priority 100)                        ; overlays' priority
;;   (sideline-display-backend-name t)            ; display the backend name
;;   ;;(sideline-backends-left '(sideline-flymake))
;;   (sideline-backends-right '(sideline-flymake))
;;   :hook ((flycheck-mode . sideline-mode)   ; for `sideline-flycheck`
;;          (flymake-mode  . sideline-mode))  ; for `sideline-flymake`
;;   )

;; (use-package sideline-flymake)
;; (use-package sideline-eldoc
;;   :ensure (sideline-eldoc :type git :host github :repo "sideline-eldoc"))

(provide 'lsp-config)
;;; lsp-config.el ends here
