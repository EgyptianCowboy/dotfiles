;;; font-config.el --- Setup fonts                   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(set-face-attribute 'default nil
                    :family "Berkeley Mono" :height 120)
(set-face-attribute 'fixed-pitch nil
                    :family "Berkeley Mono")
;; (set-face-attribute 'default nil
;;                     :family "go-mono" :height 120)
;; (set-face-attribute 'fixed-pitch nil
;;                     :family "go-mono")


(set-face-attribute 'variable-pitch nil
                    :family "Iosevka Etoile"
                    :height 110)
;; (set-face-attribute 'mixed-pitch nil
;;                     :family "Iosevka Etoile")

(customize-set-variable 'inhibit-compacting-font-caches t)
(customize-set-variable 'x-underline-at-descent-line t)
(customize-set-variable 'underline-minimum-offset 0)
(customize-set-variable 'line-spacing 0)
(customize-set-variable 'text-scale-remap-header-line t)

(provide 'font-config)
;;; font-config.el ends here
