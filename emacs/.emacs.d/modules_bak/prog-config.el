;;; prog-config.el --- General programming configs   -*- lexical-binding: t; -*-

;; Copyright (C) 2023

;; Author:  <sil@t480s>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package smartparens
  :hook (prog-mode . smartparens-mode))

(use-package flymake
  :ensure nil
  :bind (:map flymake-mode-map
              ("C-c f" . transient-flymake))
  :hook
  ((prog-mode . flymake-mode)))

(use-package flymake-popon
  :ensure (flymake-popon
           :type git
           :repo "https://codeberg.org/akib/emacs-flymake-popon.git")
  :hook (flymake-mode . flymake-popon-mode))

(use-package beardbolt
  :ensure (beardbolt :type git
                     :host github
                     :repo "joaotavora/beardbolt"))

(use-package display-line-numbers
  :ensure nil
  :hook (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-major-tick 0)
  (display-line-numbers-minor-tick 0)
  (display-line-numbers-grow-only t)
  (display-line-numbers-type t)
  (display-line-numbers-width 2)
  (display-line-numbers-widen t))

(use-package indent-bars
  :ensure (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :custom
  (indent-bars-treesit-support t)
  (indent-bars-no-descend-string t)
  (indent-bars-treesit-ignore-blank-lines-types '("module"))
  (indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
				      list list_comprehension
				      dictionary dictionary_comprehension
				      parenthesized_expression subscript)))
  (indent-bars-color '(highlight :face-bg t :blend 0.15))
  (indent-bars-pattern ".")
  (indent-bars-width-frac 0.1)
  (indent-bars-pad-frac 0.1)
  (indent-bars-zigzag nil)
  (indent-bars-color-by-depth '(:regexp "outline-\\([0-9]+\\)" :blend 1)) ; blend=1: blend with BG only
  (indent-bars-highlight-current-depth '(:blend 0.5)) ; pump up the BG blend on current
  (indent-bars-display-on-blank-lines t)
  :hook (prog-mode . indent-bars-mode)) ; or whichever modes you prefer

(use-package devdocs
  :ensure (devdocs :type git :host github :repo "astoff/devdocs.el")
  :bind (:map global-map
              ("C-h D" . devdocs-lookup))
  :hook ((python-ts-mode . (lambda () (setq-local devdocs-current-docs '("python~3.12"))))
         (c++-ts-mode . (lambda () (setq-local devdocs-current-docs '("cpp"))))))


(use-package justl
  :ensure (justl :fetcher github
                 :repo "psibi/justl.el")
  :custom
  (justl-recipe-width 25))

;; (use-package just-mode)
;; Configure Tempel
;; (use-package tempel
;;   ;; :bind (("M-+" . tempel-complete) ;; Alternative tempel-expand
;;   ;;        ("M-*" . tempel-insert))
;;   :custom
;;   (tempel-path "~/.config/emacs/templates")
;;   :init
;;   ;; Setup completion at point
;;   (defun tempel-setup-capf ()
;;     ;; Add the Tempel Capf to `completion-at-point-functions'.
;;     ;; `tempel-expand' only triggers on exact matches. Alternatively use
;;     ;; `tempel-complete' if you want to see all matches, but then you
;;     ;; should also configure `tempel-trigger-prefix', such that Tempel
;;     ;; does not trigger too often when you don't expect it. NOTE: We add
;;     ;; `tempel-expand' *before* the main programming mode Capf, such
;;     ;; that it will be tried first.
;;     (setq-local completion-at-point-functions
;;                 (cons #'tempel-expand
;;                       completion-at-point-functions)))

;;   ;; (add-hook 'conf-mode-hook 'tempel-setup-capf)
;;   (add-hook 'prog-mode-hook 'tempel-setup-capf)
;;   ;; (add-hook 'text-mode-hook 'tempel-setup-capf)

;;   ;; Optionally make the Tempel templates available to Abbrev,
;;   ;; either locally or globally. `expand-abbrev' is bound to C-x '.
;;   ;; (add-hook 'prog-mode-hook #'tempel-abbrev-mode)
;;   ;; (global-tempel-abbrev-mode)
;; )

(use-package drepl
  :ensure (drepl :type git :host github :repo "astoff/drepl"))

(provide 'prog-config)
;;; prog-config.el ends here
