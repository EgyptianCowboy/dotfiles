;;; latex-config.el --- Latex config                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; (use-package latex
;;   :ensure nil
;;   :mode (("\\.tex\\'" . TeX-latex-mode)
;;          ("\\.tex\\.erb\\'" . TeX-latex-mode)
;;          ("\\.etx\\'" . TeX-latex-mode))
;;   :custom
;;   (TeX-global-PDF-mode 1)
;;   (preview-scale-function 1.5)
;;   (TeX-auto-save t)
;;   (TeX-parse-self t)
;;   (TeX-save-query nil)
;;   (TeX-source-correlate-method 'synctex)
;;   :hook ((LaTeX-mode . prettify-symbols-mode)
;;          (LaTeX-mode . TeX-fold-mode)
;;          (LaTeX-mode-hook . (lambda ()
;;                               (add-to-list 'TeX-command-list
;;                                            '("LatexMk" "latexmk -pdf %s" TeX-run-TeX nil t :help "Run LatexMk"))))))

;; (use-package cdlatex
;;   :hook (LaTeX-mode . turn-on-cdlatex))

;; (use-package xenops
;;   :hook (LaTeX-mode . xenops-mode))

(use-package pdf-tools
  :ensure (pdf-tools :pre-build ("./server/autobuild") :files (:defaults "server/epdfinfo"))
  :functions (pdf-isearch-batch-mode)
  :commands (pdf-tools-install pdf-view-mode)
  :custom (pdf-view-midnight-colors '("#AFA27C" . "#0F0E16"))
  :config (add-hook 'pdf-view-mode-hook
                    (lambda ()
                      ;; get rid of borders on pdf's edges
                      ;; (set (make-local-variable 'evil-normal-state-cursor) (list nil))
                      ;;for fast i-search in pdf buffers
                      (pdf-isearch-minor-mode)
                      (pdf-isearch-batch-mode)
                      ;; (pdf-view-dark-minor-mode)
                      ;; (pdf-view-midnight-minor-mode)
                      ))
  :mode (("\\.pdf\\'" . pdf-view-mode)))

(provide 'latex-config)
;;; latex-config.el ends here
