;;; compilation-tdd.el --- TDD and compilation configuration for emacs  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package compile
  :ensure nil
  :custom
  (compilation-scroll-output 'first-error)
  (compilation-always-kill t)
  (compilation-max-output-line-length nil)
  :hook (compilation-mode . hl-line-mode)
  :init
  (add-hook 'compilation-buffer-function
            (lambda (buf str)
              (if (null (string-match ".*exited abnormally.*" str))
                  (progn
                    (run-at-time
                     "1 sec" nil 'delete-window-on
                     (get-buffer-create "*compilation*"))
                    (message "No Compilation Errors!"))))))

(use-package fancy-compilation
  :ensure t
  :init
  (fancy-compilation-mode)
  :custom
  (fancy-compilation-scroll-output 'first-error)
  (fancy-compilation-override-colors nil)
  (fancy-compilation-term "xterm-256color"))

;; (use-package recompile-on-save
;;   :hook (after-init . (lambda () (run-at-time 1 nil
;;                                               (lambda ()
;;                                                 (when (get-buffer "*Compile Log*")
;;                                                   (kill-buffer "*Compile Log*")
;;                                                   (delete-other-windows))))))
;;   ;; :init
;;   ;; (recompile-on-save-advice compile)
;;   :custom
;;   (fancy-compilation-override-colors nil)
;;   (fancy-compilation-term "xterm-256color"))
                        

(provide 'compilation-tdd)
;;; compilation-tdd.el ends here
