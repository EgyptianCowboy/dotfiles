;;;; config-init.el --- Initial configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2023
;; SPDX-License-Identifier: MIT

;;; Commentary:

;;; Code:

(require 'project)

;; If the source file is newer than the compiled file, load it instead
;; of the compiled version.
(customize-set-variable 'load-prefer-newer t)

;; Create the variable if needed
(if (boundp 'emacs-home)
    (message "emacs-home value set by user: %s" emacs-home)
  (defvar emacs-home nil
    "Defines where the Emacs project was cloned to.
This is set when loading the config-init.el module during
initialization.  Alternatively, it can be set by the user
explicitly."))

;; Only set the `emacs-home' variable if it does not already
;; have a value set by the user.
(when (null emacs-home)
  (setq emacs-home
        (expand-file-name
         (project-root
          (project-current nil (file-name-directory load-file-name))))))

;; update the `load-path' to include the Emacs modules path

(let ((modules (expand-file-name "./modules/" emacs-home)))
  (when (file-directory-p modules)
    (message (concat "adding modules to load-path: " modules))
    (add-to-list 'load-path modules)))

(let ((lisp-packages (expand-file-name "./lisp/" emacs-home)))
  (when (file-directory-p lisp-packages)
    (message (concat "adding lisp file to load-path: " lisp-packages))
    (add-to-list 'load-path lisp-packages)))

;; When writing modules, insert header from skeleton
(auto-insert-mode)
(with-eval-after-load "autoinsert"
  (define-auto-insert
    (cons (concat (expand-file-name emacs-home) "modules/*\\.el")
          "Emacs Lisp Skeleton")
    '("Emacs Module Description: "
      ";;;; " (file-name-nondirectory (buffer-file-name)) " --- " str
      (make-string (max 2 (- 80 (current-column) 27)) ?\s)
      "-*- lexical-binding: t; -*-" '(setq lexical-binding t)
      "
;; Copyright (C) " (format-time-string "%Y") "
;; SPDX-License-Identifier: MIT
;;; Commentary:
;; " _ "
;;; Code:
(provide '"
      (file-name-base (buffer-file-name))
      ")
;;; " (file-name-nondirectory (buffer-file-name)) " ends here\n")))

;; Save all customizations to `custom-file'
;; (add-hook 'after-init-hook #'customize-save-customized)

(provide 'config-init)
;;; config-ini.el ends here
