;;; treesitter-config.el --- Treesitter settings     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package treesit-auto
  :ensure (treesit-auto :host github
                        :repo "renzmann/treesit-auto")
  :demand t
  :custom
  (treesit-font-lock-level 4)
  (c-ts-mode-indent-offset 4)
  (c++-ts-mode-indent-offset 4)
  (treesit-auto-install 'prompt)
  :config
  (setq typst-ts-config
        (make-treesit-auto-recipe
         :lang 'typst
         :ts-mode 'typst-ts-mode
         :url "https://github.com/uben0/tree-sitter-typst"
         :revision "master"
         :source-dir "src"))
  (add-to-list 'treesit-auto-recipe-list typst-ts-config)
  (global-treesit-auto-mode))

(use-package combobulate
  :ensure (combobulate :host github
                       :repo "mickeynp/combobulate")
  :custom
  (combobulate-key-prefix "C-c m"))


(provide 'treesitter-config)
;;; treesitter-config.el ends here
