;;; sane-defaults.el --- Sane defaults for emacs     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;;; General settings
(setq delete-pair-blink-delay 0.15 ; Emacs28
      help-window-select t
      next-error-recenter '(4) ; center of the window
      find-library-include-other-files nil ; Emacs 29
      remote-file-name-inhibit-delete-by-moving-to-trash t ; Emacs 30
      remote-file-name-inhibit-auto-save t                 ; Emacs 30
      save-interprogram-paste-before-kill t
      mode-require-final-newline 'visit-save)
(setq-default truncate-partial-width-windows nil)

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

(customize-set-variable 'yank-pop-change-selection t)
(customize-set-variable 'kill-whole-line t)
(customize-set-variable 'track-eol t) ; Keep cursor at end of lines.
(customize-set-variable 'line-move-visual nil) ; To be required by track-eol
;; Update UI less frequently
(customize-set-variable 'idle-update-delay 1.0)
;; Do not saves duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Typed text replaces the selection if the selection is active,
;; pressing delete or backspace deletes the selection.
(delete-selection-mode)

;; Replacing yes/no with y/n.
(defalias 'yes-or-no-p 'y-or-n-p)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 0)
(customize-set-variable 'hscroll-margin 0)
(customize-set-variable 'scroll-preserve-screen-position t)
(pixel-scroll-precision-mode 1)

(setq mouse-wheel-scroll-amount
      '(1
        ((shift) . 5)
        ((meta) . 0.5)
        ((control) . text-scale))
      mouse-drag-copy-region nil
      make-pointer-invisible t
      mouse-wheel-progressive-speed t
      mouse-wheel-follow-mouse t)
(mouse-wheel-mode 1)

;; Better support for files with long lines
(setq-default bidi-paragraph-direction 'left-to-right)
(setq-default bidi-inhibit-bpa t)
(setq-default indicate-buffer-boundaries nil) ;  Don't show where buffer starts/ends
(setq-default indicate-empty-lines nil)
(setq-default apropos-do-all t)
(global-so-long-mode 1)
(global-subword-mode 1)

(customize-set-variable 'frame-resize-pixelwise t)
(customize-set-variable 'visible-bell nil)
(customize-set-variable 'ring-bell-function 'ignore)
	    ;; Disable system-wide dialogsu
(customize-set-variable 'use-file-dialog nil)
(customize-set-variable 'use-dialog-box nil)                ; Avoid GUI dialogs
(customize-set-variable 'x-gtk-use-system-tooltips nil)     ; Do not use GTK tooltips
(customize-set-variable 'epa-pinentry-mode 'loopback)

(use-package emacs
  :ensure nil
  :preface
  (defun config-visit ()
    (interactive)
    (find-file (concat emacs-home "init.el")))

  (defun split-and-follow-h ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))

  (defun split-and-follow-v ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))

  (defun ol/split-window-right ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'right nil))

  (defun ol/split-window-below ()
    (interactive)
    (split-window (frame-root-window)
                  nil 'below nil))
  ;; :hook (eval-expression-minibuffer-setup . show-paren-mode)
  :bind (:map global-map
	      ("<insert>" . nil)
	      ("M-SPC" . cycle-spacing)
	      ("C-x k" . kill-this-buffer)
	      ("C-c o" . occur)
	      ("C-z" . nil)
	      ("C-c C-z" . nil)
	      ("C-x C-z" . nil)
              ("C-x C-c" . nil)
	      ("C-h h" . nil)
	      ("C-c e" . config-visit)
	      ([remap split-window-below] . split-and-follow-h)
	      ([remap split-window-right] . split-and-follow-v)
              ;; ("C-z 2" . ol/split-window-below)
              ;; ("C-z 3" . ol/split-window-right)
              ))

(use-package no-littering
  :ensure t
  :custom
  (no-littering-etc-directory
   (expand-file-name "config/" user-emacs-directory))
  (no-littering-var-directory
   (expand-file-name "data/" user-emacs-directory))
  :init
  (require 'no-littering))

(use-package recentf
  :ensure nil
  :after no-littering
  :custom
  (recentf-save-file (locate-user-emacs-file "var/recentf-save.el"))
  (recentf-max-saved-items 1000)
  (recentf-exclude '(".gz" ".xz" ".zip" "/elpa/" "/ssh:" "/sudo:"))
  (recentf-auto-cleanup 30)
  (recentf-max-menu-items 50)
  (recentf-additional-variables '(kill-ring search-ring regexp-search-ring))
  :config
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  (run-with-idle-timer 30 t 'recentf-save-list)
  (recentf-mode 1))

(use-package comment-dwim-2
  :bind (:map global-map
              ([remap comment-dwim] . comment-dwim-2)))

(use-package which-key
  :ensure t
  :init
  (which-key-mode))

(use-package uniquify
  :ensure nil
  :custom
  (uniquify-buffer-name-style 'reverse)
  (uniquify-separator " • ")
  (uniquify-after-kill-buffer-p t)
  (uniquify-ignore-buffers-re "^\\*")
  (uniquify-strip-common-suffix t)
  (uniquify-after-kill-buffer-p t))

;; Direnv
(use-package envrc
  :ensure t
  :init (envrc-global-mode 1))

;; ====== Backups ======
(setq create-lockfiles nil
      ;; Enable making backup files
      make-backup-files nil
      ;; Number each backup file
      version-control nil
      ;; Copy instead of renaming current file
      backup-by-copying nil
      ;; Clean up after itself
      delete-old-versions t
      ;; Keep up to 5 old versions of each file
      kept-old-versions 5
      ;; Keep up to 5 new versions of each file
      kept-new-versions 5
      ;; Keep up to 5 versions when cleaning a directory
      dired-kept-versions 5)

;; Paren
(show-paren-mode t)
(setq show-paren-context-when-offscreen 'child-frame
      show-paren-when-point-in-periphery t
      show-paren-when-point-inside-paren t)

;; Make .h files be C++ mode
(setq auto-mode-alist(cons '("\\.h$"   . c++-mode)  auto-mode-alist))

;; Performance
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb

;; Some basic settings
(setq frame-title-format '("%b"))
(setq ring-bell-function 'ignore)
(setq use-short-answers t)
(setq native-comp-async-report-warnings-errors 'silent) ; Emacs 28 with native compilation
(setq native-compile-prune-cache t) ; Emacs 29

(provide 'sane-defaults)
;;; sane-defaults.el ends here
