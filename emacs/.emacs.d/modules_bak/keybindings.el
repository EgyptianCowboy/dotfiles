;;; keybindings.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; (global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

(use-package repeat
  :ensure nil
  :custom
  ;; Repeatable key chords (repeat-mode)
  (repeat-on-final-keystroke t)
  (repeat-exit-timeout 5)
  (repeat-exit-key "<escape>")
  (repeat-keep-prefix nil)
  (repeat-check-key t)
  (repeat-echo-function 'ignore)
  ;; Technically, this is not in repeat.el, though it is the
  ;; same idea.
  (set-mark-command-repeat-pop t)
  :config
  (repeat-mode 1))

(provide 'keybindings)
;;; keybindings.el ends here
