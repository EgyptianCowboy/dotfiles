;;; python-config.el --- Python config for emacs     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; (use-package lsp-pyright
;;   
;;   :hook (python-ts-mode . (lambda ()
;;                           (require 'lsp-pyright)
;;                           (lsp-deferred))))  ; or lsp-deferred

;; (use-package lpy
;;   :hook (python-mode . lpy-mode))

;; (setq-default python-flymake-command '(""))

(defun my/python-mode-hook ()
  (setq-local flymake-diagnostic-functions
              (remove 'python-flymake flymake-diagnostic-functions)))

(add-hook 'python-mode-hook 'my/python-mode-hook)
(add-hook 'python-ts-mode-hook 'my/python-mode-hook)

;; (setq-default python-shell-interpreter "/home/sil/Programming/python/fluent/.venv/bin/python")
(setq-default python-shell-interpreter-interactive-arg "-i")
(setq-default python-shell-prompt-regexp ">>> ")
(setq-default python-shell-prompt-block-regexp "\\.\\.\\. ")
(setq-default python-shell-prompt-output-regexp "")

(provide 'python-config)
;;; python-config.el ends here
