;;; modeline.el --- Modeline config                  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; (use-package minions
;;   :demand t
;;   :custom
;;   (minions-mode-line-delimiters
;;    (cons "" ""))
;;   (minions-mode-line-lighter "☰")
;;   :config
;;   ;; remove trailing space and guard
;;   (let ((minions nil))
;;     (dolist (m minions-mode-line-modes)
;;       (push m minions))
;;     (when (string= " " (car minions))
;;       (pop minions)))
;;   (minions-mode 1))

(use-package breadcrumb
  :ensure (breadcrumb :type git :host github :repo "joaotavora/breadcrumb"))

(defvar-local sil/breadcrumb
    '(:eval (breadcrumb-imenu-crumbs)))
;; '(:eval (breadcrumb-project-crumbs)))

(defvar-local sil/line-buffer-name
    '(:eval
      (list
       " "
       (if (mode-line-window-selected-p)
	   (propertize (format "%s" (buffer-name)) 'face 'bold)
	 (propertize (format "%s" (buffer-name)) 'face 'shadow)))
      )
  "My Personal Modelines Buffername")

(defvar-local sil/modified
    '(:eval (cond (buffer-read-only "⨂")
                  ((buffer-modified-p) "⬤")
                  (t "◯"))))

(defun sil/modeline--major-mode-name ()
  "Return capitalized `major-mode' as a string."
  (capitalize (string-replace "-mode" "" (symbol-name major-mode))))

;; (defvar-local sil/line-major-mode-symbol
;;     '(:eval (nerd-icons-icon-for-buffer))
;;   "Mode line construct to display the major mode.")

(defvar-local sil/line-major-mode
    '(:eval
      (list
       (propertize (sil/modeline--major-mode-name) 'face 'bold)
       ))
  "Mode line construct to display the major mode.")

(declare-function vc-git--symbolic-ref "vc-git" (file))

(defun prot-modeline--vc-branch-name (file backend)
  "Return capitalized VC branch name for FILE with BACKEND."
  (when-let ((rev (vc-working-revision file backend))
             (branch (or (vc-git--symbolic-ref file)
                         (substring rev 0 7))))
    (capitalize branch)))

(declare-function vc-git-working-revision "vc-git" (file))

(defvar prot-modeline-vc-map
  (let ((map (make-sparse-keymap)))
    (define-key map [mode-line down-mouse-1] 'vc-diff)
    (define-key map [mode-line down-mouse-3] 'vc-root-diff)
    map)
  "Keymap to display on VC indicator.")

(defun prot-modeline--vc-help-echo (file)
  "Return `help-echo' message for FILE tracked by VC."
  (format "Revision: %s\nmouse-1: `vc-diff'\nmouse-3: `vc-root-diff'"
          (vc-working-revision file)))

(defun prot-modeline--vc-text (file branch &optional face)
  "Prepare text for Git controlled FILE, given BRANCH.
With optional FACE, use it to propertize the BRANCH."
  (concat
   (propertize (char-to-string #xE0A0) 'face 'shadow)
   " "
   (propertize branch
               'face face
               'mouse-face 'mode-line-highlight
               'help-echo (prot-modeline--vc-help-echo file)
               'local-map prot-modeline-vc-map)
   ;; " "
   ;; (prot-modeline-diffstat file)
   ))

(defun prot-modeline--vc-details (file branch &optional face)
  "Return Git BRANCH details for FILE, truncating it if necessary.
The string is truncated if the width of the window is smaller
than `split-width-threshold'."
  (prot-modeline-string-truncate
   (prot-modeline--vc-text file branch face)))

(defvar prot-modeline--vc-faces
  '((added . vc-locally-added-state)
    (edited . vc-edited-state)
    (removed . vc-removed-state)
    (missing . vc-missing-state)
    (conflict . vc-conflict-state)
    (locked . vc-locked-state)
    (up-to-date . vc-up-to-date-state))
  "VC state faces.")

(defun prot-modeline--vc-get-face (key)
  "Get face from KEY in `prot-modeline--vc-faces'."
   (alist-get key prot-modeline--vc-faces 'up-to-date))

(defun prot-modeline--vc-face (file backend)
  "Return VC state face for FILE with BACKEND."
  (prot-modeline--vc-get-face (vc-state file backend)))


(defvar-local prot-modeline-vc-branch
    '(:eval
      (when-let* (((mode-line-window-selected-p))
                  (file (buffer-file-name))
                  (backend (vc-backend file))
                  ;; ((vc-git-registered file))
                  (branch (prot-modeline--vc-branch-name file backend))
                  (face (prot-modeline--vc-face file backend)))
        (prot-modeline--vc-details file branch face)))
  "Mode line construct to return propertized VC branch.")

;;;; Flymake errors, warnings, notes

(declare-function flymake--severity "flymake" (type))
(declare-function flymake-diagnostic-type "flymake" (diag))

;; Based on `flymake--mode-line-counter'.
(defun prot-modeline-flymake-counter (type)
  "Compute number of diagnostics in buffer with TYPE's severity.
TYPE is usually keyword `:error', `:warning' or `:note'."
  (let ((count 0))
    (dolist (d (flymake-diagnostics))
      (when (= (flymake--severity type)
               (flymake--severity (flymake-diagnostic-type d)))
        (cl-incf count)))
    (when (cl-plusp count)
      (number-to-string count))))

(defvar prot-modeline-flymake-map
  (let ((map (make-sparse-keymap)))
    (define-key map [mode-line down-mouse-1] 'flymake-show-buffer-diagnostics)
    (define-key map [mode-line down-mouse-3] 'flymake-show-project-diagnostics)
    map)
  "Keymap to display on Flymake indicator.")

(defmacro prot-modeline-flymake-type (type indicator &optional face)
  "Return function that handles Flymake TYPE with stylistic INDICATOR and FACE."
  `(defun ,(intern (format "prot-modeline-flymake-%s" type)) ()
     (when-let ((count (prot-modeline-flymake-counter
                        ,(intern (format ":%s" type)))))
       (concat
        (propertize ,indicator 'face 'shadow)
        (propertize count
                    'face ',(or face type)
                     'mouse-face 'mode-line-highlight
                     ;; FIXME 2023-07-03: Clicking on the text with
                     ;; this buffer and a single warning present, the
                     ;; diagnostics take up the entire frame.  Why?
                     'local-map prot-modeline-flymake-map
                     'help-echo "mouse-1: buffer diagnostics\nmouse-3: project diagnostics")))))

(prot-modeline-flymake-type error "☣")
(prot-modeline-flymake-type warning "!")
(prot-modeline-flymake-type note "·" success)

(defvar-local prot-modeline-flymake
    `(:eval
      (when (and (bound-and-true-p flymake-mode)
                 (mode-line-window-selected-p))
        (list
         ;; See the calls to the macro `prot-modeline-flymake-type'
         '(:eval (prot-modeline-flymake-error))
         '(:eval (prot-modeline-flymake-warning))
         '(:eval (prot-modeline-flymake-note)))))
  "Mode line construct displaying `flymake-mode-line-format'.
Specific to the current window's mode line.")

;;;; Eglot

(with-eval-after-load 'eglot
  (setq mode-line-misc-info
        (delete '(eglot--managed-mode (" [" eglot--mode-line-format "] ")) mode-line-misc-info)))

(defvar-local prot-modeline-eglot
    `(:eval
      (when (and (featurep 'eglot) (mode-line-window-selected-p))
        '(eglot--managed-mode eglot--mode-line-format)))
  "Mode line construct displaying Eglot information.
Specific to the current window's mode line.")


(dolist (construct '(sil/modified
                     sil/line-buffer-name
                     sil/line-major-mode-symbol
		     sil/line-major-mode
                     sil/breadcrumb
                     ;; prot-modeline-vc-branch
                     prot-modeline-flymake
                     prot-modeline-eglot
		     ;; sil/git-indicator
		     ;; sil/magit-indicator
                     ))
  (put construct 'risky-local-variable t))


(setq-default mode-line-format
	      '("%e"
		;; " "
                ;; sil/line-major-mode-symbol
		" "
                sil/line-major-mode
		mode-line-format-right-align
		prot-modeline-vc-branch
                " " prot-modeline-flymake
                " " prot-modeline-eglot))

(setq mode-line-right-align-edge 'window)

(setq-default header-line-format
	      '(" "
                sil/modified
                " "
                sil/breadcrumb
                " in "
                sil/line-buffer-name
                ))


;; (use-package emacs
;;   :ensure nil
;;   :custom-face
;;   (mode-line ((t (:background nil :overline nil :box nil))))
;;   (mode-line-active ((t (:background nil :overline (:color "black" :position t) :box nil))))
;;   (mode-line-inactive ((t (:background nil :overline (:color "black" :position t) :box nil))))
;;   (header-line ((t (:background nil :underline (:color "black" :position t) :box nil :height 140)))))

(provide 'modeline)
;;; modeline.el ends here
