;;; haskell-config.el --- Haskell configuration      -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package haskell-mode
  :bind (:map haskell-mode-map
              ("C-c M-." . hoogle                         )
              ;; Jump to the import blocks and back in current file.
              ([f12]     . haskell-navigate-imports       )
              ([f11]     . haskell-navigate-imports-return)
              ;; Interactive stuff
              ("C-c C-z" . haskell-interactive-switch     )
              ("C-c C-l" . haskell-process-load-file)
              ;; For the times when the LSP stuff fails
              ("C-c ."   . haskell-process-do-type        )
              ("C-c ,"   . haskell-process-do-info        )
              ("C-M-;"   . haskell-mode-jump-to-def-or-tag))
  :custom
  (haskell-interactive-popup-errors nil) ; Don't pop up errors in a separate buffer.
  (haskell-indentation-where-pre-offset  1)
  (haskell-indentation-where-post-offset 1)
  (haskell-process-auto-import-loaded-modules t))

(use-package hindent
  :hook
  (haskell-mode . hindent-mode)
  :custom
  (hindent-process-path "fourmolu")
  (hindent-extra-args '("-o" "-XBangPatterns"
                        "-o" "-XTypeApplications"
                        "--indentation" "2")))


(provide 'haskell-config)
;;; haskell-config.el ends here
