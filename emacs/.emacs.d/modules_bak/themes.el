;;; themes.el --- Emacs themes                       -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package spacious-padding
  :demand t
  :ensure (spacious-padding :host github
                            :repo "protesilaos/spacious-padding")
  :custom
  (spacious-padding-widths
   '( :internal-border-width 10
      :header-line-width 4
      :mode-line-width 2
      :tab-width 2
      :right-divider-width 30
      :scroll-bar-width 8))
  :config
  (spacious-padding-mode t))

(use-package modus-themes
  :demand t
  :ensure (modus-themes :host github
                        :repo "protesilaos/modus-themes")
  :config
  (defun my-modus-themes-custom-faces ()
    (modus-themes-with-colors
      (custom-set-faces
       ;; Simpler modeline
       `(mode-line ((,c :background nil
                        :underline nil
                        :overline ,border-mode-line-active
                        :box nil )))
       `(mode-line-active ((,c :background nil
                               :underline nil
                               :overline ,border-mode-line-active
                               :box nil)))
       `(mode-line-inactive ((,c :background nil
                                 :underline nil
                                 :overline ,border-mode-line-inactive
                                 :box nil)))
       `(header-line ((t (:background nil :underline (:color ,border-mode-line-active :position t) :box nil)))))))
  (add-hook 'modus-themes-after-load-theme-hook #'my-modus-themes-custom-faces)
  ;; (modus-themes-load-theme 'modus-operandi-tinted)
  (modus-themes-load-theme 'modus-operandi)
  :bind (:map global-map
              ("<f6>" . modus-themes-toggle))
  :custom
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-custom-auto-reload t)
  (modus-themes-disable-other-themes t)

  (modus-themes-prompts '(italic-bold))
  (modus-themes-completions
   '((matches . (bold underline))
     (selection . (semibold))))

  (modus-themes-common-palette-overrides
   `(
     (fg-prompt cyan-faint)
     (bg-prompt unspecified)

     (fringe unspecified)
     (line-number unspecified)
     (fg-window-divider bg-main)
     (window-divider-first-pixel unspecified)
     (window-divider-last-pixel unspecified)

     (bg-paren-match bg-magenta-intense)

     (underline-err red-faint)
     (underline-warning yellow-faint)
     (underline-note cyan-faint)

     (fg-line-number-inactive "gray50")
     (fg-line-number-active fg-main)
     (bg-line-number-inactive unspecified)
     (bg-line-number-active unspecified))))

;; (use-package nano-theme
;;   :init
;;   (load-theme 'nano-theme t))

;; (use-package nano-vertico
;;   :ensure (nano-vertico :host github
;;                         :repo "rougier/nano-vertico")
;;   :init
;;   (nano-vertico-mode 1))


(use-package rainbow-mode
  :ensure t
  :init
  (rainbow-mode 1))

(provide 'themes)
;;; themes.el ends here
