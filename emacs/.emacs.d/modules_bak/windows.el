;;; windows.el --- Emacs config to handle window movement  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package windmove
  :ensure nil
  :custom
  (windmove-create-window nil)
  :bind (:map global-map
              ("M-o" . transient-window)))

;; (use-package ace-window)

;; Popup windows
(use-package popper
  :ensure t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :custom
  (popper-reference-buffers
   '("\\*compilation\\*" compilation-mode
     "\\*Cargo Run\\*" cargo-process-mode
     "^\\*vterm\\*" vterm-mode
     "^\\*Flymake.*" flymake-mode
     "^\\*Flycheck.*" flycheck-error-list-mode
     "^\\*lsp-help\\*" lsp-help-mode
     "^\\*eldoc\\*" special-mode
     "^\\*ert\\*" ert-results-mode
     "^\\*HTTP Response\\*" javascript-mode
     "^\\*SQL.*" sql-interactive-mode
     "^\\*cargo-test\\*" cargo-test-mode
     "^\\*cargo-run\\*" cargo-run-mode
     "^\\*rustic-compilation\\*" rustic-compilation-mode
       "\\*Messages\\*"
     "Output\\*$"
     "\\*Async Shell Command\\*"
     "\\*Go-Translate\\*"
     ("^\\*Warnings\\*$" . hide)
     help-mode
     helpful-mode))
  :init
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

(provide 'windows)
;;; windows.el ends here
