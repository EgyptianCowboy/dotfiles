;;; infra-config.el --- Configs for infrastructure mods  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package terraform-mode
  :hook
  (terraform-mode . outline-minor-mode)
  :custom (terraform-indent-level 4))

(use-package ansible
  :commands ansible-auto-decrypt-encrypt
  :init
  (put 'ansible-vault-password-file 'safe-local-variable #'stringp)
  :custom
  (ansible-section-face 'font-lock-variable-name-face)
  (ansible-task-label-face 'font-lock-doc-face))

(use-package ansible-doc)

(use-package jinja2-mode
  :mode "\\.j2$"
  :custom
  ;; The default behavior is to reindent the whole buffer on save. This is
  ;; disruptive and imposing. There are indentation commands available; the user
  ;; can decide when they want their code reindented.
  (jinja2-enable-indent-on-save nil))

(use-package kubernetes
  :after (magit)
  :commands (kubernetes-overview)
  :custom
  (kubernetes-poll-frequency 3600)
  (kubernetes-redraw-frequency 3600))

(use-package yaml-mode)

(use-package yaml-pro
  :hook
  (yaml-mode . yaml-pro-ts-mode))

(provide 'infra-config)
;;; infra-config.el ends here
