;;; llm.el --- LLM configs                           -*- lexical-binding: t; -*-

;; Copyright (C) 2023  

;; Author:  <sil@t480s>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package openai
  :ensure (openai :type git :host github :repo "emacs-openai/openai")
  :config
  (setq openai-key #'openai-key-auth-source))

(use-package chatgpt
  :ensure (chatgpt :type git :host github :repo "emacs-openai/chatgpt")
  :requires openai)

(provide 'llm)
;;; llm.el ends here
