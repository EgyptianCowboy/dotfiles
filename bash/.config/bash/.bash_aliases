alias shutdown='doas shutdown now'
alias reboot='doas reboot'
alias zathura='zathura --config-dir=$XDG_CONFIG_HOME/zathura'
alias refresh='source ~/.bashrc'
alias grep='rg'
alias diff='colordiff'

# git aliases
alias timeline='git log --decorate --oneline --graph'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# ls aliases
alias ls='exa'
alias l='exa -l'
alias ll='exa -l'
alias la='exa -a'
alias lla='exa -la'

# find alias
alias find='fd'

# common commands
alias q='exit'
alias c='clear'
alias h='history'
alias cs='clear;ls'
alias o='xdg-open'