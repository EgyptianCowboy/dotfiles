# # ~/.profile: executed by the command interpreter for login shells.
# # This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# # exists.
# # see /usr/share/doc/bash/examples/startup-files for examples.
# # the files are located in the bash-doc package.

# # the default umask is set in /etc/profile; for setting the umask
# # for ssh logins, install and configure the libpam-umask package.
# #umask 022

# export XDG_CONFIG_HOME="$HOME/.config"
# export XDG_CACHE_HOME="$HOME/.cache"
# export XDG_DATA_HOME="$HOME/.local/share"
# export XDG_RUNTIME_DIR="/run/user/`id -u`"

# export VIMINIT=":source $XDG_CONFIG_HOME/vim/.vimrc"
# export INPUTRC="$XDG_CONFIG_HOME/readline/.inputrc"
# export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/.gtkrc-2.0"
# export CCACHE_CONFIGPATH="$XDG_CONFIG_HOME/ccache.config"
# export CCACHE_DIR="$XDG_CACHE_HOME/.ccache"

# export EDITOR="emacs -nw"
# export BROWSER="chromium"
# # export MPD_HOST="localhost"
# # export MPD_PORT="6601"
# # export TERM=st-256color
# export DOTNET_CLI_TELEMETRY_OPTOUT=true

# # xsecurelock
# export XSECURELOCK_AUTH_BACKGROUND_COLOR=#001b58
# export XSECURELOCK_BLANK_TIMEOUT=15
# export XSECURELOCK_BLANK_DPMS_STATE=off
# export XSECURELOCK_PASSWORD_PROMPT=disco
# export XSECURELOCK_FONT=PragmataPro Mono

# export QT_QPA_PLATFORMTHEME="qt5ct"

# #export IDF_PATH="$HOME/.local/esp/esp-idf"
# #export ESPIDF="$HOME/.local/esp/esp-idf"

# export XMONAD_CONFIG_HOME="$XDG_CONFIG_HOME/xmonad"
# export SBCL_HOME=/usr/lib/sbcl/
# export GOPATH="$HOME/.local/go"

# # Start blinking
# export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
# # Start bold
# export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
# # Start stand out
# export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
# # End standout
# export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
# # Start underline
# export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red
# # End Underline
# export LESS_TERMCAP_ue=$(tput sgr0)
# # End bold, blinking, standout, underline
# export LESS_TERMCAP_me=$(tput sgr0)

# add_path() {
#     if [ -d "$1" ]; then
# 	export PATH="$PATH:$1"
#     fi
# }

# add_ldpath() {
#     if [ -z "$LD_LIBRARY_PATH" ]; then
# 	export LD_LIBRARY_PATH="$1"
#     elif [ ! "$LD_LIBRARY_PATH" =~ $1 ]; then
# 	export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$1"
#     fi
# }

# add_ldpath /usr/local/lib
# add_ldpath /home/sil/.local/lib

# add_path $HOME/.ghcup/bin
# add_path $HOME/.cabal/bin

# add_path $HOME/.local/xilinx/Vivado/2019.1/bin
# add_path $HOME/.local/xilinx/SDK/2019.1/bin
# add_path $HOME/.local/xilinx/DocNav/bin
# add_path $HOME/.local/texlive/2019/bin/x86_64-linux
# add_path $HOME/.local/smlnj/bin
# add_path $GOPATH/bin



# INFOPATH="/home/sil/.local/texlive/2019/texmf-dist/doc/info:$INFOPATH"

# # if [ -d "$HOME/.local/anaconda" ]; then
# #     PATH="$HOME/.local/anaconda/bin:$PATH"
# # fi

# # if running bash
# if [ -n "$BASH_VERSION" ]; then
#     # include .bashrc if it exists
#     if [ -f "$HOME/.bashrc" ]; then
# 	. "$HOME/.bashrc"
#     fi
# fi

# # set PATH so it includes user's private bin if it exists
# if [ -d "$HOME/bin" ] ; then
#     PATH="$HOME/bin:$PATH"
# fi

# # set PATH so it includes ~/.local/bin
# if [ -d "$HOME/.local/bin" ] ; then
#     PATH="$HOME/.local/bin:$PATH"
# fi

# # if .local/scripts exists, add this to path
# if [ -d "$HOME/.local/scripts" ] ; then
#     PATH="$HOME/.local/scripts:$PATH"
# fi

# if [ -d "/usr/lib/qt5" ]; then
#     PATH="/usr/lib/qt5/bin:$PATH"
# fi

# if [ -d "$HOME/.local/e4thcom" ] ; then
#     PATH="$HOME/.local/e4thcom:$PATH"
# fi

# if [ -d "$HOME/.nix-profile/bin/" ] ; then
#     PATH="$HOME/.nix-profile/bin/:$PATH"
# fi

# #if [ -z $DISPLAY ] && [ $(tty) = /dev/tty1 ]; then
# #    exec startx > /dev/null 2>&1
# #fi

# . "$HOME/.cargo/env"
