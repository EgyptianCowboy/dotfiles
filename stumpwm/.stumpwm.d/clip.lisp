(load-module "clipboard-history")

(define-key *top-map* (kbd "s-y") "show-clipboard-history")
;; start the polling timer process
(clipboard-history:start-clipboard-manager)
