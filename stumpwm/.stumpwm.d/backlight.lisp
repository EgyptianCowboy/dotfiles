;; min and max level set with media keys 
(define-key *top-map* (kbd "XF86MonBrightnessDown") "exec brightnessctl -q s 10%-")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "exec brightnessctl -q s 10%+")
