(defcommand killandremove () ()
  (run-commands
   "delete-window"
   "remove-split"))

(defcommand redshift () ()
            (run-shell-command "redshift -l 50.51:4.21"))

(defcommand redshift-temp (temp) ((:number "temp?? : "))
            (async-run (concatenate 'string "redshift -P -O  " (write-to-string temp)))
            (echo-string (current-screen) (concat "Screen temperature: " (write-to-string temp))))



(defcommand all-windowlist (&optional (fmt *window-format*)
                            window-list) (:rest)
  (let ((window-list (or window-list
                         (sort-windows-by-number
                          (screen-windows (current-screen))))))
    (if (null window-list)
        (message "No managed windows")
        (let ((window (select-window-from-menu window-list fmt)))
          (if window
              (progn
                (switch-to-group (window-group window))
                (group-focus-window (window-group window) window))
              (throw 'error :abort))))))

(defcommand all-window-formatted () ()
  (all-windowlist "%s%120t"))
