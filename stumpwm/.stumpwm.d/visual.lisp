(in-package :stumpwm)

;; (ql:quickload "clx-truetype")
;; (require 'clx-truetype)
;; (xft:cache-fonts)

(setf *colors*
      '("#1c1b19"                       ; ^0 black
        "#ef2f27"			; ^1 red
        "#519f50"			; ^2 green
        "#fbb829"			; ^3 yellow
        "#2c78bf"			; ^4 blue
        "#e02c6d"			; ^5 magenta
        "#0aaeb3"                       ; ^6 cyan
        ;; "#1c1b19"                       ; Delete this
        "#fce8c3"			; ^7 white
        "#ffffff"                       ; ^8 filler
        "#009696"))

;; (setf *mode-line-background-color* "#1c1b19")
;; (setf *mode-line-foreground-color* "#FCE8C3")

;; (setf *window-focus-color* "#FCE8C3")
;; (setf *window-unfocus-color* "#1c1b19")
;; (setf *float-focus-color* "#FCE8C3")
;; (setf *float-unfocus-color* "#1c1b19")

(set-bg-color "#1c1b19")
(set-fg-color "#fce8c3")
(set-border-color "#fce8c3")
(set-win-bg-color "#1c1b19")

(update-color-map (current-screen))

;; (setf *mode-line-border-width* 0)
(setf *window-border-style* :thin) ;; :none :thick :thin :tight

;; window border widths
(defvar *border-off-width* 1)
(defvar *border-on-width* 1)

;; floating window border widths
(setf *float-window-title-height* 12)
(setf *float-window-border* 1)

(setf *startup-message* nil)

(setf *message-window-gravity* :bottom-right)
(setf *input-window-gravity* :bottom-right)

(set-font "-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso10646-1")

;; (set-font (list  (make-instance 'xft:font
;;                                 :family "PragmataPro Mono"
;;                                 :subfamily "Regular"
;;                                 :size 8)
;;                  (make-instance 'xft:font
;;                                 :family "Material Icons"
;;                                 :subfamily "Regular"
;;                                 :size 10)))

;; (run-with-timer
;;  900 900
;;  (lambda ()
;;    (loop for font in (stumpwm::screen-fonts (current-screen))
;;       when (typep font 'xft:font)
;;       do (clrhash (xft::font-string-line-bboxes font))
;;         (clrhash (xft::font-string-line-alpha-maps font))
;;         (clrhash (xft::font-string-bboxes font))
;;         (clrhash (xft::font-string-alpha-maps font)))))

;; (set-font "-misc-tamzen-medium-r-normal--12-116-100-100-c-80-iso8859-1")


;; (defvar *vol-status-command*
;;   "amixer get Master | grep '[0-9]*\%' -o | tr -d '\\n'")

;; (defvar *vol-status-command*
;;   "pactl list sinks | grep '^[[:space:]]Volume:' | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,'")

;; (defun current-ip ()
;;   "Returns the IP addres in CIDR notation."
;;   (run-shell-command
;;    "CIDR=`ip -4 addr show wlp61s0 | grep inet | awk -F ' ' '{print $2}'`; echo -n $CIDR | tr -d '[:cntrl]'"
;;    t))


;; (defun current-gw ()
;;   "Returns the default gateway"
;;   (run-shell-command
;;    "echo -n Gw:`ip route show | grep default | awk '{print $3}' | tr -d '[:cntrl:]'`"
;;    t))

;; Show time, cpu usage and network traffic in the modelinecomment
;; (setf *screen-mode-line-format*
;;       (list  "%g | "
;;              "%d"
;;              " | ^4^f1^n^f0 %B | ^4^f1^n^f0 "
;;              '(:eval (current-ip)) " - %I"
;;              ;; " | ^4^f1^n^f0 "
;; 	     ;; '(:eval (run-shell-command *vol-status-command* t))
;; 	     " | %W"))

;; (setf *window-format* "%10t"
;;       wifi:*wifi-modeline-fmt* "%p"
;;       *group-format* "%n"
;;       *time-modeline-string* "\[%a\] %d/%m/%y - %H:%M"
;;       *mode-line-position* :top)

;;; When windows are destroyed window numbers are not synced
;;; 2kays <https://github.com/2kays> posted a solution on
;;; the TipsAndTricks section of the wiki
;;; This will repack window numbers every time a window is killed
(add-hook *destroy-window-hook*
          #'(lambda (win) (repack-window-numbers)))

;; Turn on the modeline
;; (if (not (head-mode-line (current-head)))
;;     (toggle-mode-line (current-screen) (current-head)))
