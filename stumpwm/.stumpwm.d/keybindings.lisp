(set-prefix-key (kbd "s-s"))

(defun run-or-raise-prefer-group (cmd win-cls)
  "If there are windows in the same class, cycle in those. Otherwise call run-or-raise with group search t."
  (let ((windows (group-windows (current-group))))
    (if (member win-cls (mapcar #'window-class windows) :test #'string-equal)
        (run-or-raise cmd `(:class ,win-cls) nil T)
        (run-or-raise cmd `(:class ,win-cls) T T))))

(defcommand rr-firefox () ()
            (run-or-raise-prefer-group "firefox" "Firefox"))

(defcommand rr-chromium () ()
            (run-or-raise-prefer-group "chromium" "Chromium"))

(defcommand rr-next () ()
            (run-or-raise-prefer-group "next" "Next"))

(defcommand toggle-modeline () ()
            (toggle-mode-line (current-screen) (current-head)))

(defcommand sil-emacs () ()
            "Run emacs"
            (run-or-raise-prefer-group "emacs -T emacs" "Emacs"))

;; (defcommand rr-emacsclient () ()
;;             (run-or-raise-prefer-group "emacs" "Emacs"))

;; (defcommand emacsclient-launch () ()
;;             (run-shell-command "emacsclient -nc"))

;; (defcommand decide-on-emacsclient () ()
;;             (if (equal (run-shell-command "pgrep \"emacsclient\"" t) "")
;;                 (run-shell-command "emacsclient -c")
;;                 (rr-emacsclient)))

;; (defcommand kill-and-remove () ()
;;             "Kills the window and removes the frame"
;;             (kill)
;;             (remove))

(defcommand vsplit-and-switch () ()
            "Splits vertically and switches to next window"
            (vsplit)
            (fnext))

(defcommand hsplit-and-switch () ()
	    "Splits horizontally and switches to next window"
	    (hsplit)
	    (fnext))

(defcommand hsplit-switch-openterm () ()
	    "Splits horizontally and opens terminal in new window"
	    (hsplit-and-switch)
        (run-commands "exec st"))

(defcommand vsplit-switch-openterm () ()
	    "Splits vertically and opens terminal in new window"
	    (vsplit-and-switch)
        (run-commands "exec st"))

(defvar *pass-keys*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "c") "pass-copy-menu")
    (define-key m (kbd "g") "pass-generate")
    m))

(defvar *redshift-levels*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "1") "redshift-temp 5500")
    (define-key m (kbd "2") "redshift-temp 4500")
    (define-key m (kbd "3") "redshift-temp 3500")
    (define-key m (kbd "4") "redshift-temp 2500")
    (define-key m (kbd "0") "exec redshift -x")
    m))

(defvar *mynet-bindings*
  (let ((m (make-sparse-keymap)))
    m))

(defvar *myterm-frame-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "t") "exec st")
    (define-key m (kbd "v") "vsplit-switch-openterm")
    (define-key m (kbd "h") "hsplit-switch-openterm")
    (define-key m (kbd "x") "exec xterm")
    m))

(defvar *myfloat-frame-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "f") "float-this")
    (define-key m (kbd "v") "unfloat-this")
    m))

;; Group config
(defvar *mygroup-frame-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "F1") "gmove-and-follow 1")
    (define-key m (kbd "F2") "gmove-and-follow 2")
    (define-key m (kbd "F3") "gmove-and-follow 3")
    (define-key m (kbd "F4") "gmove-and-follow 4")
    (define-key m (kbd "1") "gmove-and-follow 1")
    (define-key m (kbd "2") "gmove-and-follow 2")
    (define-key m (kbd "3") "gmove-and-follow 3")
    (define-key m (kbd "4") "gmove-and-follow 4")
    (define-key m (kbd "h")  "gprev")
    (define-key m (kbd "l")  "gnext")
    (define-key m (kbd "H")  "gprev-with-window")
    (define-key m (kbd "L")  "gnext-with-window")
    (define-key m (kbd "i") "groups")
    (define-key m (kbd "w")  "grouplist")
    (define-key m (kbd "m")  "gmove")
    m))

;; Frame bindings
(defvar *myframe-frame-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "r") "iresize")
    (define-key m (kbd "w") "all-windowlist-formatted")
    (define-key m (kbd "W") "windowlist")
    (define-key m (kbd "R") "title")
    (define-key m (kbd "b") "balance-frames")
    ;; (define-key m (kbd "m") "mode-line")
    (define-key m (kbd "f") "fullscreen")
    (define-key m (kbd "F") "only")
    m))

(defvar *mycmus-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "u") "exec cmus-remote -u")
    (define-key m (kbd "n") "exec cmus-remote -n")
    (define-key m (kbd "p") "exec cmus-remote -r")
    m))

(define-key *root-map* (kbd "0") "remove")
(define-key *root-map* (kbd "1") "only")
(define-key *root-map* (kbd "2") "vsplit")
(define-key *root-map* (kbd "3") "hsplit")

(define-key *top-map* (kbd "s-q") "delete")
;; (define-key *top-map* (kbd "s-Q") "killandremove")
(define-key *top-map* (kbd "s-r") "remove")

(define-key *top-map* (kbd "s-f") "pull-hidden-next")
(define-key *top-map* (kbd "s-b") "pull-hidden-previous")
(define-key *top-map* (kbd "M-TAB") "fnext")

(define-key *top-map* (kbd "s-k") "move-focus up")
(define-key *top-map* (kbd "s-j") "move-focus down")
(define-key *top-map* (kbd "s-h") "move-focus left")
(define-key *top-map* (kbd "s-l") "move-focus right")

(define-key *top-map* (kbd "s-K") "move-window up")
(define-key *top-map* (kbd "s-J") "move-window down")
(define-key *top-map* (kbd "s-H") "move-window left")
(define-key *top-map* (kbd "s-L") "move-window right")

(define-key *top-map* (kbd "s-M") "exchange-direction up")
(define-key *top-map* (kbd "s-N") "exchange-direction down")
(define-key *top-map* (kbd "s-<") "exchange-direction left")
(define-key *top-map* (kbd "s->") "exchange-direction right")

(define-key *root-map* (kbd "s-1") "gselect 1")
(define-key *root-map* (kbd "s-2") "gselect 2")
(define-key *root-map* (kbd "s-3") "gselect 3")
(define-key *root-map* (kbd "s-4") "gselect 4")

(define-key *top-map* (kbd "s-F1") "gselect 1")
(define-key *top-map* (kbd "s-F2") "gselect 2")
(define-key *top-map* (kbd "s-F3") "gselect 3")
(define-key *top-map* (kbd "s-F4") "gselect 4")

;; (define-key *root-map* (kbd "s-e") "emacsclient-launch")
;; (define-key *root-map* (kbd "e") "decide-on-emacsclient")
(define-key *root-map* (kbd "e") "sil-emacs")
(define-key *root-map* (kbd "b") "rr-firefox")
;; (define-key *root-map* (kbd "c") "rr-chromium")
(define-key *top-map* (kbd "s-y") "exec clipmenu")
(define-key *top-map* (kbd "s-p") '*pass-keys*)
(define-key *top-map* (kbd "s-t") '*myterm-frame-bindings*)
(define-key *top-map* (kbd "s-w") '*myfloat-frame-bindings*)
(define-key *top-map* (kbd "s-g") '*mygroup-frame-bindings*)
(define-key *top-map* (kbd "s-r") '*myframe-frame-bindings*)
(define-key *top-map* (kbd "s-m") '*mycmus-bindings*)
(define-key *top-map* (kbd "s-n") '*mynet-bindings*)
(define-key *top-map* (kbd "SunPrint_Screen") "exec scrot ~/Pictures/Screenshots/%b%d::%H%M%S.png && notify-send \"Screenshot saved.\"")
(define-key *root-map* (kbd "s")   '*redshift-levels*)
(define-key *top-map* (kbd "s-d") "exec dmenu_run -fn \"PragmataPro Mono-12\"")
(define-key *top-map* (kbd "s-u") "exec dmenu_unicode")
(define-key *root-map* (kbd "t") "toggle-modeline")
(define-key *top-map* (kbd "s-o") "exec xsecurelock")
(define-key *top-map* (kbd "s-SunPrint_Screen") "exec flameshot gui")
