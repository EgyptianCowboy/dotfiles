;; -*- Mode: Lisp -*-
(in-package :stumpwm)

(ql:quickload :xembed)
(ql:quickload :zpng)
;; (ql:quickload :slynk)

(add-to-load-path "~/.local/stumpwm-contrib/")
(init-load-path "~/.local/stumpwm-contrib/")

(mapcar #'load-module
        '("pass"
          "notify"
          ;; "wifi"
          ;; "net"
          ;; "ttf-fonts"
          ;; "battery-portable"
          ))

;; (slynk:create-server :dont-close t :port 4004)

;; (run-shell-command "feh --bg-fill /home/sil/Pictures/wallpaper/painting/cyprien2.jpg")
;; (run-shell-command "autorandr --change")
;; (run-shell-command "feh --bg-fill ~/Pictures/wallpaper/dore/color1.jpg")
;; (run-shell-command "feh --bg-fill ~/Pictures/wallpaper/painting/Shishkin_DozVDubLesu.jpg")
(run-shell-command "feh --bg-center /home/sil/Pictures/wallpaper/Flammarion_scales.jpg")
(run-shell-command "exec xsetroot -cursor_name left_ptr")
(run-shell-command "exec xset b off")
(run-shell-command "exec setxkbmap -layout us")
;; (run-shell-command "exec xrdb -merge .Xresources")
;; (run-shell-command "exec udiskie")
(run-shell-command "polyrun")
;; (run-shell-command "compton -CGb")

(setf (getenv "GDK_CORE_DEVICE_EVENTS") "1")

;; Basics
(setf *startup-message* NIL
      *suppress-abort-messages* t
      *shell-program* (getenv "SHELL")
      ;; *terminal* (getenv "TERM")
      )

;; -special keys
(define-keysym #x1008ff95 "XF86WLAN")
(define-keysym #x1008ff93 "XF86Battery")
(define-keysym #x1008ff13 "XF86AudioRaiseVolume")
(define-keysym #x1008ff11 "XF86AudioLowerVolume")
(define-keysym #x1008ff12 "XF86AudioMute")
(define-keysym #x1008ff2f "XF86Sleep")

(set-module-dir
 (pathname-as-directory (concat (getenv "HOME") "/.stumpwm.d/modules")))
(setf *mouse-focus-policy* :click) ;; :click :ignore :sloppy

;; Set up multiple groups
(stumpwm:run-commands "grename 1")
(stumpwm:run-commands "gnewbg 2")
(stumpwm:run-commands "gnewbg 3")
(stumpwm:run-commands "gnewbg-float 4")

(mapcar #'load '("~/.stumpwm.d/async.lisp"
                 "~/.stumpwm.d/functions.lisp"
                 "~/.stumpwm.d/backlight.lisp"
                 "~/.stumpwm.d/audio.lisp"
                 "~/.stumpwm.d/keybindings.lisp"
                 "~/.stumpwm.d/visual.lisp"
                 ;; "~/.stumpwm.d/tray.lisp"
                 "~/.stumpwm.d/notifications.lisp"
                 ;; "~/.stumpwm.d/clip.lisp"
                 ))

(run-shell-command "exec xmodmap ~/.Xmodmap")
;; (run-shell-command "exec cmst -m")
